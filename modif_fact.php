<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Consultation document</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier un document existant</strong>.
				</p>
				
			</div>
			
<?php

	if (isset($_POST['liste_fact'])) $liste_fact=$_POST['liste_fact'];
		else $liste_fact="";
		
	if (isset($_POST['liste_dev'])) $liste_dev=$_POST['liste_dev'];
		else $liste_dev="";
		
	if (isset($_POST['liste_comm'])) $liste_comm=$_POST['liste_comm'];
		else $liste_comm="";
	
	if (isset($_POST['ListFact'])) $ListFact=$_POST['ListFact'];
		else $ListFact="";
		
	if (isset($_GET['num_fact'])) $ListFact=$_GET['num_fact'];
		else $ListFact=$ListFact;
		
	$ListFact=$liste_fact.$liste_dev.$liste_comm.$ListFact;
		
	$element_club="";
	$Net_prod="";
	$Net_prest="";
	$Net_tot="";
	$Rem_tot="";
	$type_aff="";
	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	
	
	$Requete = "SELECT * FROM $db_facture_entete WHERE num_fact ='$ListFact'";

	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>'); 
					
	$Donnees = mysqli_fetch_array($ResReq);
				
	$num_fact=$Donnees["num_fact"];
	$nom_param=$Donnees["nom_param"];
	$date_fact=$Donnees["date_fact"];
	$date_valid_deb=$Donnees["date_valid_deb"];
	$date_valid_fin=$Donnees["date_valid_fin"];
	$total_HT=$Donnees["total_HT"];
	$total_TVA1=$Donnees["total_TVA1"];
	$total_TVA2=$Donnees["total_TVA2"];
	$total_TVA3=$Donnees["total_TVA3"];
	$total_TVA4=$Donnees["total_TVA4"];
	$tot_produits=$Donnees["tot_produits"];
	$tot_prestas=$Donnees["tot_prestas"];
	$remise=$Donnees["remise"];
	$total_TTC=$Donnees["total_TTC"];
	$avoir=$Donnees["avoir"];
	$type_paiement=$Donnees["type_paiement"];
	$nom_membre=$Donnees["nom_membre"];
	$nom_client=$Donnees["nom_client"];
	$type=$Donnees["type"];
	$etat=$Donnees["etat"];
	
	if ($date_valid_deb!="" || $date_valid_deb!="")
		{
		$date_valid_deb = explode("/", $date_valid_deb);
		$date_valid_deb = $date_valid_deb[2].'-'.$date_valid_deb[1].'-'.$date_valid_deb[0];
		$date_valid_fin = explode("/", $date_valid_fin);
		$date_valid_fin = $date_valid_fin[2].'-'.$date_valid_fin[1].'-'.$date_valid_fin[0];
		}
	else
		{
		$date_valid_deb="";	
		$date_valid_fin="";
		}

	$num_fact_det = explode("-", $num_fact);
	$nouv_num_fact=$num_fact_det[1].'-'.$num_fact_det[2].'-'.$num_fact_det[3];
	$nouv_num_fact_my=$num_fact_det[2].'-'.$num_fact_det[3];
		
	$Requete2 = "SELECT * FROM $db_compte_client WHERE ref_clients ='$nom_client'";

	$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
					
	$Donnees2 = mysqli_fetch_array($ResReq2);
					
	$nom_clie=$Donnees2["nom"];
	$contact_cli=$Donnees2["contact"];
	$adresse_cli=$Donnees2["adresse"];
	$ville_cli=$Donnees2["ville"];
	$code_postal_cli=$Donnees2["code_postal"];
	
	$Requete5 = "SELECT * FROM $db_parametres WHERE ref ='$nom_param'";

	$ResReq5 = mysqli_query($db, $Requete5) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>'); 
					
	$Donnees5 = mysqli_fetch_array($ResReq5);
		
	$nom_parametre=$Donnees5["nom_param"];
	$nom_entreprise=$Donnees5["nom_entreprise"];
	$add_entreprise=$Donnees5["add_entreprise"];
	$cp_ville=$Donnees5["cp_ville"];
	$tel=$Donnees5["tel"];
	$lieu=$Donnees5["lieu"];
	$TVA_intra=$Donnees5["TVA_intra"];
	$cond_devis=$Donnees5["cond_devis"];
	$cond_commande=$Donnees5["cond_commande"];
	$cond_facture=$Donnees5["cond_facture"];
	$mentions=$Donnees5["mentions"];
	$coord_banque=$Donnees5["coord_banque"];
	$coord_RIB=$Donnees5["coord_RIB"];
	$coord_IBAN=$Donnees5["coord_IBAN"];
	$app=$Donnees5["app"];
	$clause=$Donnees5["clause"];
	$capital=$Donnees5["capital"];
	$SIREN=$Donnees5["SIREN"];
	$RCS=$Donnees5["RCS"];
	$APE=$Donnees5["APE"];
	$info_autre=$Donnees5["info_autre"];
	
	$cond_devis=nl2br($cond_devis);
	$cond_commande=nl2br($cond_commande);
	$cond_facture=nl2br($cond_facture);
	
	$Requete3 = "SELECT * FROM $db_facture_detail WHERE num_fact='$num_fact' AND type='produit'";
	$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, facture incorrecte ou inexistante</span>');
	$nbenreg3 = mysqli_num_rows($ResReq3);
	$nbchamps3 = mysqli_num_fields($ResReq3);
	$Tabdo3[$nbenreg3][$nbchamps3]="";
	$Tabchamps3[$nbchamps3]="";
	
			for($K=0; $K < $nbchamps3; $K++) 
				{
				$tabchamps3[$K] = mysqli_fetch_field_direct($ResReq3,$K);
				}
 		
			$K=0;
			
			while ($donnees3 = mysqli_fetch_array($ResReq3))
				{	
				$Tabdo3[$K][0]=$donnees3["ref_fact_det"];
				$Tabdo3[$K][1]=$donnees3["designation"];
				$Tabdo3[$K][2]=$donnees3["reference"];
				$Tabdo3[$K][3]=$donnees3["prix"];
				$Tabdo3[$K][4]=$donnees3["quantite"];
				$Tabdo3[$K][5]=$donnees3["taux_TVA"];
				$Tabdo3[$K][6]=$donnees3["remise"];
				$Tabdo3[$K][7]=$donnees3["totHT"];
				$Tabdo3[$K][8]=$donnees3["TVA"];
				$Tabdo3[$K][9]=$donnees3["totTTC"];
				$Tabdo3[$K][10]=$donnees3["info_comp"];
				$K++;
				}
				
	$Requete4 = "SELECT * FROM $db_facture_detail WHERE num_fact='$num_fact' AND type='prestation'";
	$ResReq4 = mysqli_query($db, $Requete4) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, facture incorrecte ou inexistante</span>');
	$nbenreg4 = mysqli_num_rows($ResReq4);
	$nbchamps4 = mysqli_num_fields($ResReq4);
	$Tabdo4[$nbenreg4][$nbchamps4]="";
	$Tabchamps4[$nbchamps4]="";

			for($L=0; $L < $nbchamps4; $L++) 
				{
				$tabchamps4[$L] = mysqli_fetch_field_direct($ResReq4,$L);
				}
		
			$L=0;
			
			while ($donnees4 = mysqli_fetch_array($ResReq4))
				{
				$Tabdo4[$L][0]=$donnees4["ref_fact_det"];
				$Tabdo4[$L][1]=$donnees4["designation"];
				$Tabdo4[$L][2]=$donnees4["reference"];
				$Tabdo4[$L][3]=$donnees4["prix"];
				$Tabdo4[$L][4]=$donnees4["quantite"];
				$Tabdo4[$L][5]=$donnees4["taux_TVA"];
				$Tabdo4[$L][6]=$donnees4["remise"];
				$Tabdo4[$L][7]=$donnees4["totHT"];
				$Tabdo4[$L][8]=$donnees4["TVA"];
				$Tabdo4[$L][9]=$donnees4["totTTC"];
				$Tabdo4[$L][10]=$donnees4["info_comp"];
				$L++;
				}
	
	$Requete6 = "SELECT designation, reference, quantite FROM $db_prod_prest WHERE nature = 'produit' ORDER by designation";
	$ResReq6 = mysqli_query($db, $Requete6) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, pi&egrave;ce incorrecte ou inexistante</span>');
	$nbenreg6 = mysqli_num_rows($ResReq6);
	$nbchamps6 = mysqli_num_fields($ResReq6);
	$Tabdo6[$nbenreg6][$nbchamps6]="";
	$Tabchamps6[$nbchamps6]="";

			for($N=0; $N < $nbchamps6; $N++) 
				{
				$tabchamps6[$N] = mysqli_fetch_field_direct($ResReq6,$N);
				}

			$N=0;
			
			while ($donnees6 = mysqli_fetch_array($ResReq6))
				{
				$Tabdo6[$N][1]=$donnees6["designation"];
				$Tabdo6[$N][2]=$donnees6["reference"];
				$N++;
				}

	$Requete7 = "SELECT designation, reference, quantite FROM $db_prod_prest WHERE nature = 'prestation' ORDER by designation";
	$ResReq7 = mysqli_query($db, $Requete7) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, prestation incorrecte ou inexistante</span>');
	$nbenreg7 = mysqli_num_rows($ResReq7);
	$nbchamps7 = mysqli_num_fields($ResReq7);
	$Tabdo7[$nbenreg7][$nbchamps7]="";
	$Tabchamps7[$nbchamps7]="";

			for($O=0; $O < $nbchamps7; $O++) 
				{
				$tabchamps7[$O] = mysqli_fetch_field_direct($ResReq7,$O);
				}
 		
			$O=0;
			
			while ($donnees7 = mysqli_fetch_array($ResReq7))
				{
				$Tabdo7[$O][1]=$donnees7["designation"];
				$Tabdo7[$O][2]=$donnees7["reference"];
				$O++;
				}	

	$Requete_param = "SELECT ref, nom_param FROM $db_parametres ORDER by nom_param";
	$ResReq_param = mysqli_query($db, $Requete_param) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>');
	
	$Requete_cli = "SELECT ref_clients, nom, contact FROM $db_compte_client ORDER by nom";
	$ResReq_cli = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
	
?>
			
		<fieldset>
				
			<legend class="lg"> R&eacute;capitulatif g&eacute;n&eacute;ral : </legend>
			
			<form action="req_cons_fact.php" method="post">
			<input type="hidden" name="num_fact" value="<?php echo $num_fact;?>" />
			
			<p><label class="gauche" for="nouv_cli">Nom du client (et contact si indiqu&eacute;)</label>
				<select id="nouv_cli" class="droit" name="nouv_cli">
					<option value="<?php echo $nom_client;?>"><?php echo $nom_clie;?> <?php echo $contact_cli;?></option>
					<?php
					while ($LigneDo_cli = mysqli_fetch_array($ResReq_cli)) 
						{
						$ref_cli = $LigneDo_cli["ref_clients"];
						$nom_cli = $LigneDo_cli["nom"];
						$cont_cli = $LigneDo_cli["contact"];
						echo '<option value="'.$ref_cli.'">'.$nom_cli.' '.$cont_cli.'</option>';
						}
					echo '<option value="">Aucun</option>;';
					?>
				</select></p>
				
			<p><label class="gauche" for="nouv_param">Param&egrave;tres</label>
				<select id="nouv_param" class="droit" name="nouv_param">
					<option value="<?php echo $nom_param;?>"><?php echo $nom_parametre;?></option>
					<?php
					while ($LigneDo_param = mysqli_fetch_array($ResReq_param)) 
						{
						$ref_par = $LigneDo_param["ref"];
						$nom_par = $LigneDo_param["nom_param"];
						echo '<option value="'.$ref_par.'">'.$nom_par.'</option>';
						}
					echo '<option value="">Aucun</option>;';
					?>
				</select></p>
			
<?php
				if ($num_fact_det[0]=="FA")
					{
					$type_aff='facture d\'avoirs';
					}
				else
					{
					$type_aff=$type;
					}
?>
			
			<p><label class="gauche" for="nouv_fact">Num&eacute;ro de <?php echo $type_aff; ?> (num&eacute;ro actuel : <?php echo $num_fact;?>)</label>
			


<?php 

				$db = mysqli_connect($db_server,$db_user,$db_password); 
				mysqli_select_db($db,$db_database);

				$Requete = "SELECT num_fact FROM facture_entete where num_fact LIKE 'D%' ";
				$ResReq = mysqli_query($db, $Requete);

				$max = 0;

				while($obj = $ResReq->fetch_object()){
				    $line =$obj->num_fact; 
				    $line_explode = explode('-', $line);
				    $line_value = $line_explode[1];
				    $line_value = substr($line_value,0,2);
				    if($line_value>$max){
				    	$max = $line_value;
				    }
				};
				$total_D = $max + 1;
				$total_D = sprintf('%02d', $total_D);
				// var_dump($total_D);
				 ?>
				 <?php 

				$db = mysqli_connect($db_server,$db_user,$db_password); 
				mysqli_select_db($db,$db_database);

				$Requete = "SELECT num_fact FROM facture_entete where num_fact LIKE 'F%' ";
				$ResReq = mysqli_query($db, $Requete);

				$max = 0;

				while($obj = $ResReq->fetch_object()){
				    $line =$obj->num_fact; 
				    $line_explode = explode('-', $line);
				    $line_value = $line_explode[1];
				    $line_value = substr($line_value,0,2);
				    if($line_value>$max){
				    	$max = $line_value;
				    }
				};
				$total_F = $max + 1;
				$total_F = sprintf('%02d', $total_F);
				// var_dump($total_F);
				 ?>
			 	<?php 

				$db = mysqli_connect($db_server,$db_user,$db_password); 
				mysqli_select_db($db,$db_database);

				$Requete = "SELECT num_fact FROM facture_entete where num_fact LIKE 'FA%' ";
				$ResReq = mysqli_query($db, $Requete);

				$max = 0;

				while($obj = $ResReq->fetch_object()){
				    $line =$obj->num_fact; 
				    $line_explode = explode('-', $line);
				    $line_value = $line_explode[1];
				    $line_value = substr($line_value,0,2);
				    if($line_value>$max){
				    	$max = $line_value;
				    }
				};
				$total_FA = $max + 1;
				$total_FA = sprintf('%02d', $total_FA);
				// var_dump($total_FA);
				?>
				 <?php 

				$db = mysqli_connect($db_server,$db_user,$db_password); 
				mysqli_select_db($db,$db_database);

				$Requete = "SELECT num_fact FROM facture_entete where num_fact LIKE 'C%' ";
				$ResReq = mysqli_query($db, $Requete);

				$max = 0;

				while($obj = $ResReq->fetch_object()){
				    $line =$obj->num_fact; 
				    $line_explode = explode('-', $line);
				    $line_value = $line_explode[1];
				    $line_value = substr($line_value,0,2);
				    if($line_value>$max){
				    	$max = $line_value;
				    }
				};
				$total_C = $max + 1;
				$total_C = sprintf('%02d', $total_C);
				// var_dump($total_C);
				 ?>
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
				<script type="text/javascript">
				$(function(){
					$('#nom_client').change(function(){
						 var conceptName = $('#nom_client').find("option:selected").val();
					 });
					$('input.on_change').change(function(){
							// var currentMonth = (new Date).getMonth() + 1;
							// var aftermonth = currentMonth + '-';
							// var currentYear = (new Date).getFullYear();
							// var currentdate = aftermonth + currentYear;
							var currentdate = "<?php echo $nouv_num_fact_my ?>";
						if($('.on_change[value="devis"]').is(':checked')) {
							var numberorder_D = "<?php echo $total_D ?>";
							var after = '-' + currentdate;
							var after = numberorder_D + after;
							$('#nouv_fact').attr('value', after);
						}
						if($('.on_change[value="commande"]').is(':checked')) {
							var numberorder_C = "<?php echo $total_C ?>";
							var after = '-' + currentdate;
							var after = numberorder_C + after;
							$('#nouv_fact').attr('value', after);
						}
						if($('.on_change[value="facture"]').is(':checked')) {
							var numberorder_F = "<?php echo $total_F ?>";
							var after = '-' + currentdate;
							var after = numberorder_F + after;
							$('#nouv_fact').attr('value', after); 
						}
						if($('.on_change[value="fact_av"]').is(':checked')) {
							var numberorder_FA = "<?php echo $total_FA ?>";
							var after = '-' + currentdate;
							var after = numberorder_FA + after;
							$('#nouv_fact').attr('value', after); 
						}
						
					});
				});
				</script>









			<input class="droit" id="nouv_fact" type="text" name="nouv_fact" value="<?php echo $nouv_num_fact;?>"/></p>
			
			<p><label class="gauche" for="date_fact">Date saisie de <?php echo $type_aff; ?></label>
			<input class="droit" id="date_fact" type="text" name="date_fact" value="<?php echo $date_fact;?>"/></p>
			
			<p class="cen">Statut : <strong><?php echo $etat; ?></strong></p>
			
			<p>Date de validit&eacute;</p>
				
			<p><label class="gauche" for="date_valid_deb">Du :</label>
			<input class="droit" id="date_valid_deb" type="date" name="date_valid_deb" value="<?php echo $date_valid_deb;?>"/></p>
			
			<p><label class="gauche" for="date_valid_fin">Au :</label>
			<input class="droit" id="date_valid_fin" type="date" name="date_valid_fin" value="<?php echo $date_valid_fin;?>"/></p>
			
<?php
		
		if ($type=="devis")
			{
			echo '<p class="cen">Transformer en : <input class="on_change" type="radio" name="transform" value="commande"/> Commande ? <input class="on_change" type="radio" name="transform" value="facture"/> Facture ? <input class="on_change" type="radio" name="transform" value="fact_av"/> Facture d\'avoirs ?</p>
			
			<p class="cen"><strong>Cocher une case transformera le devis en commande ou facture</strong></p>';
			}
			
		if ($type=="commande")
			{
			echo '<p class="cen">Transformer en : <input class="on_change" type="radio" name="transform" value="devis"/> Devis ? <input class="on_change" type="radio" name="transform" value="facture"/> Facture ? <input class="on_change" type="radio" name="transform" value="fact_av"/> Facture d\'avoirs ?</p>
			
			<p class="cen"><strong>Cocher une case transformera la commande en devis ou facture</strong></p>';
			}
			
		if ($type=="facture")
			{
			if ($num_fact_det[0]=="FA")
				{
				echo '<p class="cen">Transformer en : <input class="on_change" type="radio" name="transform" value="devis"/> Devis ? <input class="on_change" type="radio" name="transform" value="commande"/> Commande ? <input class="on_change" type="radio" name="transform" value="facture"/> Facture ?</p>';
				}
			else
				{
				echo '<p class="cen">Transformer en : <input class="on_change" type="radio" name="transform" value="devis"/> Devis ? <input class="on_change" type="radio" name="transform" value="commande"/> Commande ? <input class="on_change" type="radio" name="transform" value="fact_av"/> Facture d\'avoirs ?</p>';
				}
			
			echo '<p class="cen"><strong>Cocher une case transformera la facture en devis ou commande</strong></p>
			
			<p><label class="gauche" for="type_paiement">Type de paiement (si facture):</label>
					<select class="droit" id="type_paiement" name="type_paiement">
						<option value="'.$type_paiement.'">'.stripslashes($type_paiement).'</option>
						<option value="carte">carte</option>
						<option value="ch&egrave;que">ch&egrave;que</option>
						<option value="esp&egrave;ces">esp&egrave;ces</option>
						<option value="autre">autre</option>
					</select>
				</p>';
			}
			
?>
			<p class="cen"><input type="submit" name="modif_fact" value="Modifier"/><input type="reset" value="R&eacute;initialiser"/></p>
			
			</form>
						
		</fieldset>
	
		<fieldset>
				
			<legend class="lg"> D&eacute;tail des produits et prestations : </legend>
				
			<table>
				
				<tr>
					
					<th>Designation | R&eacute;ference</th>
					<th>Quantit&eacute;</th>
					<th>Prix Unitaire</th>
					<th>Tot HT</th>
					<th>Remise</th>
					<th>Tot TTC</th>
					<th></th>
					
				</tr>

<?php
			
				for($K=0; $K < $nbenreg3; $K++) 
					{	
					if (substr($Tabdo3[$K][6], -1)=="%")
						{
						$Net_prod=$Tabdo3[$K][9]-($Tabdo3[$K][9]*(substr($Tabdo3[$K][6], 0, -1)/100));
						$Rem_tot=$Rem_tot+($Tabdo3[$K][9]*(substr($Tabdo3[$K][6], 0, -1)/100));
						}
					else
						{
						$Net_prod=$Tabdo3[$K][9]-$Tabdo3[$K][6];
						$Rem_tot=$Rem_tot+$Tabdo3[$K][6];
						}
					$Net_tot=$Net_tot+$Net_prod;
					echo '<tr>
						<form action="req_cons_fact.php" method="post">
						<input type="hidden" name="ref_fact_det" value="'.$Tabdo3[$K][0].'" />
						<input type="hidden" name="num_fact" value="'.$num_fact.'" />
						<input type="hidden" name="prod_anc" value="'.$Tabdo3[$K][1].' | '.$Tabdo3[$K][2].'" />
						<input type="hidden" name="prix_prod_anc" value="'.$Tabdo3[$K][3].'" />
						<input type="hidden" name="quant_prod_anc" value="'.$Tabdo3[$K][4].'" />
						<input type="hidden" name="remise_prod_anc" value="'.$Tabdo3[$K][6].'" />
							<td>
								<p><select class="cons" name="prod">
									<option value="'.$Tabdo3[$K][1].' | '.$Tabdo3[$K][2].'">'.$Tabdo3[$K][1].' | '.$Tabdo3[$K][2].'</option>';
									for($N=0; $N < $nbenreg6; $N++) 
										{		
										echo '<option value="'.$Tabdo6[$N][1].' | '.$Tabdo6[$N][2].'">'.$Tabdo6[$N][1].' | '.$Tabdo6[$N][2].'</option>';
										}
								echo '</select></p>
								<p><textarea class="doc" id="info_comp_prod" name="info_comp_prod">'.$Tabdo3[$K][10].'</textarea></p>
							</td>
							<td><input style="width:40px;" type="text" name="quant_prod" value="'.$Tabdo3[$K][4].'"/></td>
							<td><input style="width:40px;" type="text" name="prix_prod" value="'.$Tabdo3[$K][3].'"/> &euro;</td>
							<td>'.$Tabdo3[$K][7].' &euro;</td>
							<td><input style="width:40px;" type="text" name="remise_prod" value="'.$Tabdo3[$K][6].'"/></td>
							<td><p>'.$Tabdo3[$K][9].' &euro;</p><p><strong>Net : '.$Net_prod.' &euro;</strong></p></td>
							<td><input type="submit" value="Modifier"/>
						</form>
						<form action="req_cons_fact.php" method="post">
						<input type="hidden" name="num_fact" value="'.$num_fact.'" />
						<input type="hidden" name="ref_fact_det" value="'.$Tabdo3[$K][0].'" />
						<input type="hidden" name="prod_anc" value="'.$Tabdo3[$K][1].' | '.$Tabdo3[$K][2].'" />
						<input type="hidden" name="quant_prod_anc" value="'.$Tabdo3[$K][4].'" />
							<input type="submit" name="supp_fact" value="Supprimer"/></td>
						</form>
						</tr>';
					}
			
				for($L=0; $L < $nbenreg4; $L++) 
					{	
					if (substr($Tabdo4[$L][6], -1)=="%")
						{
						$Net_prest=$Tabdo4[$L][9]-($Tabdo4[$L][9]*(substr($Tabdo4[$L][6], 0, -1)/100));
						$Rem_tot=$Rem_tot+($Tabdo4[$L][9]*(substr($Tabdo4[$L][6], 0, -1)/100));
						}
					else
						{
						$Net_prest=$Tabdo4[$L][9]-$Tabdo4[$L][6];
						$Rem_tot=$Rem_tot+$Tabdo4[$L][6];
						}
					$Net_tot=$Net_tot+$Net_prest;
					echo '<tr>
						<form action="req_cons_fact.php" method="post">
						<input type="hidden" name="ref_fact_det" value="'.$Tabdo4[$L][0].'" />
						<input type="hidden" name="num_fact" value="'.$num_fact.'" />
						<input type="hidden" name="prest_anc" value="'.$Tabdo4[$L][1].' | '.$Tabdo4[$L][2].'" />
						<input type="hidden" name="prix_prest_anc" value="'.$Tabdo4[$L][3].'" />
						<input type="hidden" name="quant_prest_anc" value="'.$Tabdo4[$L][4].'" />
						<input type="hidden" name="remise_prest_anc" value="'.$Tabdo4[$L][6].'" />
							<td>
								<p><select class="cons" name="prest">
									<option value="'.$Tabdo4[$L][1].' | '.$Tabdo4[$L][2].'">'.$Tabdo4[$L][1].' | '.$Tabdo4[$L][2].'</option>';
									for($O=0; $O < $nbenreg7; $O++) 
										{		
										echo '<option value="'.$Tabdo7[$O][1].' | '.$Tabdo7[$O][2].'">'.$Tabdo7[$O][1].' | '.$Tabdo7[$O][2].'</option>';
										}
								echo '</select></p>
								<p><textarea class="doc" id="info_comp_prod" name="info_comp_prod">'.$Tabdo4[$L][10].'</textarea></p>
							</td>
							<td><input style="width:40px;" type="text" name="quant_prest" value="'.$Tabdo4[$L][4].'"/></td>
							<td><input style="width:40px;" type="text" name="prix_prest" value="'.$Tabdo4[$L][3].'"/> &euro;</td>
							<td>'.$Tabdo4[$L][7].' &euro;</td>
							<td><input style="width:40px;" type="text" name="remise_prest" value="'.$Tabdo4[$L][6].'"/></td>
							<td><p>'.$Tabdo4[$L][9].' &euro;</p><p><strong>Net : '.$Net_prest.' &euro;</strong></p></td>
							<td><input type="submit" value="Modifier"/>
						</form>
						<form action="req_cons_fact.php" method="post">
						<input type="hidden" name="num_fact" value="'.$num_fact.'" />
						<input type="hidden" name="ref_fact_det" value="'.$Tabdo4[$L][0].'" />
						<input type="hidden" name="prest_anc" value="'.$Tabdo4[$L][1].' | '.$Tabdo4[$L][2].'" />
						<input type="hidden" name="quant_prest_anc" value="'.$Tabdo4[$L][4].'" />
							<input type="submit" name="supp_fact" value="Supprimer"/></td>
						</form>
						</tr>';
					}
				
			echo '</table>
			
		</fieldset>

		<fieldset>
				
			<legend class="lg"> Ajout de produits et prestations : </legend>
				
				<table>
						
						<tr>
					
							<th>Designation | R&eacute;ference</th>
							<th>Quantit&eacute;</th>
							<th>Prix Unitaire</th>
							<th>Remise</th>
							<th></th>
							
						</tr>
				
						<tr>
							<td colspan="7"><strong>Ajout d\'un produit suppl&eacute;mentaire</strong></td>
						</tr>
						
						<tr>
						<form action="req_cons_fact.php" method="post">
						<input type="hidden" name="num_fact" value="'.$num_fact.'" />
							<td>
								<p><select class="cons" name="prod_crea">
									<option value=""></option>';
									for($N=0; $N < $nbenreg6; $N++) 
										{		
										echo '<option value="'.$Tabdo6[$N][1].' | '.$Tabdo6[$N][2].'">'.$Tabdo6[$N][1].' | '.$Tabdo6[$N][2].'</option>';
										}
								echo '</select></p>
								<p><textarea class="doc" id="info_comp_prod_crea" name="info_comp_prod_crea"></textarea></p>
							</td>
							<td><input style="width:40px;" type="text" name="quant_prod_crea" /></td>
							<td><input style="width:40px;" type="text" name="prix_prod_crea" /> &euro;</td>
							<td><input style="width:40px;" type="text" name="remise_prod" /></td>
							<td colspan="2"><input type="submit" value="Ajout"/></td>
						</form>
						</tr>';
						
					echo '<tr>
							<td colspan="7"><strong>Ajout d\'une prestation suppl&eacute;mentaire</strong></td>
						</tr>
						
						<tr>
						<form action="req_cons_fact.php" method="post">
						<input type="hidden" name="num_fact" value="'.$num_fact.'" />
							<td>
								<p><select class="cons" name="prest_crea">
									<option value=""></option>';
									for($O=0; $O < $nbenreg7; $O++) 
										{		
										echo '<option value="'.$Tabdo7[$O][1].' | '.$Tabdo7[$O][2].'">'.$Tabdo7[$O][1].' | '.$Tabdo7[$O][2].'</option>';
										}
								echo '</select></p>
								<p><textarea class="doc" id="info_comp_prest_crea" name="info_comp_prest_crea"></textarea></p>
							</td>
							<td><input style="width:40px;" type="text" name="quant_prest_crea" /></td>
							<td><input style="width:40px;" type="text" name="prix_prest_crea" /> &euro;</td>
							<td><input style="width:40px;" type="text" name="remise_prest" /></td>
							<td colspan="2"><input type="submit" value="Ajout"/></td>
						</form>
						</tr>';
			
?>
				
			</table>
			
		</fieldset>		
	
	<fieldset>
				
		<legend class="lg"> Totaux : </legend>
			
		<table>
			
			<tr>
			
				<th>Total HT</th>

<?php
			if (substr($remise, -1)=="%")
				{
				$Rem_tot=$Rem_tot+($Net_tot*(substr($remise, 0, -1)/100));
				$Net_tot=$Net_tot-($Net_tot*(substr($remise, 0, -1)/100));
				}
			else
				{
				$Rem_tot=$Rem_tot+$remise;
				$Net_tot=$Net_tot-$remise;
				}
				
			if ($total_TVA1!="0")
				{
				echo '<th>Tot TVA 20%</th>';
				}
			if ($total_TVA2!="0")
				{
				echo '<th>Tot TVA 10%</th>';
				}
			if ($total_TVA3!="0")
				{
				echo '<th>Tot TVA 5.5%</th>';
				}
			if ($total_TVA4!="0")
				{
				echo '<th>Tot TVA 2.1%</th>';
				}

			echo '<th>Total TTC</th>';
			
			if ($Rem_tot!="0")
				{
				echo '<th>Remise totale</th>';
				}
				
			if ($avoir!="0")
				{
				echo '<th>Avoirs</th>';
				}
		
		echo '<th>Net</th>
		
		</tr>

		<tr>
				
			<td>'.$total_HT.' &euro;</td>';

			if ($total_TVA1!="0")
				{
				echo '<td>'.$total_TVA1.' &euro;</td>';
				}
			if ($total_TVA2!="0")
				{
				echo '<td>'.$total_TVA2.' &euro;</td>';
				}
			if ($total_TVA3!="0")
				{
				echo '<td>'.$total_TVA3.' &euro;</td>';
				}
			if ($total_TVA4!="0")
				{
				echo '<td>'.$total_TVA4.' &euro;</td>';
				}

			echo '<td>'.$total_TTC.' &euro;</td>';
			
			$Rem_tot=round($Rem_tot, 2);
			
			if ($Rem_tot!="0")
				{
				echo '<td>'.$Rem_tot.' &euro;</td>';
				}
				
			if ($avoir!="0")
				{
				echo '<td>'.$avoir.' &euro;</td>';
				}
				
			$Net_tot=$Net_tot-$avoir;
			$Net_tot=round($Net_tot, 2);
				
			echo '<td>'.$Net_tot.' &euro;</td>';	

?>
			
			</tr>
			
		</table>

	</fieldset>
	
	<fieldset>
		
		<legend> Remise globale : </legend>
		
		<form action="req_cons_fact.php" method="post">
		<input type="hidden" name="num_fact" value="<?php echo $num_fact;?>" />
		<input type="hidden" name="remise_anc" value="<?php echo $remise;?>" />
		<input type="hidden" name="avoir_anc" value="<?php echo $avoir;?>" />
		<input type="hidden" name="date_fact" value="<?php echo $date_fact;?>" />
		<input type="hidden" name="type_paiement" value="<?php echo $type_paiement;?>" />
		
		<p><label class="gauche" for="remise_glob">Remise globale (hors remise produits/prestations, &eacute;crire "0" pour effacer la remise actuelle):</label>
		<input class="droit" id="remise_glob" type="text" name="remise_glob" value="<?php echo $remise;?>"/></p>
		
		<p class="cen"><input type="submit" name="modif_fact" value="Modifier"/><input type="reset" value="R&eacute;initialiser"/></p>
			
		</form>
			
	</fieldset>
	
	<fieldset>
					
		<legend> Validation : </legend>
<?php
		if ($avoir!="")
			{
			if ($avoir!="0")
				{
				echo '<form action="imprime_fact_avoir.php" target="_blank" method="post">
						<p class="cen">
							<input type="hidden" name="numfact" id="numfact" value="'.$ListFact.'" />
							<input type="submit" value="Imprimer avoirs"/>
						</p>
					</form>';
				}
			}
?>
			<form action="imprime_fact.php" target="_blank" method="post">
				<p class="cen">
					<input type="hidden" name="numfact" id="numfact" value="<?php echo $ListFact;?>" />
					<input type="submit" value="Imprimer"/>
				</p>
			</form>
			<form action="/pdf_export/render2" target="_blank" method="post">
				<p class="cen">
					<input type="hidden" name="numfact2" id="numfact" value="<?php echo $ListFact;?>" />
					<input type="submit" value="Imprimer PDF"/>
				</p>
			</form>
						
	</fieldset>
	
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>