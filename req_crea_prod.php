<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';

	if(isset($_POST['designation']))      $designation=$_POST['designation'];
	else      $designation="";
	
	if(isset($_POST['nature']))      $nature=$_POST['nature'];
	else      $nature="";
	
	if(isset($_POST['reference']))      $reference=$_POST['reference'];
	else      $reference="";
	
	if(isset($_POST['informations']))      $informations=$_POST['informations'];
	else      $informations="";
	
	if(isset($_POST['quantite']))      $quantite=$_POST['quantite'];
	else      $quantite="";
	
	if(isset($_POST['qte_limite']))      $qte_limite=$_POST['qte_limite'];
	else      $qte_limite="";
	
	if(isset($_POST['prix_achat']))      $prix_achat=$_POST['prix_achat'];
	else      $prix_achat="";
	
	if(isset($_POST['prix_vente']))      $prix_vente=$_POST['prix_vente'];
	else      $prix_vente="";
	
	if(isset($_POST['prix_TTC']))      $prix_TTC=$_POST['prix_TTC'];
	else      $prix_TTC="";
	
	if(isset($_POST['coef']))      $coef=$_POST['coef'];
	else      $coef="";
	
	if(isset($_POST['taux_TVA']))      $taux_TVA=$_POST['taux_TVA'];
	else      $taux_TVA="";
	
	if(isset($_POST['remise']))      $remise=$_POST['remise'];
	else      $remise="";
	
	if(isset($_POST['ref_fournisseur']))      $ref_fournisseur=$_POST['ref_fournisseur'];
	else      $ref_fournisseur="";
	
	if(isset($_POST['ref_prod_fournisseur']))      $ref_prod_fournisseur=$_POST['ref_prod_fournisseur'];
	else      $ref_prod_fournisseur="";

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	

	$designation = mysqli_real_escape_string($db, $designation);
	$nature = mysqli_real_escape_string($db, $nature);
	$reference = mysqli_real_escape_string($db, $reference);
	$informations_rec = $informations;
	$informations = mysqli_real_escape_string($db, $informations);
	$quantite = mysqli_real_escape_string($db, $quantite);
	$qte_limite = mysqli_real_escape_string($db, $qte_limite);
	$prix_achat = mysqli_real_escape_string($db, $prix_achat);
	$prix_vente = mysqli_real_escape_string($db, $prix_vente);
	$coef = mysqli_real_escape_string($db, $coef);
	$taux_TVA = mysqli_real_escape_string($db, $taux_TVA);
	$prix_TTC = mysqli_real_escape_string($db, $prix_TTC);
	$remise = mysqli_real_escape_string($db, $remise);
	$ref_fournisseur = mysqli_real_escape_string($db, $ref_fournisseur);
	$ref_prod_fournisseur = mysqli_real_escape_string($db, $ref_prod_fournisseur);

?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
				<h2>Ajout produit</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter un nouveau produit</strong>.
				</p>		
			
			</div>
			
<?php
	
	if ($prix_vente=="")
		{
		if ($nature=="produit")
			{
			$prix_vente=$prix_achat*$coef;
			}
		else
			{
			$prix_vente=$prix_vente;
			}
		}
	else
		{
		if ($prix_achat=="" && $coef=="")
			{
			$prix_vente=$prix_vente;
			}
		else
			{
			$prix_vente=$prix_achat*$coef;
			}
		}
		
	if ($prix_TTC=="")
		{
		$prix_TTC=$prix_vente+($prix_vente*($taux_TVA/100));
		}
	else
		{
		$prix_vente=$prix_TTC/(1+($taux_TVA/100));
		}

	$prix_vente=round($prix_vente, 2);
	$prix_TTC=round($prix_TTC, 2);
	
	if (substr($remise, -1)=="%")
		{
		$prix_net=$prix_TTC-($prix_TTC*(substr($remise, 0, -1)/100));
		}
	else
		{
		$prix_net=$prix_TTC-$remise;
		}
	
	mysqli_query($db, "INSERT into $db_prod_prest (designation, nature, reference, informations, quantite, qte_limite, prix_achat, coef, prix_vente, taux_TVA, remise, prix_TTC, ref_fournisseur, ref_prod_fournisseur) VALUES ('$designation','$nature','$reference','$informations','$quantite','$qte_limite','$prix_achat','$coef','$prix_vente','$taux_TVA','$remise','$prix_TTC','$ref_fournisseur','$ref_prod_fournisseur')")
	or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
	
	$Requete2 = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur ='$ref_fournisseur'";
	$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
	$Donnees2 = mysqli_fetch_array($ResReq2);
	$nom_fournisseur=$Donnees2["nom"];
	
?>

	<p class="cen"><span class="validation">Informations enregistr&eacute;es</span></p>
	
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif : </legend>
				
				<p>D&eacute;signation : <strong><?php echo $designation;?></strong></p>
				
				<p>Nature : <strong><?php echo $nature;?></strong></p>
				
				<p>R&eacute;f&eacute;rence : <strong><?php echo $reference;?></strong></p>
				
				<p>Informations : <strong><?php echo $informations_rec;?></strong></p>
				
				<p>Quantit&eacute; : <strong><?php echo $quantite;?></strong></p>
				
				<p>Quantit&eacute; limite : <strong><?php echo $qte_limite;?></strong></p>
				
				<p>Prix d'achat : <strong><?php echo $prix_achat;?> &euro;</strong></p>
				
				<p>Coefficient : <strong><?php echo $coef;?></strong></p>
				
				<p>Prix hors taxes : <strong><?php echo $prix_vente;?> &euro;</strong></p>
				
				<p>Taux TVA : <strong><?php echo $taux_TVA;?>%</strong></p>
				
				<p>Remise : <strong><?php echo $remise;?></strong></p>
				
				<p>Fournisseur : <strong><?php echo $nom_fournisseur;?></strong></p>
				
				<p>R&eacute;f&eacute;rence fournisseur : <strong><?php echo $ref_prod_fournisseur;?></strong></p>
				
				<p>Prix TTC : <strong><?php echo $prix_TTC;?> &euro;</strong></p>
				
				<p>Prix net : <strong><?php echo $prix_net;?> &euro;</strong></p>
		
			</fieldset>
	
		<p class="cen"><a href="crea_prod.php">Revenir au formulaire d'ajout</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
	
		</div>
	
	</div>
		
<?php
		}
require_once 'Main_ft.php'; 
?>