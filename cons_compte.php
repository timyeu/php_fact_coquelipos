<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation du compte</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier votre compte</strong>.
				</p>
				
			</div>

<?php

			$date_agend=date('m/Y');
			$date_memo=date('d/m/Y');

			$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
			mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
					
			$Requete = "SELECT * FROM $db_membres WHERE nom ='$nom_membre'";
			$Requete_fact = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='facture' ORDER by num_fact";
			$Requete_dev = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='devis' ORDER by num_fact";
			$Requete_comm = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='commande' ORDER by num_fact";	
			$Requete_agend = "SELECT * FROM $db_agenda WHERE nom_membre ='$nom_membre' AND date_complete LIKE '%".$date_agend."' ORDER BY STR_TO_DATE(date_complete, '%d/%m/%Y')";
			$Requete_doc_val = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND STR_TO_DATE(date_valid_fin, '%d/%m/%Y')<= STR_TO_DATE('$date_memo', '%d/%m/%Y') ORDER by num_fact";
	
			$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>'); 
			$ResReq_fact = mysqli_query($db, $Requete_fact) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>'); 
			$ResReq_dev = mysqli_query($db, $Requete_dev) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>'); 
			$ResReq_comm = mysqli_query($db, $Requete_comm) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>'); 

			$Donnees = mysqli_fetch_array($ResReq);
		
			$ref=$Donnees["ref"];
			$id=$Donnees["id"];
			$mdp=$Donnees["mdp"];
			$nom=$Donnees["nom"];
			$memo=$Donnees["memo"];
			$info_admin=$Donnees["info_admin"];
			$info_facture=$Donnees["info_facture"];
			$info_devis=$Donnees["info_devis"];
			$info_commande=$Donnees["info_commande"];
			
			$ResReq_agend = mysqli_query($db, $Requete_agend) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
			$nbenreg_agend = mysqli_num_rows($ResReq_agend);
			$nbchamps_agend = mysqli_num_fields($ResReq_agend);
			$Tabdo_agend[$nbenreg_agend][$nbchamps_agend]="";
			$Tabchamps_agend[$nbchamps_agend]="";

			for($I=0; $I < $nbchamps_agend; $I++) 
				{
				$tabchamps_agend[$I] = mysqli_fetch_field_direct($ResReq_agend,$I);
				}
	
			$I=0;
			
			while ($donnees_agend = mysqli_fetch_array($ResReq_agend))
				{	
				$Tabdo_agend[$I][1]=$donnees_agend["ref"];
				$Tabdo_agend[$I][2]=$donnees_agend["nom_membre"];
				$Tabdo_agend[$I][3]=$donnees_agend["date_complete"];
				$Tabdo_agend[$I][4]=$donnees_agend["intit_action"];
				$Tabdo_agend[$I][5]=$donnees_agend["horaire_deb"];
				$Tabdo_agend[$I][6]=$donnees_agend["horaire_fin"];
				$Tabdo_agend[$I][7]=$donnees_agend["action"];
				$Tabdo_agend[$I][8]=$donnees_agend["ListCli"];
				$Tabdo_agend[$I][9]=$donnees_agend["ListFour"];
				$Tabdo_agend[$I][10]=$donnees_agend["ListProd"];
				$Tabdo_agend[$I][11]=$donnees_agend["ListPrest"];
				$Tabdo_agend[$I][12]=$donnees_agend["detail_agend"];
				$I++;
				}
				
			$ResReq_doc_val = mysqli_query($db, $Requete_doc_val) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>'); 
			$nbenreg_doc_val = mysqli_num_rows($ResReq_doc_val);
			$nbchamps_doc_val = mysqli_num_fields($ResReq_doc_val);
			$Tabdo_doc_val[$nbenreg_doc_val][$nbchamps_doc_val]="";
			$Tabchamps_doc_val[$nbchamps_doc_val]="";

					for($L=0; $L < $nbchamps_doc_val; $L++) 
						{
						$tabchamps_doc_val[$L] = mysqli_fetch_field_direct($ResReq_doc_val,$L);
						}
			
					$L=0;
					
					while ($donnees_doc_val = mysqli_fetch_array($ResReq_doc_val))
						{	
						$Tabdo_doc_val[$L][1]=$donnees_doc_val["num_fact"];
						$Tabdo_doc_val[$L][2]=$donnees_doc_val["date_fact"];
						$Tabdo_doc_val[$L][3]=$donnees_doc_val["date_valid_deb"];
						$Tabdo_doc_val[$L][4]=$donnees_doc_val["date_valid_fin"];
						$Tabdo_doc_val[$L][5]=$donnees_doc_val["nom_client"];
						$Tabdo_doc_val[$L][6]=$donnees_doc_val["type"];
						$Tabdo_doc_val[$L][7]=$donnees_doc_val["etat"];
						$L++;
						}
?>
		
		<fieldset>
			
			<legend> M&eacute;mo : </legend>
			
			<form action="req_cons_compte.php" method="post">
		
				<input type="hidden" name="ref" id="ref" value="<?php echo $ref;?>" />
				
				<p><label class="gauche" for="memo">M&eacute;mo :</label>
				<textarea class="droit_memo" id="memo" name="memo"><?php echo $memo; ?></textarea></p>
					
				<br /><br /><br /><br />

				<p class="cen">
					<input type="submit" value="Modifier"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>
			
			</form>
		
		</fieldset>
		
		<fieldset>
			
			<legend> Votre activit&eacute; : </legend>
			
			<form action="modif_fact.php" method="post">
		
<?php		

			$ld = '<p><label class="gauche" for="liste_fact">Nombre de factures associ&eacute;es : '.$info_facture.'</label>
				<select class="droit" id="liste_fact" name="liste_fact">
				<option value=""></option>';
					while ($LigneDo_fact = mysqli_fetch_array($ResReq_fact)) 
						{
						$Nmnum_fact = $LigneDo_fact["num_fact"];
						
						$ld .= '<option value="'.$Nmnum_fact.'">'.$Nmnum_fact.'</option>';
						}
					$ld .= '</select></p>';
					
					print $ld;
					
			$ld2 = '<p><label class="gauche" for="liste_dev">Nombre de devis associ&eacute;es : '.$info_devis.'</label>
				<select class="droit" id="liste_dev" name="liste_dev">
				<option value=""></option>';
					while ($LigneDo_dev = mysqli_fetch_array($ResReq_dev)) 
						{
						$Nmnum_dev = $LigneDo_dev["num_fact"];
						
						$ld2 .= '<option value="'.$Nmnum_dev.'">'.$Nmnum_dev.'</option>';
						}
					$ld2 .= '</select></p>';
					
					print $ld2;
					
			$ld3 = '<p><label class="gauche" for="liste_comm">Nombre de commandes associ&eacute;es : '.$info_commande.'</label>
				<select class="droit" id="liste_comm" name="liste_comm">
				<option value=""></option>';
					while ($LigneDo_comm = mysqli_fetch_array($ResReq_comm)) 
						{
						$Nmnum_comm = $LigneDo_comm["num_fact"];
						
						$ld3 .= '<option value="'.$Nmnum_comm.'">'.$Nmnum_comm.'</option>';
						}
					$ld3 .= '</select></p>';
					
					print $ld3;
			
?>							
				<p class="cen">
					<input type="submit" value="Consulter"/>
				</p>
			
			</form>
		
		</fieldset>
		
<?php
		if ($nbenreg_doc_val!=0)
			{
?>		
	
		<fieldset>
		
		<legend class="lg"> Ech&eacute;ance des documents : </legend>
		
		<p class="cen"><strong>Appara&icirc;ssent dans ce cadre les documents ayant d&eacute;pass&eacute; leur date d'&eacute;ch&eacute;ance</strong></p>
			
			<table>

<?php
			echo '<tr>
				<th>Num&eacute;ro</th>
				<th>Type</th>
				<th>Date cr&eacute;ation</th>
				<th>Validit&eacute;</th>
				<th>Client</th>
				<th>Etat</th>
			</tr>';
			
			for($L=0; $L < $nbenreg_doc_val; $L++) 
				{
				echo '<form action="supp_fact.php" method="post">
				<input type="hidden" name="ListFact" value="'.$Tabdo_doc_val[$L][1].'" />
				<tr>	
				<td>'.$Tabdo_doc_val[$L][1].'</td>
				<td>'.$Tabdo_doc_val[$L][6].'</td>
				<td>'.$Tabdo_doc_val[$L][2].'</td>
				<td>Du '.$Tabdo_doc_val[$L][3].' au '.$Tabdo_doc_val[$L][4].'</td>';
				if ($Tabdo_doc_val[$L][5]!="")
					{
					$Requete_cli_ech = "SELECT nom FROM $db_compte_client WHERE ref_clients='".$Tabdo_doc_val[$L][5]."'";
					$ResReq_cli_ech = mysqli_query($db, $Requete_cli_ech) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
					$Donnees_cli_ech = mysqli_fetch_array($ResReq_cli_ech);
					$nom_cli_ech=$Donnees_cli_ech["nom"];
					echo '<td>'.$nom_cli_ech.'</td>';
					}
				echo '<td>'.$Tabdo_doc_val[$L][7].'</td>
				<td><input type="submit" name="supp_fact" value="St."/></td>
				</tr>
				</form>';
				}
?>
				
			</table>

		</fieldset>

<?php
			}
		if ($nbenreg_agend!=0)
			{
?>		
	
		<fieldset>
			
			<legend> Agenda : </legend>

			<p class="cen"><strong>Agenda du mois en cours</strong></p>
			
			<table>
				
				<tr>
			
					<th>Date</th>
					<th>Intitul&eacute;</th>
					<th>Horaire</th>
					<th>Concernant</th>
					<th>D&eacute;tail</th>
						
				</tr>

<?php
			for($I=0; $I < $nbenreg_agend; $I++) 
				{
				echo '<tr>
						<td>
							<p><strong>'.$Tabdo_agend[$I][3].'</strong></p>
							<p>
								<form action="req_supp_agenda.php" method="post">
								<input type="hidden" name="ref" value="'.$Tabdo_agend[$I][1].'"/>
								<input type="submit" value="Sup."/></form>
							</p></td>';
				echo '<td>'.$Tabdo_agend[$I][4].'</td>';
				if ($Tabdo_agend[$I][5]!="")
					{
					if ($Tabdo_agend[$I][6]!="")
						{
						echo '<td><p>De <strong>'.$Tabdo_agend[$I][5].'</strong></p>
						<p>Heure fin : <strong>'.$Tabdo_agend[$I][6].'</strong></p></td>';
						}
					else
						{
						echo '<td><p>&Agrave; : <strong>'.$Tabdo_agend[$I][5].'</strong></p></td>';
						}
					}
				else
					{
					echo '<td></td>';
					}
				echo '<td><p>'.$Tabdo_agend[$I][7].'</p>';
				if ($Tabdo_agend[$I][8]!="")
					{
					$Requete_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='".$Tabdo_agend[$I][8]."'";
					$ResReq_cli = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
					$Donnees_cli = mysqli_fetch_array($ResReq_cli);
					$nom_cli=$Donnees_cli["nom"];
					echo '<p>Client : <strong>'.$nom_cli.'</strong></p></td>';
					}
				if ($Tabdo_agend[$I][9]!="")
					{
					$Requete_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='".$Tabdo_agend[$I][9]."'";
					$ResReq_four = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
					$Donnees_four = mysqli_fetch_array($ResReq_four);
					$nom_four=$Donnees_four["nom"];
					echo '<p>Fournisseur : <strong>'.$nom_four.'</strong></p></td>';
					}
				if ($Tabdo_agend[$I][10]!="")
					{
					$Requete_prod = "SELECT designation FROM $db_prod_prest WHERE nature='produit' AND ref_produits='".$Tabdo_agend[$I][10]."'";
					$ResReq_prod = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
					$Donnees_prod = mysqli_fetch_array($ResReq_prod);
					$nom_prod=$Donnees_prod["designation"];
					echo '<p>Produit : <strong>'.$nom_prod.'</strong></p></td>';
					}
				if ($Tabdo_agend[$I][11]!="")
					{
					$Requete_prest = "SELECT designation FROM $db_prod_prest WHERE nature='prestation' AND ref_produits='".$Tabdo_agend[$I][11]."'";
					$ResReq_prest = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
					$Donnees_prest = mysqli_fetch_array($ResReq_prest);
					$nom_prest=$Donnees_prest["designation"];
					echo '<p>Prestation : <strong>'.$nom_prest.'</strong></p></td>';
					}
				echo '<td style="width:50%;">'.$Tabdo_agend[$I][12].'</td>
				</tr>';
				}
?>
					
			</table>
			
		</fieldset>
		
<?php
			}
?>	
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>