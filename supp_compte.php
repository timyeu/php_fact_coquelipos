<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Suppression compte</h2>
			
				<p>
				Ce formulaire vous permet de <strong>supprimer un compte existant</strong>.
				</p>
				
			</div>

<?php

	if (isset($_POST['ListComp'])) $ListComp=$_POST['ListComp'];
		else $ListComp="";
		
	if (isset($_GET['ref'])) $ListComp=$_GET['ref'];
		else $ListComp=$ListComp;
		
	$Requete = "SELECT * FROM $db_membres WHERE ref ='$ListComp'";
		
		if (empty($ListComp))
			{
				echo "Il faut selectionner un compte dans la liste";
			}
			
			else
			{	
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
					
				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>'); 
				
				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref=$Donnees["ref"];
				$nom=$Donnees["nom"];
				$memo=$Donnees["memo"];
			}

?>
			
		<form action="req_supp_compte.php" method="post">
		<input type="hidden" name="ref" id="ref" value="<?php echo $ref;?>" />
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif compte : </legend>
				
				<p>Nom : <strong><?php echo $nom;?></strong></p>
				
				<p>M&eacute;mo : <strong><?php echo $memo;?></strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Supprimer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_supp_compte.php">Revenir &agrave; la liste de suppression des comptes</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>