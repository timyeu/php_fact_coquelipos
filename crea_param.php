<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout param&egrave;tres</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter de nouveaux param&egrave;tres relatifs aux factures, devis et commandes</strong>.
				</p>
				
			</div>
			
		<form enctype="multipart/form-data" action="req_crea_param.php" method="post">
			
			<fieldset>
				
				<legend class="lg"> Param&egrave;tres en-t&ecirc;te : </legend>
				
				<p><label class="gauche" for="nom_entreprise">Raison sociale et nom de l'entreprise :</label>
				<input class="droit" id="nom_entreprise" type="text" name="nom_entreprise"/></p>
				
				<p><label class="gauche" for="add_entreprise">Adresse :</label>
				<input class="droit" id="add_entreprise" type="text" name="add_entreprise"/></p>
				
				<p><label class="gauche" for="cp_ville">Code Postal et ville :</label>
				<input class="droit" id="cp_ville" type="text" name="cp_ville"/></p>
				
				<p><label class="gauche" for="tel">Telephone :</label>
				<input class="droit" id="tel" type="text" name="tel"/></p>
				
				<p><label class="gauche" for="lieu">Lieu d'ex&eacute;cution des travaux :</label>
				<input class="droit" id="lieu" type="text" name="lieu"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA Intra :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra"/></p>
				
				<p><label class="gauche" for="logo_param">Choisir un logo (taille max : 160px par 160px, aucun caract&egrave;res sp&eacute;ciaux) :</label>
				<input class="droit" id="logo_param" type="file" name="logo_param"/></p>
		
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Param&egrave;tres pied de page : </legend>
				
				<p><label class="gauche" for="cond_devis">Conditions de r&egrave;glement, mentions, infos associ&eacute;es (devis) :</label>
				<textarea class="droit" id="cond_devis" name="cond_devis"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="cond_commande">Conditions de r&egrave;glement, mentions, infos associ&eacute;es (commande) :</label>
				<textarea class="droit" id="cond_commande" name="cond_commande"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="cond_facture">Conditions de r&egrave;glement, mentions, infos associ&eacute;es (facture) :</label>
				<textarea class="droit" id="cond_facture" name="cond_facture"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="mentions">Mentions autre :</label>
				<input class="droit" id="mentions" type="text" name="mentions"/></p>	
		
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Cordonn&eacute;es bancaires: </legend>
				
				<p><label class="gauche" for="coord_banque">Banque :</label>
				<input class="droit" id="coord_banque" type="text" name="coord_banque"/></p>
				
				<p><label class="gauche" for="coord_RIB">RIB :</label>
				<input class="droit" id="coord_RIB" type="text" name="coord_RIB"/></p>
				
				<p><label class="gauche" for="coord_IBAN">IBAN :</label>
				<input class="droit" id="coord_IBAN" type="text" name="coord_IBAN"/></p>
				
				<p>Faire appara&icirc;tre ces coordonn&eacute;es sur :</p>
				
				<p class="cen">
					Devis ? <input type="checkbox" name="app_devis" value="AD"/>
					Commande ? <input type="checkbox" name="app_commande" value="AC"/>
					Facture ? <input type="checkbox" name="app_facture" value="AF"/>
				</p>
				
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Cordonn&eacute;es entreprise: </legend>
				
				<p><label class="gauche" for="clause">Clause de r&eacute;serve de propri&eacute;t&eacute; :</label>
				<input class="droit" id="clause" type="text" name="clause"/></p>
				
				<p><label class="gauche" for="capital">Capital social :</label>
				<input class="droit" id="capital" type="text" name="capital"/></p>
				
				<p><label class="gauche" for="SIREN">SIREN :</label>
				<input class="droit" id="SIREN" type="text" name="SIREN"/></p>
				
				<p><label class="gauche" for="RCS">RCS :</label>
				<input class="droit" id="RCS" type="text" name="RCS"/></p>
				
				<p><label class="gauche" for="APE">Code APE :</label>
				<input class="droit" id="APE" type="text" name="APE"/></p>
				
				<p><label class="gauche" for="info_autre">Information autre :</label>
				<input class="droit" id="info_autre" type="text" name="info_autre"/></p>
				
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p><label class="gauche" for="nom_param">Nom des param&egrave;tres :</label>
				<input class="droit" id="nom_param" type="text" name="nom_param"/></p>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Effacer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>