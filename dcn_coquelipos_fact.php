<?php
    // $domain = $_SERVER["SERVER_NAME"];
	$link_domain = $_SERVER["SERVER_NAME"];
	$link_domain_child = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$link_laster = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
	$link_laster = "/".$link_laster;
	$link_domain_child = str_replace($link_laster,"",$link_domain_child);
	$link_domain = $link_domain.$link_domain_child;
?>
<?php
session_start();
date_default_timezone_set('Europe/Paris');
 
$_SESSION = array();
if (isset($_COOKIE[session_name()]))
{setcookie(session_name(),'',time()-4200,'/');}
session_unset();
session_destroy();
header('Location: http://'.$link_domain.'/Accueil.php');
?>
