<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>
	
	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation compte</h2>
			
			</div>

<?php

	if(isset($_POST['ref']))      $ref=$_POST['ref'];
	else      $ref="";
	
	if(isset($_POST['id']))      $id=$_POST['id'];
	else      $id="";
	
	if(isset($_POST['mdp']))      $mdp=$_POST['mdp'];
	else      $mdp="";
	
	if(isset($_POST['nom']))      $nom=$_POST['nom'];
	else      $nom="";
	
	if(isset($_POST['memo']))      $memo=$_POST['memo'];
	else      $memo="";
	
	if(isset($_POST['info_facture']))      $info_facture=$_POST['info_facture'];
	else      $info_facture="";
	
	if(isset($_POST['info_devis']))      $info_devis=$_POST['info_devis'];
	else      $info_devis="";
	
	if(isset($_POST['info_commande']))      $info_commande=$_POST['info_commande'];
	else      $info_commande="";
	
	if(isset($_POST['info_admin']))      $info_admin=$_POST['info_admin'];
	else      $info_admin="";
	
	if(isset($_POST['aut_admin']))      $aut_admin=$_POST['aut_admin'];
	else      $aut_admin="";
	
	if(isset($_POST['del_admin']))      $del_admin=$_POST['del_admin'];
	else      $del_admin="";
	
	if ($aut_admin!="")
		{
		$info_admin=$aut_admin;
		}
		
	if ($del_admin!="")
		{
		$info_admin=$del_admin;
		}

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
					
	$nom = mysqli_real_escape_string($db, $nom);
	$isl = sha1(sha1($id.$mdp));
	$ida = hash("sha512",$id.$isl);
	$mdpa = hash("sha512",$mdp.$isl);
	$del_admin = mysqli_real_escape_string($db, $del_admin);
	$aut_admin = mysqli_real_escape_string($db, $aut_admin);
	$memo = mysqli_real_escape_string($db, $memo);
	
	if ($id != "")
		{
		mysqli_query($db, "UPDATE $db_membres SET nom='$nom', id='$ida', info_admin='$info_admin', memo='$memo' WHERE ref = '$ref'")
		or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
		}
	
	if ($mdp != "")
		{
		mysqli_query($db, "UPDATE $db_membres SET nom='$nom', mdp='$mdpa', info_admin='$info_admin', memo='$memo' WHERE ref = '$ref'")
		or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
		}
		
	if ($id == "" && $mdp == "")
		{
		mysqli_query($db, "UPDATE $db_membres SET nom='$nom', info_admin='$info_admin', memo='$memo' WHERE ref = '$ref'")
		or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
		}
	
	$nom = stripslashes($nom);
	$memo = stripslashes($memo);
		
	echo '<p class="cen"><span class="validation">Informations enregistr&eacute;es</span></p>';
	
?>	
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif compte : </legend>
				
				<p>Nom : <strong><?php echo $nom;?></strong></p>
				
				<p>M&eacute;mo : <strong><?php echo $memo;?></strong></p>
				
				<p>Nombre de factures cr&eacute;es : <?php echo $info_facture?></p>
				
				<p>Nombre de devis cr&eacute;es : <?php echo $info_devis?></p>
				
				<p>Nombre de commandes cr&eacute;es : <?php echo $info_commande?></p>
		
			</fieldset>
			
		<p class="cen"><a href="modif_compte.php?ref=<?php echo $ref;?>">Revenir &agrave; la modification du compte en cours</a></p>
			
		<p class="cen"><a href="liste_modif_compte.php">Revenir &agrave; la liste de modification des comptes</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
	
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>