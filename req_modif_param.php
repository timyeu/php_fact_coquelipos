<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>
	
	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation param&egrave;tres</h2>
			
			</div>

<?php

	if(isset($_POST['ref']))      $ref=$_POST['ref'];
	else      $ref="";

	if(isset($_POST['nom_param']))      $nom_param=$_POST['nom_param'];
	else      $nom_param="";
	
	if(isset($_POST['nom_entreprise']))      $nom_entreprise=$_POST['nom_entreprise'];
	else      $nom_entreprise="";
	
	if(isset($_POST['add_entreprise']))      $add_entreprise=$_POST['add_entreprise'];
	else      $add_entreprise="";
	
	if(isset($_POST['cp_ville']))      $cp_ville=$_POST['cp_ville'];
	else      $cp_ville="";
	
	if(isset($_POST['tel']))      $tel=$_POST['tel'];
	else      $tel="";
	
	if(isset($_POST['lieu']))      $lieu=$_POST['lieu'];
	else      $lieu="";
	
	if(isset($_POST['TVA_intra']))      $TVA_intra=$_POST['TVA_intra'];
	else      $TVA_intra="";
	
	if(isset($_POST['cond_devis']))      $cond_devis=$_POST['cond_devis'];
	else      $cond_devis="";
	
	if(isset($_POST['cond_commande']))      $cond_commande=$_POST['cond_commande'];
	else      $cond_commande="";
	
	if(isset($_POST['cond_facture']))      $cond_facture=$_POST['cond_facture'];
	else      $cond_facture="";
	
	if(isset($_POST['mentions']))      $mentions=$_POST['mentions'];
	else      $mentions="";
	
	if(isset($_POST['coord_banque']))      $coord_banque=$_POST['coord_banque'];
	else      $coord_banque="";
	
	if(isset($_POST['coord_RIB']))      $coord_RIB=$_POST['coord_RIB'];
	else      $coord_RIB="";
	
	if(isset($_POST['coord_IBAN']))      $coord_IBAN=$_POST['coord_IBAN'];
	else      $coord_IBAN="";
	
	if(isset($_POST['app_devis']))      $app_devis=$_POST['app_devis'];
	else      $app_devis="";
	
	if(isset($_POST['app_commande']))      $app_commande=$_POST['app_commande'];
	else      $app_commande="";
	
	if(isset($_POST['app_facture']))      $app_facture=$_POST['app_facture'];
	else      $app_facture="";
	
	if(isset($_POST['clause']))      $clause=$_POST['clause'];
	else      $clause="";
	
	if(isset($_POST['capital']))      $capital=$_POST['capital'];
	else      $capital="";
	
	if(isset($_POST['SIREN']))      $SIREN=$_POST['SIREN'];
	else      $SIREN="";
	
	if(isset($_POST['RCS']))      $RCS=$_POST['RCS'];
	else      $RCS="";
	
	if(isset($_POST['APE']))      $APE=$_POST['APE'];
	else      $APE="";
	
	if(isset($_POST['info_autre']))      $info_autre=$_POST['info_autre'];
	else      $info_autre="";
	
	if(isset($_POST['anc_img']))      $anc_img=$_POST['anc_img'];
	else      $anc_img="";
	
	if(isset($_POST['eff']))      $eff=$_POST['eff'];
	else      $eff="";

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	$nom_param = mysqli_real_escape_string($db, $nom_param);
	$nom_entreprise = mysqli_real_escape_string($db, $nom_entreprise);
	$add_entreprise = mysqli_real_escape_string($db, $add_entreprise);
	$cp_ville = mysqli_real_escape_string($db, $cp_ville);
	$tel = mysqli_real_escape_string($db, $tel);
	$lieu = mysqli_real_escape_string($db, $lieu);
	$TVA_intra = mysqli_real_escape_string($db, $TVA_intra);
	$cond_devis = mysqli_real_escape_string($db, $cond_devis);
	$cond_commande = mysqli_real_escape_string($db, $cond_commande);
	$cond_facture = mysqli_real_escape_string($db, $cond_facture);
	$mentions = mysqli_real_escape_string($db, $mentions);
	$coord_banque = mysqli_real_escape_string($db, $coord_banque);
	$coord_RIB = mysqli_real_escape_string($db, $coord_RIB);
	$coord_IBAN = mysqli_real_escape_string($db, $coord_IBAN);
	$app_devis = mysqli_real_escape_string($db, $app_devis);
	$app_commande = mysqli_real_escape_string($db, $app_commande);
	$app_facture = mysqli_real_escape_string($db, $app_facture);
	$clause = mysqli_real_escape_string($db, $clause);
	$capital = mysqli_real_escape_string($db, $capital);
	$SIREN = mysqli_real_escape_string($db, $SIREN);
	$RCS = mysqli_real_escape_string($db, $RCS);
	$APE = mysqli_real_escape_string($db, $APE);
	$info_autre = mysqli_real_escape_string($db, $info_autre);
		
	$app=$app_devis.'-'.$app_commande.'-'.$app_facture;
	
	if ($_FILES['logo_param']['name'] != "")
		{
			$content_dir = "../fact/Images/";		 
			$tmp_file = $_FILES['logo_param']['tmp_name'];
		 
			if( !is_uploaded_file($tmp_file) )
			{
				exit('<span class="err_bdd">Le fichier est introuvable</span><br /><a href="modif_param.php?ref='.$ref.'">Revenir &agrave; la modification des param&egrave;tres en cours</a>');
			}

			$type_file = $_FILES['logo_param']['type'];
			
			if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'bmp') && !strstr($type_file, 'png') )
			{
				exit('<span class="err_bdd">Format .jpg, .jpeg, .bmp ou .png seulement</span><br /><a href="modif_param.php?ref='.$ref.'">Revenir &agrave; la modification des param&egrave;tres en cours</a>');
			}
			
			$img_size = getimagesize($_FILES['logo_param']['tmp_name']); 
			
			if ($img_size[0] > 160 || $img_size[1] > 160)
				{
				exit('<span class="err_bdd">L\'image est trop volumineuse, taille requise : largeur <= 160 pixels et hauteur <= 160 pixels</span><br /><a href="modif_param.php?ref='.$ref.'">Revenir &agrave; la modification des param&egrave;tres en cours</a>');
				}

			$name_file = $_FILES['logo_param']['name'];
			$Alogo=$name_file;
		 
			if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
			{
				exit('<span class="err_bdd">Impossible de copier le fichier dans '.$content_dir.'</span><br /><a href="modif_param.php?ref='.$ref.'">Revenir &agrave; la modification des param&egrave;tres en cours</a>');
			}
			
			mysqli_query($db, "UPDATE $db_parametres SET nom_param='$nom_param', nom_entreprise='$nom_entreprise', add_entreprise='$add_entreprise', cp_ville='$cp_ville', tel='$tel', lieu='$lieu', TVA_intra='$TVA_intra', cond_devis='$cond_devis', cond_commande='$cond_commande', cond_facture='$cond_facture', mentions='$mentions', coord_banque='$coord_banque', coord_RIB='$coord_RIB', coord_IBAN='$coord_IBAN', app='$app', clause='$clause', capital='$capital', SIREN='$SIREN', RCS='$RCS', APE='$APE', info_autre='$info_autre', logo_param='$Alogo' WHERE ref = '$ref'")
			or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
		}
	else
		{
		if($eff!="")
			{
			$Alogo="";
			mysqli_query($db, "UPDATE $db_parametres SET nom_param='$nom_param', nom_entreprise='$nom_entreprise', add_entreprise='$add_entreprise', cp_ville='$cp_ville', tel='$tel', lieu='$lieu', TVA_intra='$TVA_intra', cond_devis='$cond_devis', cond_commande='$cond_commande', cond_facture='$cond_facture', mentions='$mentions', coord_banque='$coord_banque', coord_RIB='$coord_RIB', coord_IBAN='$coord_IBAN', app='$app', clause='$clause', capital='$capital', SIREN='$SIREN', RCS='$RCS', APE='$APE', info_autre='$info_autre', logo_param='$Alogo' WHERE ref = '$ref'")
			or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
			}
		else
			{
			mysqli_query($db, "UPDATE $db_parametres SET nom_param='$nom_param', nom_entreprise='$nom_entreprise', add_entreprise='$add_entreprise', cp_ville='$cp_ville', tel='$tel', lieu='$lieu', TVA_intra='$TVA_intra', cond_devis='$cond_devis', cond_commande='$cond_commande', cond_facture='$cond_facture', mentions='$mentions', coord_banque='$coord_banque', coord_RIB='$coord_RIB', coord_IBAN='$coord_IBAN', app='$app', clause='$clause', capital='$capital', SIREN='$SIREN', RCS='$RCS', APE='$APE', info_autre='$info_autre' WHERE ref = '$ref'")
			or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
			}
		}
	
	$nom_param = stripslashes($nom_param);
	$nom_entreprise = stripslashes($nom_entreprise);
	$add_entreprise = stripslashes($add_entreprise);
	$cp_ville = stripslashes($cp_ville);
	$tel = stripslashes($tel);
	$lieu = stripslashes($lieu);
	$TVA_intra = stripslashes($TVA_intra);
	$cond_devis = stripslashes($cond_devis);
	$cond_commande = stripslashes($cond_commande);
	$cond_facture = stripslashes($cond_facture);
	$mentions = stripslashes($mentions);
	$coord_banque = stripslashes($coord_banque);
	$coord_RIB = stripslashes($coord_RIB);
	$coord_IBAN = stripslashes($coord_IBAN);
	$clause = stripslashes($clause);
	$capital = stripslashes($capital);
	$SIREN = stripslashes($SIREN);
	$RCS = stripslashes($RCS);
	$APE = stripslashes($APE);
	$info_autre = stripslashes($info_autre);
	
	$cond_devis=nl2br($cond_devis);
	$cond_commande=nl2br($cond_commande);
	$cond_facture=nl2br($cond_facture);
		
	echo '<p class="cen"><span class="validation">Informations enregistr&eacute;es</span></p>';
	
?>	
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif param&egrave;tres : </legend>
				
				<p>Raison sociale et nom de l'entreprise : <strong><?php echo $nom_param;?></strong></p>
				
				<p>Adresse : <strong><?php echo $nom_entreprise;?></strong></p>
				
				<p>Code Postal et ville : <strong><?php echo $add_entreprise;?></strong></p>
				
				<p>Telephone : <strong><?php echo $tel;?></strong></p>
				
				<p>Lieu d'ex&eacute;cution des travaux : <strong><?php echo $lieu;?></strong></p>
				
				<p>TVA Intra : <strong><?php echo $TVA_intra;?></strong></p>
				
				<p>Conditions de r&egrave;glement, mentions, infos associ&eacute;es (devis) : </p>
				
				<p><strong><?php echo $cond_devis;?></strong></p>
				
				<p>Conditions de r&egrave;glement, mentions, infos associ&eacute;es (commande) : </p>
				
				<p><strong><?php echo $cond_commande;?></strong></p>
				
				<p>Conditions de r&egrave;glement, mentions, infos associ&eacute;es (facture) : </p>
				
				<p><strong><?php echo $cond_facture;?></strong></p>
				
				<p>Mentions autre : <strong><?php echo $mentions;?></strong></p>
				
				<p>Banque : <strong><?php echo $coord_banque;?></strong></p>
				
				<p>RIB : <strong><?php echo $coord_RIB;?></strong></p>
				
				<p>IBAN : <strong><?php echo $coord_IBAN;?></strong></p>
				
				<p>Clause de r&eacute;serve de propri&eacute;t&eacute; : <strong><?php echo $clause;?></strong></p>
				
				<p>Capital social : <strong><?php echo $capital;?> &euro;</strong></p>
				
				<p>SIREN : <strong><?php echo $SIREN;?></strong></p>
				
				<p>RCS : <strong><?php echo $RCS;?></strong></p>
				
				<p>APE : <strong><?php echo $APE;?></strong></p>
				
				<p>Information autre : <strong><?php echo $info_autre;?></strong></p>
				
<?php
				if ($_FILES['logo_param']['name'] != "")
					{
					if($eff=="")
						{
						echo '<p class="cen">Image :<strong>'.$_FILES['logo_param']['name'].'</strong></p>
						<p class="cen"><img src="Images/'.$_FILES['logo_param']['name'].'"/></p>';
						}
					else
						{
						echo '<p class="cen"><strong>Pas d\'image</strong></p>';
						}
					}
				else
					{
					if($anc_img!="")
						{
						echo '<p class="cen">Image :<strong>'.$anc_img.'</strong></p>
						<p class="cen"><img src="Images/'.$anc_img.'"/></p>';
						}
					else
						{
						echo '<p class="cen"><strong>Pas d\'image</strong></p>';
						}
					}
?>
		
			</fieldset>
			
		<p class="cen"><a href="modif_param.php?ref=<?php echo $ref;?>">Revenir &agrave; la modification des param&egrave;tres en cours</a></p>
			
		<p class="cen"><a href="liste_modif_param.php">Revenir &agrave; la liste de modification des param&egrave;tres</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
	
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>