-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 26, 2016 at 12:41 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php_fact`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
  `ref` int(11) NOT NULL AUTO_INCREMENT,
  `nom_membre` varchar(100) DEFAULT NULL,
  `date_complete` varchar(20) DEFAULT NULL,
  `horaire_deb` varchar(10) DEFAULT NULL,
  `horaire_fin` varchar(10) DEFAULT NULL,
  `intit_action` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `ListCli` varchar(50) DEFAULT NULL,
  `ListFour` varchar(50) DEFAULT NULL,
  `ListProd` varchar(50) DEFAULT NULL,
  `ListPrest` varchar(50) DEFAULT NULL,
  `detail_agend` text,
  `couleur` text NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`ref`, `nom_membre`, `date_complete`, `horaire_deb`, `horaire_fin`, `intit_action`, `action`, `ListCli`, `ListFour`, `ListProd`, `ListPrest`, `detail_agend`, `couleur`) VALUES
(1, 'Daniel Marechal', '08/11/2014', NULL, NULL, '', 'Rendez-vous', '1', '', '', '', '', ''),
(2, 'Daniel Marechal', '27/11/2014', NULL, NULL, 'Prospection', 'Rendez-vous', '1', '', '15', '', 'Le rappeler pour savoir si il a bien reÃ§u notre offre', ''),
(3, 'Daniel Marechal', '30/11/2014', NULL, NULL, 'A faire', 'Autre', '', '', '', '', 'Appel \r\nArticle de loi Mr Gilson', ''),
(4, 'Daniel Marechal', '24/12/2014', '9:00', '11:00', 'noel', 'Rendez-vous', '', '', '', '', 'essai', ''),
(5, 'Daniel Marechal', '01/09/2015', '', '', 'Relance projet', 'Entretien tÃ©lÃ©phonique', '9', '', '', '', 'Savoir si elle a un nouveau projet', ''),
(6, 'Daniel Marechal', '01/08/2015', '', '', 'essai', 'Rendez-vous', '', '', '', '', 'info', ''),
(7, 'Daniel Marechal', '04/12/2015', '15:50', '16:00', 'essai2', 'Entretien tÃ©lÃ©phonique', '9', '', '', '', 'eer', '');

-- --------------------------------------------------------

--
-- Table structure for table `compte_client`
--

CREATE TABLE IF NOT EXISTS `compte_client` (
  `ref_clients` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `fonc_contact` varchar(100) DEFAULT NULL,
  `contact_autre` varchar(100) DEFAULT NULL,
  `fonc_contact_autre` varchar(100) DEFAULT NULL,
  `nature` varchar(50) DEFAULT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `code_postal` varchar(50) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `tel_portable` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `activite` varchar(100) DEFAULT NULL,
  `site_web` varchar(50) DEFAULT NULL,
  `siret` varchar(100) DEFAULT NULL,
  `TVA_intra` varchar(100) DEFAULT NULL,
  `encours` double DEFAULT NULL,
  `liste_commandes` varchar(500) DEFAULT NULL,
  `nb_facture` double DEFAULT NULL,
  `date_anniv` varchar(10) DEFAULT NULL,
  `date_autre` varchar(10) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `date_crea` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ref_clients`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `compte_client`
--

INSERT INTO `compte_client` (`ref_clients`, `nom`, `contact`, `fonc_contact`, `contact_autre`, `fonc_contact_autre`, `nature`, `adresse`, `ville`, `code_postal`, `tel`, `tel_portable`, `fax`, `mail`, `activite`, `site_web`, `siret`, `TVA_intra`, `encours`, `liste_commandes`, `nb_facture`, `date_anniv`, `date_autre`, `notes`, `date_crea`) VALUES
(1, ' M OPET', NULL, NULL, NULL, NULL, 'professionnel', '', 'LunÃ©ville', '54300', '', '', '', '', '', '', '', '', 0, '', 1, '', '', 'Une solution complÃ¨te : Machines + Logiciel\r\nVÃªtements et chaussures maroquinerie\r\n DÃ¨s que possible\r\nhttps://portal.mvfglobal.com/index.php/leads/lead/4635574b-b2f5-3fed-7780-546f08c8619f', '24/11/2014'),
(2, 'Asfor 55', NULL, NULL, NULL, NULL, 'professionnel', '8, Parc Bradfer ', 'Bar Le Duc', '55 000', '03 29 79 36 55', '', '', '', '', '', '', '', 0, '', 14, '', '', '', '27/11/2014'),
(3, 'DiscothÃ¨que Osiris', 'MR Chardot', '', '', '', 'professionnel', ' 2 Avenue Lyautey', 'Vaucouleurs ', '55140', '', '06 42 18 91 09', '', 'cvianney@gmail.com', '', '', '', '', 0, '', 0, '', '', 'MR Chardot', '04/12/2014'),
(4, 'La Superette', NULL, NULL, NULL, NULL, 'professionnel', ' 42 rue Jeanne d''Arc ', 'Vaucouleurs ', '55140', '', '06 42 18 91 09', '', 'cvianney@gmail.com', '', '', '', '', 0, '', 3, '', '', 'MR Chardot ', '04/12/2014'),
(5, 'Eclat de modes ', 'Pascaline Mansuy  ', '', '', '', 'professionnel', '6 rue du gÃ©nÃ©ral de Gaulle ', 'Vagney', '88 120 ', '', '0652940966 ', '', ' pascaline.mansuy@gmail.com', '', '', '', '', 0, '', 0, '', '', '15/12/2014\r\nJe lui ai fait une dÃ©mo, je fois la recontacter vers le mois de fÃ©vrier et d''ici lÃ  lui envoyer une proposition.', '15/12/2014'),
(6, 'Ossuaire de Douamont', 'Mr Gerard ', NULL, NULL, NULL, 'professionnel', '', '', '', '0645989408', '', '', '', '', '', '', '', 0, '', 0, '', '', '15/12/2014 \r\nIl a un logiciel qui s''appelle XLSOFT. Il n''a pas de maintenance et Ã§a l''inquiÃ¨te.\r\nJe dois aller le voir afin de voir de quelle maniÃ¨re on pourrait l''aider en cas de problÃ¨me.\r\n', '15/12/2014'),
(7, 'LE CASTELLINO', '', '', '', '', 'professionnel', '7 ROUTE CHATILLON', 'CHATEAUVILLAIN', '52120', '03 25 02 58 10', '', '', '', '', '', '', '', 0, '', 1, '', '', '', '20/01/2015'),
(8, 'CONSTRUCTYS OPCA de la construction ', 'Carpentier Karine', '', 'Mr Vincent Fournier', '', 'professionnel', '32 rue Rene Boulanger CS 60033', 'Paris Cedex 10 ', '75483', '0182839500', '', '', 'karine.carpentier@constructys.fr', '', '', '', '', 0, '', 1, '', '', '', '26/01/2015'),
(9, ' David', 'Gabrielle David', '', '', '', 'professionnel', '', '', '57360', '0771130331', '', '', 'gabrielle.david@bbox.fr', '', '', '', '', 0, '', 7, '', '', 'essai 01\r\nessai 02\r\n', '05/02/2015'),
(10, 'TOP DISCOUNT', '', '', '', '', 'professionnel', '3 rue Battant', 'BesanÃ§on', '25 000', '', '07 81 58 14 43', '', '', '', '', '', '', 0, '', 1, '', '', '', '26/02/2015'),
(11, 'OPCAIM', '', '', '', '', 'professionnel', '7 Parc Bradfer BP 70014', 'Bar Le Duc', '55 000', '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '16/04/2015'),
(12, ' SARL ACCESS PHONE 79', '', '', '', '', 'particulier', '5, rue Jean Jacques Rousseau', '', '79000', 'Niort', '', '', '', '', '', '', '', 0, '', 3, '', '', '', '16/04/2015'),
(13, 'Chambre de mÃ©tiers et le l''artisanat de la Haute-', 'Joelle Beaufrez', '', '', '', 'professionnel', '9, rue DecrÃ¨s - BP 12053', 'Chaumont', '52902 cedex 9', '03 25 32 19 77', '06 18 71 12 90', '03 25 32 89 50', 'formation@cma-haute-marne.fr  - ', '', '', '', '', 0, '', 4, '', '', '', '24/04/2015'),
(14, 'Friterie Roche sur Marne', 'Laurent', '', '', '', 'professionnel', '', 'Roche Sur Marne', '', '06 31 95 58 90', '', '', '', '', '', '', '', 0, '', 1, '', '', '', '05/06/2015'),
(15, 'Chambre de mÃ©tiers et de l''artisanat de la Meuse ', 'ThÃ©rÃ¨se BOUTILLOT', 'Directeur du Service Economique', 'Laurence THIMOUY', 'Assistante Formation', 'professionnel', '39 Quai Sadi Carnot,', 'Bar-le-Duc', ' 55000 ', '03 29 79 20 11', '', '', 't.boutillot@cma-meuse.fr', '', '', '', '', 0, '', 3, '', '', 'l.thimouy@cma-meuse.fr', '06/12/2015');

-- --------------------------------------------------------

--
-- Table structure for table `facture_detail`
--

CREATE TABLE IF NOT EXISTS `facture_detail` (
  `ref_fact_det` int(11) NOT NULL AUTO_INCREMENT,
  `num_fact` varchar(40) DEFAULT NULL,
  `date_fact` varchar(10) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `info_comp` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `prix` double DEFAULT NULL,
  `quantite` varchar(50) DEFAULT NULL,
  `taux_TVA` double DEFAULT NULL,
  `remise` varchar(10) DEFAULT NULL,
  `totHT` double DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `totTTC` double DEFAULT NULL,
  PRIMARY KEY (`ref_fact_det`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `facture_detail`
--

INSERT INTO `facture_detail` (`ref_fact_det`, `num_fact`, `date_fact`, `designation`, `reference`, `info_comp`, `type`, `prix`, `quantite`, `taux_TVA`, `remise`, `totHT`, `TVA`, `totTTC`) VALUES
(1, 'FA-18-07-2015', '30/06/2015', 'Formation attachÃ© commercial Bar Le Duc', '', '23 et 24 Juin 2015', 'prestation', 250, '2', 0, '0', 500, 0, 500),
(2, 'FA-18-07-2015', '30/06/2015', 'Formation Agephip Bar Le DUC', '', '2-4-9-10-16 et 25 Juin 2015', 'prestation', 180, '6', 0, '0', 1080, 0, 1080),
(3, 'D-0306-06-2015', '', 'Frais de Port', 'Port', '', 'prestation', 15, '1', 0, '0', 15, 0, 15),
(4, 'D-0306-06-2015', '12/06/2015', 'Solution TACTIL ALL IN ONE ', '', 'Marque matÃ©riel Ã  dÃ©finir selon arrivage. Compris : PC all in one tactil 19 Pouces imprimante the', 'produit', 600, '1', 0, '60', 600, 0, 600),
(5, 'F-38-06-2015', '30/05/2015', 'Formation Agephip Bar Le DUC', '', '5-6-22-29 Mai 2015', 'prestation', 180, '4', 0, '0', 720, 0, 720),
(6, 'F-38-06-2015', '30/05/2015', 'Formation attachÃ© commercial ENR Bar Le Duc', 'ENR', '13 Mai 2015', 'prestation', 250, '1', 0, '0', 250, 0, 250),
(7, 'F-39-06-2015', '11/06/2015', 'Diagnostic accessibilitÃ© ', 'DACMA', 'Liste des diagnostics rÃ©alisÃ© en Annexe', 'prestation', 190, '38', 0, '0', 7220, 0, 7220),
(8, 'D-0206-06-2015', '05/06/2015', 'Solution d''encaissement Location vente', 'locV24', '', 'produit', 44, '36', 0, '0', 1584, 0, 1584),
(9, 'D-0206-06-2015', '05/06/2015', 'Accompte', 'Accompte sur solution d''encaissement tactile', '', 'produit', 300, '1', 0, '0', 300, 0, 300),
(10, 'D-0106-06-2015', '05/06/2015', 'Maintenance logiciel d''encaissement ', '', 'FacturÃ© Manuellement ', 'prestation', 200, '24', 0, '0', 4800, 0, 4800),
(11, 'D-0106-06-2015', '05/06/2015', 'Solution d''encaissement : PC COQUELIPOS+ Logiciel paramÃ©trÃ©, formation, ', '', 'Dans cette configuration, la formation peut-Ãªtre prise en charge par votre OPCA', 'produit', 1790, '1', 20, '0', 1790, 358, 2148),
(12, 'C-11-12-2015', '30/04/2015', 'Formation Agephip Bar Le DUC', '', 'Date : le 29 Avril 2015', 'prestation', 180, '1', 0, '0', 180, 0, 180),
(13, 'C-11-12-2015', '30/04/2015', 'Formation attachÃ© commercial ENR Bar Le Duc', 'ENR', 'Date : 15 et 17 Avril  2015', 'prestation', 250, '2', 0, '0', 500, 0, 500),
(14, 'C-11-12-2015', '30/04/2015', 'Formation Agephip Verdun', '', 'Date : 9 et 13 Avril 2015', 'prestation', 180, '2', 0, '0', 360, 0, 360),
(15, 'C-11-12-2015', '30/04/2015', 'Formation Assistante de direction Verdun', '', 'Date : 1er Avril 2015', 'prestation', 250, '1', 0, '0', 250, 0, 250),
(16, 'F-36-05-2015', '30/04/2015', 'Maintenance logiciel d''encaissement ', '', '', 'prestation', 200, '1', 0, '0', 200, 0, 200),
(17, 'F-36-05-2015', '30/04/2015', 'Logiciel d''encaissement Coquelipos', '', 'IntallÃ© le 10 Avril', 'produit', 250, '1', 0, '0', 250, 0, 250),
(18, 'D-0104-04-2015', '24/04/2015', 'Diagnostic accessibilitÃ© ', 'DACMA', '', 'prestation', 190, '37', 0, '0', 7030, 0, 7030),
(19, 'D-11-04-2015', '16/04/2015', 'Formation accessibilitÃ© ', '', '', 'prestation', 350, '2', 0, '0', 700, 0, 700),
(20, 'F-34-04-2015', '16/04/2015', 'Solution TACTIL ALL IN ONE ', '', '', 'produit', 600, '1', 0, '0', 600, 0, 600),
(25, 'F-43-09-2015', '31/08/2015', 'Fomation BTS MUC', 'F-BTSMUC', '17, 21 matin, 27 et 28 AoÃ»t 2015', 'prestation', 250, '3.5', 0, '', 875, 0, 875),
(26, 'F-43-09-2015', '31/08/2015', 'Formation Agephip Bar Le DUC', '', '19 et 20 AoÃ»t 2015', 'prestation', 180, '2', 0, '', 360, 0, 360),
(23, 'F-70-08-2015', '31/07/2015', 'Formation Agephip Bar Le DUC', '', '09-16-21-22 et 24 matin  Juillet 2015', 'prestation', 180, '4.5', 0, '', 810, 0, 810),
(24, 'F-70-08-2015', '31/07/2015', 'Fomation BTS MUC', 'F-BTSMUC', 'demie journÃ©e le 24 AprÃ¨s midi ', 'prestation', 250, '0.5', 0, '', 125, 0, 125),
(27, 'F-44-10-2015', '30/09/2015', 'Fomation BTS MUC', 'F-BTSMUC', 'Date : 4, 8 (1/2), 15, 22(1/2), 25 Septembre 2015', 'prestation', 250, '4', 0, '', 1000, 0, 1000),
(28, 'F-44-10-2015', '30/09/2015', 'Formation Agephip Bar Le DUC', '', 'Date : 2,9,11,16,23,24 Septembre 2015', 'prestation', 180, '6', 0, '', 1080, 0, 1080),
(29, 'F-45-11-2015', '31/10/2015', 'Formation Agephip Bar Le DUC', '', '07 Octobre 2015', 'prestation', 180, '1', 0, '', 180, 0, 180),
(30, 'F-45-11-2015', '31/10/2015', 'Formation Assistante de direction Verdun', '', '9-29 Octobre 2015', 'prestation', 250, '2', 0, '', 500, 0, 500),
(35, 'F-45-11-2015', '31/10/2015', 'Fomation BTS MUC', 'F-BTSMUC', '13-15-21-23 et 30 Octobre 2015', 'prestation', 250, '5', 0, '', 1250, 0, 1250),
(32, 'F-45-11-2015', '31/10/2015', 'Formation attachÃ© commercial Bar Le Duc', '', '14-22-28 et 29 0ctobre 2015', 'prestation', 250, '4', 0, '', 1000, 0, 1000),
(33, 'F-46-11-2015', '22/11/2015', 'Relance tÃ©lÃ©phonique opÃ©ration :', 'OPTEL', '', 'prestation', 490, '1', 0, '', 490, 0, 490),
(34, 'F-45-11-2015', '31/10/2015', 'Fomation BTS MUC', 'F-BTSMUC', 'Demi JournÃ©e : le 20 Octobre 2015', 'prestation', 125, '1', 0, '', 125, 0, 125),
(36, 'D-0512-12-2015', '02/12/2015', 'Caisse tactile de la marque ELO (matÃ©riel de Demo)', '', 'essai avec ken', 'produit', 750, '1', 0, '', 750, 0, 750),
(37, 'F-47-12-2015', '02/12/2015', 'Fomation BTS MUC', 'F-BTSMUC', '', 'prestation', 250, '1,5', 0, '', 250, 0, 250),
(38, 'D-0612-12-2015', '02/12/2015', 'Accompte', 'Accompte sur solution d''encaissement tactile', 'test', 'produit', 300, '1.5', 0, '', 450, 0, 450),
(39, 'D-0712-12-2015', '03/12/2015', 'Fomation BTS MUC', 'F-BTSMUC', '', 'prestation', 250, '1.5', 0, '', 375, 0, 375),
(40, 'F-49-12-2015', '06/12/2015', 'Formation Defendre ses prix', 'FDP', 'Date : 24 Novembre 2015', 'prestation', 350, '1', 0, '', 350, 0, 350),
(41, 'F-48-12-2015', '06/12/2015', 'Formation Assistante de direction Verdun', '', '3-12- et Novembre 2015', 'prestation', 250, '3', 0, '', 750, 0, 750),
(42, 'F-48-12-2015', '06/12/2015', 'Formation attachÃ© commercial Bar Le Duc', '', '4-5-13-16-19-23-25 et Novembre 2015', 'prestation', 250, '8', 0, '', 2000, 0, 2000),
(43, 'F-48-12-2015', '06/12/2015', 'Fomation BTS MUC', 'F-BTSMUC', '6 et 27  Novembre 2015', 'prestation', 250, '2', 0, '', 500, 0, 500),
(44, 'F-48-12-2015', '06/12/2015', 'Formation Agephip Bar Le DUC', '', '9-10- et 18 Novembre 2015', 'prestation', 180, '3', 0, '', 540, 0, 540),
(45, 'F-50-12-2015', '06/12/2015', 'Formation Assistante de direction Verdun', '', '3-12- et Novembre 2015', 'prestation', 250, '3', 0, '', 750, 0, 750),
(46, 'F-50-12-2015', '06/12/2015', 'Formation attachÃ© commercial Bar Le Duc', '', '4-5-13-16-19-23-25 et 30 Novembre 2015', 'prestation', 250, '8', 0, '', 2000, 0, 2000),
(47, 'F-50-12-2015', '06/12/2015', 'Fomation BTS MUC', 'F-BTSMUC', '6 et 27 Novembre 2015', 'prestation', 250, '2', 0, '', 500, 0, 500),
(48, 'F-50-12-2015', '06/12/2015', 'Formation Agephip Bar Le DUC', '', '9-10 et 18 Novembre 2015', 'prestation', 180, '3', 0, '', 540, 0, 540),
(49, 'F-68-06-2015', '12/06/2015', 'Caisse tactile de la marque ELO (matÃ©riel de Demo)', '', '', 'produit', 750, '1', 0, '', 750, 0, 750),
(50, 'FA-16-12-2015', '04/12/2015', 'Accompte', 'Accompte sur solution d''encaissement tactile', '', 'produit', 300, '1', 0, '', 300, 0, 300),
(51, 'F-67-12-2015', '05/06/2015', 'Accompte', 'Accompte sur solution d''encaissement tactile', '', 'produit', 300, '1', 0, '', 300, 0, 300),
(52, 'FA-08-12-2015', '24/04/2015', 'Accompte', 'Accompte sur solution d''encaissement tactile', '', 'produit', 300, '1', 0, '', 300, 0, 300),
(53, 'C-14-12-2015', '05/06/2015', 'Caisse tactile de la marque ELO (matÃ©riel de Demo)', '', '', 'produit', 750, '1', 0, '', 750, 0, 750),
(54, 'F-64-12-2015', '07/12/2015', 'Accompte', 'Accompte sur solution d''encaissement tactile', 'essai', 'produit', 300, '1', 0, '', 300, 0, 300),
(55, 'D-08-12-2015', '07/12/2015', 'Caisse tactile de la marque ELO (matÃ©riel de Demo)', '', '', 'produit', 750, '1', 0, '', 750, 0, 750),
(56, 'F-65-12-2015', '08/12/2015', 'Caisse tactile de la marque ELO (matÃ©riel de Demo)', '', '', 'produit', 750, '1', 0, '', 750, 0, 750),
(57, 'F-65-12-2015', '08/12/2015', 'Logiciel d''encaissement Coquelipos', '', '', 'produit', 250, '1', 0, '', 250, 0, 250),
(58, 'C-15-12-2015', '04/12/2015', 'Papier Thermique 57*40 Carton de 50', '', '', 'produit', 19, '1', 0, '', 19, 0, 19),
(59, 'F-71-1-2016', '31/12/2015', 'Formation attachÃ© commercial Bar Le Duc', '', '02-9-10-15-17-22-DÃ©cembre 2015', 'prestation', 250, '6', 0, '', 1500, 0, 1500),
(60, 'F-71-1-2016', '31/12/2015', 'Formation Assistante de direction Verdun', '', '3 et 16 DÃ©cembre 2015', 'prestation', 250, '2', 0, '', 500, 0, 500),
(61, 'F-71-1-2016', '31/12/2015', 'Formation attachÃ© commercial ENR Bar Le Duc', 'ENR', '1-18-24-30-31 DÃ©cembre 2015', 'prestation', 250, '5', 0, '', 1250, 0, 1250),
(62, 'F-51-12-2015', '31/12/2015', 'Formation Defendre ses prix', 'FDP', '8 dÃ©cembre 2015', 'prestation', 350, '1', 0, '', 350, 0, 350),
(63, 'F-72-1-2016', '31-12-2015', 'Formation Defendre ses prix', 'FDP', '8 dÃ©cembre 2015', 'prestation', 350, '1', 0, '', 350, 0, 350);

-- --------------------------------------------------------

--
-- Table structure for table `facture_entete`
--

CREATE TABLE IF NOT EXISTS `facture_entete` (
  `num_fact` varchar(40) NOT NULL DEFAULT '',
  `nom_param` varchar(100) DEFAULT NULL,
  `date_fact` varchar(10) DEFAULT NULL,
  `date_valid_deb` varchar(10) DEFAULT NULL,
  `date_valid_fin` varchar(10) DEFAULT NULL,
  `total_HT` double DEFAULT NULL,
  `total_TVA1` double DEFAULT NULL,
  `total_TVA2` double DEFAULT NULL,
  `total_TVA3` double DEFAULT NULL,
  `total_TVA4` double DEFAULT NULL,
  `tot_produits` double DEFAULT NULL,
  `tot_prestas` double DEFAULT NULL,
  `remise` varchar(10) DEFAULT NULL,
  `total_TTC` double DEFAULT NULL,
  `avoir` double DEFAULT NULL,
  `type_paiement` varchar(50) DEFAULT NULL,
  `nom_membre` varchar(100) DEFAULT NULL,
  `nom_client` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`num_fact`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fournisseurs`
--

CREATE TABLE IF NOT EXISTS `fournisseurs` (
  `ref_fournisseur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `code_postal` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  `site_web` varchar(50) DEFAULT NULL,
  `nom_contact` varchar(50) DEFAULT NULL,
  `tel_contact` varchar(50) DEFAULT NULL,
  `siret` varchar(50) DEFAULT NULL,
  `TVA_intra` varchar(50) DEFAULT NULL,
  `prod_prop` text,
  `memo` text,
  PRIMARY KEY (`ref_fournisseur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `ref` int(11) NOT NULL AUTO_INCREMENT,
  `id` text,
  `mdp` text,
  `nom` varchar(100) DEFAULT NULL,
  `memo` text,
  `info_admin` varchar(1) DEFAULT NULL,
  `info_facture` varchar(50) DEFAULT NULL,
  `info_devis` varchar(50) DEFAULT NULL,
  `info_commande` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `membres`
--

INSERT INTO `membres` (`ref`, `id`, `mdp`, `nom`, `memo`, `info_admin`, `info_facture`, `info_devis`, `info_commande`) VALUES
(1, '385c38f2ada28bffdf88cdcaa1fcabcbc2ad75cddbe0b475908d93c5e014e709efd6f9f97161188809e13ba3bddd0c6632cc1328461f3ad015826ed95d6f16cc', 'b4fb1545a9cbffe93728c32b244623093cccad6b3f084f89f6bd315bcb30f110091714da72142976bb85c26330a95010fded897c6eae475a39e2e234538b3c9f', 'Daniel Marechal', 'test essai', '1', '45', '24', '28');

-- --------------------------------------------------------

--
-- Table structure for table `parametres`
--

CREATE TABLE IF NOT EXISTS `parametres` (
  `ref` int(11) NOT NULL AUTO_INCREMENT,
  `nom_param` varchar(100) DEFAULT NULL,
  `nom_entreprise` varchar(100) DEFAULT NULL,
  `add_entreprise` varchar(100) DEFAULT NULL,
  `cp_ville` varchar(100) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `lieu` varchar(100) DEFAULT NULL,
  `TVA_intra` varchar(100) DEFAULT NULL,
  `cond_devis` text,
  `cond_commande` text,
  `cond_facture` text,
  `mentions` varchar(200) DEFAULT NULL,
  `coord_banque` varchar(100) DEFAULT NULL,
  `coord_RIB` varchar(100) DEFAULT NULL,
  `coord_IBAN` varchar(100) DEFAULT NULL,
  `app` varchar(10) DEFAULT NULL,
  `clause` varchar(200) DEFAULT NULL,
  `capital` varchar(100) DEFAULT NULL,
  `SIREN` varchar(100) DEFAULT NULL,
  `RCS` varchar(100) DEFAULT NULL,
  `APE` varchar(100) DEFAULT NULL,
  `info_autre` varchar(200) DEFAULT NULL,
  `logo_param` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `parametres`
--

INSERT INTO `parametres` (`ref`, `nom_param`, `nom_entreprise`, `add_entreprise`, `cp_ville`, `tel`, `lieu`, `TVA_intra`, `cond_devis`, `cond_commande`, `cond_facture`, `mentions`, `coord_banque`, `coord_RIB`, `coord_IBAN`, `app`, `clause`, `capital`, `SIREN`, `RCS`, `APE`, `info_autre`, `logo_param`) VALUES
(1, 'COQUELIPOS', 'Coquelipos Daniel MarÃ©chal', '39 grande rue ', '55 130 Saint joire', '0666662026', '', '', 'RÃ¨glement Ã  rÃ©ception de facture', 'RÃ¨glement Ã  rÃ©ception de facture', 'RÃ¨glement Ã  rÃ©ception de facture', 'T.V.A. non applicable ou exonÃ©rÃ©e, article 293 B du CGI.', 'BPLC Ligny en Barrois', '', 'FR76 1470 7000 4804 8190 5004 076', '--AF', '', '', '382873255', '', '', 'NÂ°dÃ©claration centre de formation : 41550042955', 'coquelipos-logo-128.png'),
(2, 'Evoluty''s', 'Evoluty''s', '33 rue des royaux', '52 300 Joinville', '0666662026', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', 'apply_f2.png'),
(5, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(6, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(7, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(9, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', 'logo-150x96.png'),
(10, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', 'logo-150x96.png'),
(11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', ''),
(12, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '--', '', '', '', '', '', '', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `prod_prest`
--

CREATE TABLE IF NOT EXISTS `prod_prest` (
  `ref_produits` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(200) DEFAULT NULL,
  `reference` varchar(200) DEFAULT NULL,
  `informations` text,
  `nature` varchar(50) DEFAULT NULL,
  `prix_achat` double DEFAULT NULL,
  `prix_vente` double DEFAULT NULL,
  `taux_TVA` double DEFAULT NULL,
  `quantite` double DEFAULT NULL,
  `coef` double DEFAULT NULL,
  `ref_fournisseur` varchar(50) DEFAULT NULL,
  `ref_prod_fournisseur` varchar(50) DEFAULT NULL,
  `remise` varchar(10) DEFAULT NULL,
  `prix_TTC` double DEFAULT NULL,
  `qte_limite` double DEFAULT NULL,
  `qte_vendu` double DEFAULT NULL,
  PRIMARY KEY (`ref_produits`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `prod_prest`
--

INSERT INTO `prod_prest` (`ref_produits`, `designation`, `reference`, `informations`, `nature`, `prix_achat`, `prix_vente`, `taux_TVA`, `quantite`, `coef`, `ref_fournisseur`, `ref_prod_fournisseur`, `remise`, `prix_TTC`, `qte_limite`, `qte_vendu`) VALUES
(2, 'Solution d''encaissement : PC COQUELIPOS+ Logiciel paramÃ©trÃ©, formation, ', '', NULL, 'produit', 0, 1790, 20, 0, 0, '', '', '', 2148, 0, NULL),
(3, 'Formation Assistante de direction Verdun', '', NULL, 'prestation', 0, 250, 0, 0, 0, '', '', '', 250, 0, NULL),
(4, 'Formation Agephip Verdun', '', NULL, 'prestation', 0, 180, 0, 0, 0, '', '', '', 180, 0, NULL),
(5, 'Formation Agephip Bar Le DUC', '', NULL, 'prestation', 0, 180, 0, 0, 0, '', '', '', 180, 0, NULL),
(6, 'Solution ComplÃ©te d''encaissement : PC COQUELIPOS+ Logiciel paramÃ©trÃ©, formation, ', 'PCCO', NULL, 'produit', 0, 0, 20, 0, 0, '', '', '', 0, 0, NULL),
(7, 'Solution ComplÃ©te d''encaissement : logiciel coquelipos + materiel DPOS', 'DPOS', NULL, 'produit', 1790, 1790, 0, 0, 1, '', '', '', 1790, 0, NULL),
(8, 'Formation attachÃ© commercial Bar Le Duc', '', NULL, 'prestation', 0, 250, 0, 0, 0, '', '', '', 250, 0, NULL),
(9, 'Formation attachÃ© commercial ENR Bar Le Duc', 'ENR', '', 'prestation', 0, 250, 0, 0, 0, '', '', '', 250, 0, NULL),
(10, 'Papier Thermique 57*40 Carton de 50', '', '', 'produit', 0, 19, 0, -2, 0, '', '', '', 19, 0, NULL),
(11, 'Papier Thermique 80*80 Carton de 50', '', '', 'produit', 0, 70, 0, -1, 0, '', '', '', 70, 0, NULL),
(12, 'Formation Fermeture du barrois OPCA', '1214004949.01', 'Formation Fermeture du Barrois, Personne FormÃ©e : Arnaud Fauquet. Formation du 08/12/2014 au 18/12/2014. 63 Heures au taux 10.50', 'prestation', 0, 10.5, 0, 0, 0, '', '', '', 10.5, 0, NULL),
(13, 'Formation Fermeture du barrois DIF', '1214004948.01', 'Formation Fermeture du Barrois, Personne FormÃ©e : Arnaud Fauquet. Formation du 08/12/2014 au 18/12/2014. 63 Heures ', 'prestation', 0, 667.95, 0, 0, 0, '', '', '', 667.95, 0, NULL),
(14, 'PC TACTIL ELO 15E1 ONE  vrsion location', 'ELO15E', 'Voire Fiche produit en annexe\r\nValeur neuve du produit 1690  â,¬\r\nOffre en location sur 36 Mois Maintenance comprise . \r\nMontant de la maintenance 22â,¬ ht\r\nPremier loyer de 440 â,¬ ht\r\n', 'produit', 0, 54, 20, 0, 0, '', '', '', 64.8, 0, NULL),
(15, 'Caisse tactile de la marque ELO (matÃ©riel de Demo)', '', 'PC tactile de la marque ELO tout en un.\r\n15,6 Pouces !\r\nIntel Atom,\r\n1,6 GHz,\r\nRAM: 2 Go,\r\ndisque dur: 320 Go,\r\nUSB (4x), RS232 (2x),\r\nEthernet (10/100/1000 Mbits),\r\naudio, VGA\r\nAvec Logiciel Eve conciergerie. \r\nGarantie MatÃ©riel 3 Mois Ã  partir du 1er mars \r\nContenu de l''offre : Logiciel, caisse Ã©lo, imprimante thermique 57*40, douchette code barre, et tiroir caisse.\r\n\r\nAttention toute copie, modification et/ou utilisation du logiciel en dehors du point de vente et ou du MatÃ©riel vendu est strictement interdite. Le nom respect de ces rÃ¨gles fera l''objet de poursuite. Le tribunal compÃ©tant est celui de bar le duc .', 'produit', 0, 750, 0, -3, 0, '', '', '', 750, 0, NULL),
(16, 'Solution TACTIL ALL IN ONE ', '', 'PC Tactil MSI, Logiciel coquelipos', 'produit', 0, 600, 0, -1, 0, '', '', '', 600, 0, NULL),
(17, 'Formation accessibilitÃ© ', '', 'Formation AccessibilitÃ© MR GILSON,\r\nMadame DEHLINGER, Madame Autier\r\n26 et 27 Aout 2014', 'prestation', 0, 350, 0, 0, 0, '', '', '', 350, 0, NULL),
(18, 'Diagnostic accessibilitÃ© ', 'DACMA', 'RÃ©alisation d''un diagnostic sur les conditions d''accessibilitÃ© aux personnes handicapÃ©es : \r\nExamen de lâEUR(TM)Ã©tablissement, \r\nVisite des installations et Ã©quipements concernÃ©s avec le responsable du site visitÃ©,\r\nRÃ©ponses aux principales interrogations,  \r\nMise en lumiÃ¨re des travaux Ã  rÃ©aliser pour se mettre en conformitÃ©.\r\nProposition et estimation financiÃ¨re de solutions. \r\nListe des Ã©tablissements diagnostiquÃ©s en Annexe.\r\nFacturation au fur et Ã  mesure de la rÃ©alisation de la mission.', 'prestation', 0, 190, 0, 0, 0, '', '', '', 190, 0, NULL),
(19, 'Logiciel d''encaissement Coquelipos', '', 'Logiciel dâEUR(TM)encaissement, installation et paramÃ©trage du logiciel sur le matÃ©riel du client', 'produit', 0, 250, 0, -2, 0, '', '', '', 250, 0, NULL),
(20, 'Maintenance logiciel d''encaissement ', '', '1 An Ã  partir de l''ouverture du commerce.\r\nTarif exceptionnel.', 'prestation', 0, 22, 0, 0, 0, '', '', '', 22, 0, NULL),
(21, 'Solution d''encaissement Location vente', 'locV24', 'Solution d''encaissement tout compris matÃ©riel, Logiciel, maintenance.  Cette offre est soumise Ã  un apport d''environ 20 % Ã  la signature. Cette offre est indissociable de l''offre liÃ© Ã  un apport. A l''issue des 24 mois le commerÃ§ant peut dÃ©cider de conserver la maintenance ou pas.', 'produit', 0, 49, 0, 0, 0, '', '', '', 49, 0, NULL),
(22, 'Accompte', 'Accompte sur solution d''encaissement tactile', 'Ce produit est indissociable d''une location vente de 24, 36, ou 48 Mois.', 'produit', 0, 300, 0, -6, 0, '', '', '', 300, 0, NULL),
(23, 'Frais de Port', 'Port', '', 'prestation', 0, 15, 0, 0, 0, '', '', '', 15, 0, NULL),
(24, 'Fomation BTS MUC', 'F-BTSMUC', '', 'prestation', 0, 250, 0, 0, 0, '', '', '', 250, 0, NULL),
(25, 'Relance tÃ©lÃ©phonique opÃ©ration :', 'OPTEL', 'Relance tÃ©lÃ©phonique\r\n- Contacter des artisans\r\n- Leur demander s''il souhaite s''inscrire au concours\r\n"Artisans, promoteurs de gastronimie" organisÃ© par\r\nla CMA de la Mause\r\n- Recueillir les rÃ©ponses\r\n- Fichier fourni (200 contacts maxi)\r\n- Script fourni\r\n- 2 relances si appel non abouti soit 3 appels\r\nLes rÃ©sultats de l''opÃ©ration ont Ã©tÃ© fournis Ã  Laetitia L. de Mianville', 'prestation', 0, 490, 0, 0, 0, '', '', '', 490, 0, NULL),
(26, 'Formation Defendre ses prix', 'FDP', 'Savoir GÃ©rer les derniers instants de la vente pour faire signer le client.', 'prestation', 0, 350, 0, 0, 0, '', '', '', 350, 0, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
