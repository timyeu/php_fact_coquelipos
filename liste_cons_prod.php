<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';

		$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
		mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

		$Requete = "SELECT ref_produits, designation, reference FROM $db_prod_prest WHERE nature='produit' ORDER by designation";
		$Requete2 = "SELECT ref_produits, designation, reference FROM $db_prod_prest WHERE nature='prestation' ORDER by designation";

		$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, pi&egrave;ce incorrecte ou inexistante</span>'); 
		$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, pi&egrave;ce incorrecte ou inexistante</span>'); 

?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation produits/prestations</h2>
			
				<p>
				Ce formulaire vous permet de <strong>filtrer la liste des produits/prestations existantes</strong>.
				</p>
				
			</div>

			<form action="cons_prod.php" method="post" class="edit-form">
			
			<fieldset>
				
				<legend> Filtres : </legend>

				<p><label class="gauche" for="prod">Rechercher des produits commen&ccedil;ant par :</label>
				<input class="droit" id="prod" type="text" name="prod" /></p>
						
				<p><label class="gauche" for="long_liste">Liste des produits (d&eacute;signation | r&eacute;f&eacute;rence) :</label>
					<select class="droit" id="long_liste" name="ListProd">
						<option value=""></option>		
				<?php

						while ($LigneDo = mysqli_fetch_array($ResReq)) 
							{
							$Nmdes = $LigneDo["designation"];
							$Nmref = $LigneDo["reference"];
							echo '<option value="'.$Nmdes.'">'.$Nmdes.' '.$Nmref.'</option>';
							}
				
				?>
					</select></p>
				
				<p><label class="gauche" for="prest">Rechercher des prestations commen&ccedil;ant par :</label>
				<input class="droit" id="prest" type="text" name="prest" /></p>
				
				<p><label class="gauche" for="long_liste2">Liste des prestations (d&eacute;signation | r&eacute;f&eacute;rence) :</label>
					<select class="droit" id="long_liste2" name="ListPrest">
						<option value=""></option>		
				<?php

						while ($LigneDo2 = mysqli_fetch_array($ResReq2)) 
							{
							$Nmdes2 = $LigneDo2["designation"];
							$Nmref2 = $LigneDo2["reference"];
							echo '<option value="'.$Nmdes2.'">'.$Nmdes2.' '.$Nmref2.'</option>';
							}
				
				?>
					</select></p>
				
				<p class="cen"><strong>Utilisez les listes pour afficher directement un nom de produit ou de prestation dans les champs de saisie</strong></p>
				
				<p class="cen"><a href="imprime_prod.php" target="_blank">Afficher et imprimer l'inventaire complet</a></p>
				<p class="cen"><a href="imprime_prod_p.php" target="_blank">Afficher et imprimer l'inventaire par produits</a></p>
				<p class="cen"><a href="imprime_prod_s.php" target="_blank">Afficher et imprimer l'inventaire par prestations</a></p>
				
			</fieldset>
				
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Annuler"/>
				</p>
				
			</fieldset>
			
			</form>
			
			<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
				
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>