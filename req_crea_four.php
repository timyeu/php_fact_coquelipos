<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout fournisseurs</h2>
			
			</div>
			
<?php
		
//Récupération des variables
	
	if(isset($_POST['nom']))      $nom=$_POST['nom'];
	else      $nom="";
	
	if(isset($_POST['adresse']))      $adresse=$_POST['adresse'];
	else      $adresse="";
	
	if(isset($_POST['ville']))      $ville=$_POST['ville'];
	else      $ville="";

	if(isset($_POST['code_postal']))      $code_postal=$_POST['code_postal'];
	else      $code_postal="";
	
	if(isset($_POST['telephone']))      $telephone=$_POST['telephone'];
	else      $telephone="";
	
	if(isset($_POST['fax']))      $fax=$_POST['fax'];
	else      $fax="";
	
	if(isset($_POST['email']))      $email=$_POST['email'];
	else      $email="";
	
	if(isset($_POST['pays']))      $pays=$_POST['pays'];
	else      $pays="";
	
	if(isset($_POST['site_web']))      $site_web=$_POST['site_web'];
	else      $site_web="";
	
	if(isset($_POST['nom_contact']))      $nom_contact=$_POST['nom_contact'];
	else      $nom_contact="";
	
	if(isset($_POST['tel_contact']))      $tel_contact=$_POST['tel_contact'];
	else      $tel_contact="";
	
	if(isset($_POST['siret']))      $siret=$_POST['siret'];
	else      $siret="";
	
	if(isset($_POST['TVA_intra']))      $TVA_intra=$_POST['TVA_intra'];
	else      $TVA_intra="";
	
	if(isset($_POST['prod_prop']))      $prod_prop=$_POST['prod_prop'];
	else      $prod_prop="";
	
	if(isset($_POST['memo']))      $memo=$_POST['memo'];
	else      $memo="";
	
	// Connection et ouverture de la base	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	
	
	// Sécurisation des champs
	
	$nom = mysqli_real_escape_string($db, $nom);
	$adresse = mysqli_real_escape_string($db, $adresse);
	$ville = mysqli_real_escape_string($db, $ville);
	$code_postal = mysqli_real_escape_string($db, $code_postal);
	$telephone = mysqli_real_escape_string($db, $telephone);
	$fax = mysqli_real_escape_string($db, $fax);
	$email = mysqli_real_escape_string($db, $email);
	$pays = mysqli_real_escape_string($db, $pays);
	$site_web = mysqli_real_escape_string($db, $site_web);
	$nom_contact = mysqli_real_escape_string($db, $nom_contact);
	$tel_contact = mysqli_real_escape_string($db, $tel_contact);
	$siret = mysqli_real_escape_string($db, $siret);
	$TVA_intra = mysqli_real_escape_string($db, $TVA_intra);
	$prod_prop = mysqli_real_escape_string($db, $prod_prop);
	$memo = mysqli_real_escape_string($db, $memo);
	
	// Exécution de la requête
	mysqli_query($db, "INSERT into $db_fournisseurs (nom, adresse, ville, code_postal, telephone, fax, email, pays, site_web, nom_contact, tel_contact, siret, TVA_intra, prod_prop, memo) VALUES ('$nom','$adresse','$ville','$code_postal','$telephone','$fax','$email','$pays','$site_web','$nom_contact','$tel_contact','$siret','$TVA_intra','$prod_prop','$memo')")
	or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
	
	$nom = stripslashes($nom);
	$adresse = stripslashes($adresse);
	$ville = stripslashes($ville);
	$code_postal = stripslashes($code_postal);
	$telephone = stripslashes($telephone);
	$fax = stripslashes($fax);
	$email = stripslashes($email);
	$pays = stripslashes($pays);
	$site_web = stripslashes($site_web);
	$nom_contact = stripslashes($nom_contact);
	$tel_contact = stripslashes($tel_contact);
	$siret = stripslashes($siret);
	$TVA_intra = stripslashes($TVA_intra);
	$prod_prop = stripslashes($prod_prop);
	$memo = stripslashes($memo);
	
?>

	<p class="cen"><span class="validation">Informations enregistr&eacute;es</span></p>
	
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif fournisseurs : </legend>
				
				<p>Nom : <strong><?php echo $nom;?></strong></p>
				
				<p>Adresse : <strong><?php echo $adresse;?></strong></p>
				
				<p>Ville : <strong><?php echo $ville;?></strong></p>
				
				<p>Code postal : <strong><?php echo $code_postal;?></strong></p>
				
				<p>Telephone : <strong><?php echo $telephone;?></strong></p>
				
				<p>Fax : <strong><?php echo $fax;?></strong></p>
				
				<p>E-mail : <strong><?php echo $email;?></strong></p>
				
				<p>Pays : <strong><?php echo $pays;?></strong></p>
				
				<p>Site web : <strong><?php echo $site_web;?></strong></p>
				
				<p>Nom contact : <strong><?php echo $nom_contact;?></strong></p>
				
				<p>Telephone : <strong><?php echo $tel_contact;?></strong></p>
				
				<p>Siret : <strong><?php echo $siret;?></strong></p>
				
				<p>TVA intra-communautaire : <strong><?php echo $TVA_intra;?></strong></p>
				
				<p>Produits propos&eacute;s : <strong><?php echo $prod_prop;?></strong></p>
				
				<p>Memo : <strong><?php echo $memo;?></strong></p>
		
			</fieldset>
	
		<p class="cen"><a href="crea_four.php">Revenir au formulaire d'ajout</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
	
		</div>
	
	</div>
		
<?php
		}
require_once 'Main_ft.php'; 
?>