<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>
	
	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Suppression fournisseurs</h2>
			
			</div>

<?php
		
//Récupération des variables
	
	if(isset($_POST['ref_fournisseur']))      $ref_fournisseur=$_POST['ref_fournisseur'];
	else      $ref_fournisseur="";
		
	// Connection et ouverture de la base	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
				
	mysqli_query($db, "DELETE FROM $db_fournisseurs WHERE ref_fournisseur='$ref_fournisseur'")
	or die('<span class="err_bdd">Erreur lors de la suppression</span>');
	
	echo '<p class="cen"><span class="validation">Fournisseur supprim&eacute;</span></p>
	<p class="cen"><a href="liste_supp_four.php">Revenir &agrave; la liste de suppression des fournisseurs</a></p>';
		
?>	

		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
	
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>