<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
	
	$date_numfact=date('m-Y');

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	

	if(isset($_POST['numfact']))      $numfact=$_POST['numfact'];
	else      $numfact="";
	
	if(isset($_POST['date_fact']))      $date_fact=$_POST['date_fact'];
	else      $date_fact="";
	
	if(isset($_POST['nom_param']))      $nom_param=$_POST['nom_param'];
	else      $nom_param="";
	
	if(isset($_POST['nom_client']))      $nom_client=$_POST['nom_client'];
	else      $nom_client="";

	if(isset($_POST['avoir']))      $avoir=$_POST['avoir'];
	else      $avoir="0";
	
	if(isset($_POST['remise_tot']))      $remise_tot=$_POST['remise_tot'];
	else      $remise_tot="0";
	
	if (isset($_POST['type'])) $type=$_POST['type'];
	else $type="";
	
	if(isset($_POST['date_valid_deb']))      $date_valid_deb=$_POST['date_valid_deb'];
	else      $date_valid_deb="";	
	
	if(isset($_POST['date_valid_fin']))      $date_valid_fin=$_POST['date_valid_fin'];
	else      $date_valid_fin="";
	
	if (isset($_POST['type_paiement'])) $type_paiement=$_POST['type_paiement'];
	else $type_paiement="";

	if ($date_valid_deb!="" || $date_valid_deb!="")
		{
		if ($date_valid_deb[4]=="-" || $date_valid_fin[4]=="-")
			{
			$date_valid_deb = explode("-", $date_valid_deb);
			$date_valid_deb = $date_valid_deb[2].'/'.$date_valid_deb[1].'/'.$date_valid_deb[0];
			$date_valid_fin = explode("-", $date_valid_fin);
			$date_valid_fin = $date_valid_fin[2].'/'.$date_valid_fin[1].'/'.$date_valid_fin[0];
			}
		else
			{
			$date_valid_deb=$date_valid_deb;	
			$date_valid_fin=$date_valid_fin;
			}
		}
	else
		{
		$date_valid_deb=$date_valid_deb;	
		$date_valid_fin=$date_valid_fin;	
		}
	
	$Tot_produits="";
	$Tot_prestas="";
	$Tot_divers="";
	$Tot_divers_1="";
	$Tot_divers_2="";
	$Tot_HT="";
	$Tot_TVA_1="";
	$Tot_TVA_2="";
	$Tot_TVA_3="";
	$Tot_TVA_4="";
	$Tot_TTC="";
	$Net_prod="";
	$Net_prest="";
	$Net_prod_tot="";
	$Net_prest_tot="";
	$Net="";
	$info_devis="";
	$info_commande="";
	$info_facture="";
	$etat="en cours";

	$numfact = mysqli_real_escape_string($db, $numfact);
	$nom_client = mysqli_real_escape_string($db, $nom_client);
	$nom_param = mysqli_real_escape_string($db, $nom_param);
	$type_paiement = mysqli_real_escape_string($db, $type_paiement);
	
	$Requetec = "SELECT ref_clients, nb_facture FROM $db_compte_client WHERE nom ='$nom_client'";
	$ResReqc = mysqli_query($db, $Requetec) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
		
	$Donneesc = mysqli_fetch_array($ResReqc);
		
	$ref_clients=$Donneesc["ref_clients"];
	$nb_facture=$Donneesc["nb_facture"];
	
	$Requetep = "SELECT ref FROM $db_parametres WHERE nom_param ='$nom_param'";
	$ResReqp = mysqli_query($db, $Requetep) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>'); 
		
	$Donneesp = mysqli_fetch_array($ResReqp);
		
	$ref_param=$Donneesp["ref"];
	
	// if ($type=="devis")
	// 	{
	// 	$numfact="D-".$numfact."-".$date_numfact;
	// 	$type_paiement="";
	// 	}
	// else if ($type=="facture")
	// 	{
	// 	$numfact="F-".$numfact."-".$date_numfact;
	// 	}
	// else if ($type=="fact_av")
	// 	{
	// 	$numfact="FA-".$numfact."-".$date_numfact;
	// 	$type="facture";
	// 	}
	// else if ($type=="commande")
	// 	{
	// 	$numfact="C-".$numfact."-".$date_numfact;
	// 	$type_paiement="";
	// 	}

?>
	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Cr&eacute;ation <?php echo $type; ?>, pi&egrave;ce(s) et prestation(s)</h2>
			
				<p>
				R&eacute;capitulatif <?php echo $type; ?> et enregistrement dans la base de donn&eacute;es</strong>.
				</p>
				
			</div>
			
		<form action="imprime_fact.php" target="_blank" method="post">
		
		<input type="hidden" name="numfact" value="<?php echo $numfact; ?>" />
			
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif ent&ecirc;te : </legend>
				
				<p class="cen">Nom du client : <strong><?php echo stripslashes($nom_client);?></strong></p>
				
				<p class="cen">Param&egrave;tres : <strong><?php echo $nom_param;?></strong></p>
				
				<p class="cen">Num&eacute;ro de <?php echo $type; ?> : <strong><?php echo $numfact;?></strong></p>
				
				<p class="cen">Date de <?php echo $type; ?> : <strong><?php echo $date_fact;?></strong></p>
		
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Quantit&eacute;s des produits : </legend>
				
				<table>
				
					<tr>
					
						<th>Prod.</th>
						<th>Prix unit. HT</th>
						<th>Quant.</th>
						<th>Tot HT.</th>
						<th>Taux TVA.</th>
						<th>Tot TTC.</th>
						<th>Remise</th>
						<th>Net</th>
						
						
					</tr>
			
				<?php
				
				if(count(@$_POST['produit']) || count(@$_POST['quant']) || count(@$_POST['info_prod'])) 
					{
					$produit=$_POST['produit'];
					$info_prod=$_POST['info_prod'];
					function concatprod($produit, $info_prod) 
						{
						return $produit.'|'.$info_prod;
						}
					$result_prod = array_map ('concatprod', $produit, $info_prod);
					foreach (array_combine($result_prod, @$_POST['quant']) as $val => $qut)
						{
						if ($val!="" && $qut!="")
							{
							$val_prod = explode("|", $val);
							$des_prod = $val_prod[0];
							$ref_prod = $val_prod[1];
							$info_comp_prod = @$val_prod[2];
							$des_prod = mysqli_real_escape_string($db, $des_prod);
							$ref_prod = mysqli_real_escape_string($db, $ref_prod);
							$info_comp_prod = mysqli_real_escape_string($db, $info_comp_prod);
							$Requete = "SELECT * FROM $db_prod_prest WHERE designation ='$des_prod' AND reference ='$ref_prod'";

							$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 

							$Donnees = mysqli_fetch_array($ResReq);
					
							$designation=$Donnees["designation"];
							$reference=$Donnees["reference"];
							$designation = mysqli_real_escape_string($db, $designation);
							$reference = mysqli_real_escape_string($db, $reference);
							$taux_TVA=$Donnees["taux_TVA"];
							$quantite=$Donnees["quantite"];
							$prix_vente=$Donnees["prix_vente"];
							$prix_TTC=$Donnees["prix_TTC"];
							$remise=$Donnees["remise"];
							
							$totHT=$prix_vente*$qut;
							$totTTC=$prix_TTC*$qut;
							$TVA=$totTTC-$totHT;
							
							$type_fd="produit";
							
							$prix_vente=round($prix_vente, 2);
							$totHT=round($totHT, 2);
							$totTTC=round($totTTC, 2);
							$TVA=round($TVA, 2);
							
							if ($taux_TVA=="20")
								{
								$Tot_TVA_1=$Tot_TVA_1+$TVA;
								}
							if ($taux_TVA=="10")
								{
								$Tot_TVA_2=$Tot_TVA_2+$TVA;
								}
							if ($taux_TVA=="5.5")
								{
								$Tot_TVA_3=$Tot_TVA_3+$TVA;
								}
							if ($taux_TVA=="2.1")
								{
								$Tot_TVA_4=$Tot_TVA_4+$TVA;
								}

							mysqli_query($db, "INSERT into $db_facture_detail (num_fact, date_fact, designation, reference, info_comp, type, quantite, prix, taux_TVA, remise, totHT, TVA, totTTC) VALUES ('$numfact','$date_fact','$designation','$reference','$info_comp_prod','$type_fd','$qut','$prix_vente','$taux_TVA','$remise','$totHT','$TVA','$totTTC')")
							or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
							
							if ($type=="facture" || $type=="commande")
								{
								$quantite=$quantite-$qut;

								mysqli_query($db, "UPDATE $db_prod_prest SET quantite='$quantite' WHERE designation = '$des_prod'")
								or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
								}
							
							$Tot_produits=$Tot_produits+$totHT;
							$Tot_HT=$Tot_HT+$totHT;
							$Tot_TTC=$Tot_TTC+$totTTC;
							
							if (substr($remise, -1)=="%")
								{
								$Net_prod=$totTTC-($totTTC*(substr($remise, 0, -1)/100));
								}
							else
								{
								$Net_prod=$totTTC-$remise;
								}
								
							$Net_prod_tot=$Net_prod_tot+$Net_prod;
							$Net=$Net+$Net_prod;
							
							echo '<tr>
								
								<td><strong>'.stripslashes($designation).' '.stripslashes($reference).'</strong> '.stripslashes($info_comp_prod).'</td>
								<td><strong>'.$prix_vente.' &euro;</strong></td>
								<td><strong>'.$qut.'</strong></td>
								<td><strong>'.$totHT.' &euro;</strong></td>
								<td><strong>'.$taux_TVA.'%</strong></td>
								<td><strong>'.$totTTC.' &euro;</strong></td>';
								if (substr($remise, -1)!="%")
									{
									if ($remise=="")
										{
										$aff_remise=$remise;
										}
									else
										{
										$aff_remise=$remise.' &euro;';
										}
									}
								else
									{
									$aff_remise=$remise;
									}
								echo '<td><strong>'.$aff_remise.'</strong></td>
								<td><strong>'.$Net_prod.' &euro;</strong></td>
								
							</tr>';
							}
						}
					}
				else
					{
					echo '<tr>
							<td colspan="8">Pas de produits s&eacute;lectionn&eacute;s pour ce document</td>
						</tr>';
					}
					
				?>
				
				</table>
			
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Attribution des quantit&eacute;s des prestations : </legend>
				
				<table>
				
					<tr>
					
						<th>Prestation</th>
						<th>Prix unit. HT</th>
						<th>Quant.</th>
						<th>Tot HT.</th>
						<th>Taux TVA.</th>
						<th>Tot TTC.</th>
						<th>Remise</th>
						<th>Net</th>
						
					</tr>
			
				<?php
				
				if(count(@$_POST['prestation']) || count(@$_POST['quant2']) || count(@$_POST['info_prest'])) 
					{
					$prestation=$_POST['prestation'];
					$info_prest=$_POST['info_prest'];
					function concatprest($prestation, $info_prest) 
						{
						return $prestation.'|'.$info_prest;
						}
					$result_prest = array_map ('concatprest', $prestation, $info_prest);
					foreach (array_combine($result_prest, @$_POST['quant2']) as $val2 => $qut2)
						{
						if ($val2!="" && $qut2!="")
							{
							$val_prest = explode("|", $val2);
							$des_prest = $val_prest[0];
							$ref_prest = $val_prest[1];
							$info_comp_prest = @$val_prest[2];
							$des_prest = mysqli_real_escape_string($db, $des_prest);
							$ref_prest = mysqli_real_escape_string($db, $ref_prest);
							$info_comp_prest = mysqli_real_escape_string($db, $info_comp_prest);
							$Requete2 = "SELECT * FROM $db_prod_prest WHERE designation ='$des_prest' AND reference ='$ref_prest'";

							$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 

							$Donnees2 = mysqli_fetch_array($ResReq2);
					
							$designation2=$Donnees2["designation"];
							$reference2=$Donnees2["reference"];
							$designation2 = mysqli_real_escape_string($db, $designation2);
							$reference2 = mysqli_real_escape_string($db, $reference2);
							$taux_TVA2=$Donnees2["taux_TVA"];
							$quantite2=$Donnees2["quantite"];
							$prix_vente2=$Donnees2["prix_vente"];
							$prix_TTC2=$Donnees2["prix_TTC"];
							$remise2=$Donnees2["remise"];
							
							$totHT2=$prix_vente2*$qut2;
							$totTTC2=$prix_TTC2*$qut2;
							$TVA2=$totTTC2-$totHT2;
							
							$type_fd="prestation";
							
							$prix_vente2=round($prix_vente2, 2);
							$totHT2=round($totHT2, 2);
							$totTTC2=round($totTTC2, 2);
							$TVA2=round($TVA2, 2);
							
							if ($taux_TVA2=="20")
								{
								$Tot_TVA_1=$Tot_TVA_1+$TVA2;
								}
							if ($taux_TVA2=="10")
								{
								$Tot_TVA_2=$Tot_TVA_2+$TVA2;
								}
							if ($taux_TVA2=="5.5")
								{
								$Tot_TVA_3=$Tot_TVA_3+$TVA2;
								}
							if ($taux_TVA2=="2.1")
								{
								$Tot_TVA_4=$Tot_TVA_4+$TVA2;
								}

							mysqli_query($db, "INSERT into $db_facture_detail (num_fact, date_fact, designation, reference, info_comp, type, quantite, prix, taux_TVA, remise, totHT, TVA, totTTC) VALUES ('$numfact','$date_fact','$designation2','$reference2','$info_comp_prest','$type_fd','$qut2','$prix_vente2','$taux_TVA2','$remise2','$totHT2','$TVA2','$totTTC2')")
							or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
							
							$Tot_prestas=$Tot_prestas+$totHT2;
							$Tot_HT=$Tot_HT+$totHT2;
							$Tot_TTC=$Tot_TTC+$totTTC2;
							
							if (substr($remise2, -1)=="%")
								{
								$Net_prest=$totTTC2-($totTTC2*(substr($remise2, 0, -1)/100));
								}
							else
								{
								$Net_prest=$totTTC2-$remise2;
								}
							
							$Net_prest_tot=$Net_prest_tot+$Net_prest;
							$Net=$Net+$Net_prest;
							
							echo '<tr>
								
								<td><strong>'.stripslashes($designation2).' '.stripslashes($reference2).'</strong> '.stripslashes($info_comp_prest).'</td>
								<td><strong>'.$prix_vente2.' &euro;</strong></td>
								<td><strong>'.$qut2.'</strong></td>
								<td><strong>'.$totHT2.' &euro;</strong></td>
								<td><strong>'.$taux_TVA2.'%</strong></td>
								<td><strong>'.$totTTC2.' &euro;</strong></td>';
								if (substr($remise2, -1)!="%")
									{
									if ($remise2=="")
										{
										$aff_remise2=$remise2;
										}
									else
										{
										$aff_remise2=$remise2.' &euro;';
										}
									}
								else
									{
									$aff_remise2=$remise2;
									}
								echo '<td><strong>'.$aff_remise2.'</strong></td>
								<td><strong>'.$Net_prest.' &euro;</strong></td>
								
							</tr>';
							}
						}
					}
				else
					{
					echo '<tr>
							<td colspan="8">Pas de prestations s&eacute;lectionn&eacute;es pour ce document</td>
						</tr>';
					}
					
				?>
				
				</table>
			
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Date de validit&eacute; : </legend>
				
				<p class="cen">Du : <strong><?php echo $date_valid_deb;?></strong></p>
				
				<p class="cen">Au : <strong><?php echo $date_valid_fin;?></strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend class="lg"> Totaux : </legend>
			
			<?php
			
			if (substr($remise_tot, -1)=="%")
				{
				$Net=$Net-($Net*(substr($remise_tot, 0, -1)/100));
				}
			else
				{
				$Net=$Net-$remise_tot;
				}
			$Net=$Net-$avoir;
			
			$Tot_HT=round($Tot_HT, 2);
			$Tot_TVA_1=round($Tot_TVA_1, 2);
			$Tot_TVA_2=round($Tot_TVA_2, 2);
			$Tot_TVA_3=round($Tot_TVA_3, 2);
			$Tot_TVA_4=round($Tot_TVA_4, 2);
			$Tot_TTC=round($Tot_TTC, 2);
			$Net=round($Net, 2);
	
			$date_valid_deb = mysqli_real_escape_string($db, $date_valid_deb);
			$date_valid_fin = mysqli_real_escape_string($db, $date_valid_fin);
			$Tot_produits = mysqli_real_escape_string($db, $Tot_produits);
			$Tot_prestas = mysqli_real_escape_string($db, $Tot_prestas);

			mysqli_query($db, "INSERT into $db_facture_entete (num_fact, nom_param, date_fact, date_valid_deb, date_valid_fin, total_HT, total_TVA1, total_TVA2, total_TVA3, total_TVA4, tot_produits, tot_prestas, remise, total_TTC, avoir, type_paiement, nom_membre, nom_client, type, etat) VALUES ('$numfact','$ref_param','$date_fact','$date_valid_deb','$date_valid_fin','$Tot_HT','$Tot_TVA_1','$Tot_TVA_2','$Tot_TVA_3','$Tot_TVA_4','$Tot_produits','$Tot_prestas','$remise_tot','$Tot_TTC','$avoir','$type_paiement','$nom_membre','$ref_clients','$type','$etat')")
			or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');

			$Requete3 = "SELECT * FROM $db_membres WHERE nom='$nom_membre'";
			$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Erreur de s&eacute;lection</span>'); 
			$Donnees3 = mysqli_fetch_array($ResReq3);
			$info_facture = $Donnees3["info_facture"];
			$info_devis = $Donnees3["info_devis"];
			$info_commande = $Donnees3["info_commande"];
			
			if ($type=="devis")
				{
				$info_devis=$info_devis+1;
				mysqli_query($db, "UPDATE $db_membres SET info_devis='$info_devis' WHERE nom = '$nom_membre'")
				or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
				}
				
			if ($type=="commande")
				{
				$info_commande=$info_commande+1;
				mysqli_query($db, "UPDATE $db_membres SET info_commande='$info_commande' WHERE nom = '$nom_membre'")
				or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
				}
			
			if ($type=="facture")
				{
				$nb_facture=$nb_facture+1;
			
				mysqli_query($db, "UPDATE $db_compte_client SET nb_facture='$nb_facture' WHERE ref_clients = '$ref_clients'")
				or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
				
				$info_facture=$info_facture+1;
				mysqli_query($db, "UPDATE $db_membres SET info_facture='$info_facture' WHERE nom = '$nom_membre'")
				or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
				}
	
			?>
			
				<p class="cen">Total HT : <strong><?php echo $Tot_HT;?> &euro;</strong></p>
				
				<p class="cen">Total produits : <strong><?php echo $Tot_produits;?> &euro;</strong></p>
				
				<p class="cen">Net produits : <strong><?php echo $Net_prod_tot;?> &euro;</strong></p>
				
				<p class="cen">Total prestations : <strong><?php echo $Tot_prestas;?> &euro;</strong></p>
				
				<p class="cen">Net prestations : <strong><?php echo $Net_prest_tot;?> &euro;</strong></p>
				
				<p class="cen">Total TVA 20% : <strong><?php echo $Tot_TVA_1;?> &euro;</strong></p>
				
				<p class="cen">Total TVA 10% : <strong><?php echo $Tot_TVA_2;?> &euro;</strong></p>
				
				<p class="cen">Total TVA 5.5% : <strong><?php echo $Tot_TVA_3;?> &euro;</strong></p>
				
				<p class="cen">Total TVA 2.1% : <strong><?php echo $Tot_TVA_4;?> &euro;</strong></p>
				
				<p class="cen">Total TTC : <strong><?php echo $Tot_TTC;?> &euro;</strong></p>
				
				<p class="cen">Remise : <strong><?php echo $remise_tot;?> &euro;</strong></p>
				
				<p class="cen">Avoirs : <strong><?php echo $avoir;?> &euro;</strong></p>
				
<?php
				if ($type=="facture")
					{
					echo '<p class="cen">Type de paiement : <strong>'.stripslashes($type_paiement).'</strong></p>';
					}
?>
				
				<p class="cen">Total Net : <strong><?php echo $Net;?> &euro;</strong></p>
				
			</fieldset>
			
			<fieldset>
			
				<legend> Impression : </legend>
				
				<p style="text-align:center;">
					<input type="submit" value="Imprimer"/>
				</p>
				
			</fieldset>
			
		</form>
		<form action="/pdf_export/render3" method="post" target="_blank" id="reg-pdf">
			<input type="hidden" name="numfact" value="<?php echo $numfact; ?>" />
			<input type="submit" value="Imprimer PDF" name="PDF"/>
		</form>
		<style>
			#reg-pdf{
				width: 100%;
				text-align: center;
			}
			#reg-pdf input{
				margin: auto;
				display: block;
				margin-top: 10px;
			}
		</style>
		
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>