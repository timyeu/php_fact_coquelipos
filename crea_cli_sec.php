<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';

	if(isset($_POST['nom']))      $nom=$_POST['nom'];
	else      $nom="";
	
	if(isset($_POST['nature']))      $nature=$_POST['nature'];
	else      $nature="";

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	

	$Requete = "SELECT * FROM $db_compte_client";

	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, facture incorrecte ou inexistante</span>'); 
						
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout client</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter un nouveau client</strong>.
				</p>
				
			</div>
			
		<form action="req_crea_cli.php" method="post" class="crea_cli_sec">
		
<?php

	echo '<input type="hidden" name="nature" value="'.$nature.'"/>';

	if ($nature=="particulier")
		{
			echo '<fieldset>
				
				<legend class="lg"> Detail client : </legend>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom" value="'.$nom.'"/></p>
				
				<p><label class="gauche" for="adresse">Adresse :</label>
				<input class="droit" id="adresse" type="text" name="adresse"/></p>
				
				<p><label class="gauche" for="ville">Ville :</label>
				<input class="droit" id="ville" type="text" name="ville"/></p>
				
				<p><label class="gauche" for="code_postal">Code postal :</label>
				<input class="droit" id="code_postal" type="text" name="code_postal"/></p>
				
				<p><label class="gauche" for="tel">Telephone :</label>
				<input class="droit" id="tel" type="text" name="tel"/></p>
				
				<p><label class="gauche" for="tel_portable">Telephone portable :</label>
				<input class="droit" id="tel_portable" type="text" name="tel_portable"/></p>
				
				<p><label class="gauche" for="mail">E-mail :</label>
				<input class="droit" id="mail" type="text" name="mail"/></p>
				
				<p><label class="gauche" for="encours">Encours :</label>
				<input class="droit" id="encours" type="text" name="encours"/></p>
				
				<p><label class="gauche" for="liste_commandes">Liste commandes :</label>
				<textarea class="droit" id="liste_commandes" name="liste_commandes"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="date_anniv">Date anniversaire :</label>
				<input class="droit" id="date_anniv" type="text" name="date_anniv"/></p>
				
				<p><label class="gauche" for="date_autre">Date autre :</label>
				<input class="droit" id="date_autre" type="text" name="date_autre"/></p>
				
				<p><label class="gauche" for="notes">Notes :</label>
				<textarea class="droit" id="notes" name="notes"></textarea></p>
		
			</fieldset>';
		}
	else
		{
			echo '<fieldset>
				
				<legend class="lg"> Detail client : </legend>
				
				<p><label class="gauche" for="nom">Nom et raison sociale :</label>
				<input class="droit" id="nom" type="text" name="nom" value="'.$nom.'"/></p>
				
				<p><label class="gauche" for="contact">Nom contact principal (pr&eacute;ciser civilit&eacute;) :</label>
				<input class="droit" id="contact" type="text" name="contact"/></p>
				
				<p><label class="gauche" for="fonc_contact">Fonction contact principal :</label>
				<input class="droit" id="fonc_contact" type="text" name="fonc_contact"/></p>
				
				<p class="cen"><strong>Le contact principal appara&icirc;tra sur les documents si pr&eacute;cis&eacute;</strong></p>
				
				<p><label class="gauche" for="contact_autre">Nom contact autre (pr&eacute;ciser civilit&eacute;) :</label>
				<input class="droit" id="contact_autre" type="text" name="contact_autre"/></p>
				
				<p><label class="gauche" for="fonc_contact_autre">Fonction contact autre :</label>
				<input class="droit" id="fonc_contact_autre" type="text" name="fonc_contact_autre"/></p>
				
				<p><label class="gauche" for="adresse">Adresse :</label>
				<input class="droit" id="adresse" type="text" name="adresse"/></p>
				
				<p><label class="gauche" for="ville">Ville :</label>
				<input class="droit" id="ville" type="text" name="ville"/></p>
				
				<p><label class="gauche" for="code_postal">Code postal :</label>
				<input class="droit" id="code_postal" type="text" name="code_postal"/></p>
				
				<p><label class="gauche" for="tel">Telephone :</label>
				<input class="droit" id="tel" type="text" name="tel"/></p>
				
				<p><label class="gauche" for="tel_portable">Telephone portable :</label>
				<input class="droit" id="tel_portable" type="text" name="tel_portable"/></p>

				<p><label class="gauche" for="fax">Fax :</label>
				<input class="droit" id="fax" type="text" name="fax"/></p>
				
				<p><label class="gauche" for="mail">E-mail :</label>
				<input class="droit" id="mail" type="text" name="mail"/></p>
				
				<p><label class="gauche" for="activite">Activite :</label>
				<input class="droit" id="activite" type="text" name="activite"/></p>
				
				<p><label class="gauche" for="site_web">Site web :</label>
				<input class="droit" id="site_web" type="text" name="site_web"/></p>
				
				<p><label class="gauche" for="siret">Siret :</label>
				<input class="droit" id="siret" type="text" name="siret"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA intra-communautaire :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra"/></p>
				
				<p><label class="gauche" for="encours">Encours :</label>
				<input class="droit" id="encours" type="text" name="encours"/></p>
				
				<p><label class="gauche" for="liste_commandes">Liste commandes :</label>
				<textarea class="droit" id="liste_commandes" name="liste_commandes"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="date_anniv">Date anniversaire :</label>
				<input class="droit" id="date_anniv" type="text" name="date_anniv"/></p>
				
				<p><label class="gauche" for="date_autre">Date autre :</label>
				<input class="droit" id="date_autre" type="text" name="date_autre"/></p>
				
				<p><label class="gauche" for="notes">Notes :</label>
				<textarea class="droit" id="notes" name="notes"></textarea></p>
		
			</fieldset>';
		}
		
?>		
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Effacer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>