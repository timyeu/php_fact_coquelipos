<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';

	if (isset($_GET['ok'])) $ok="Article modifi&eacute;";
		else $ok="";

?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation produits/prestations</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier des produits/prestations existant(e)s</strong>.
				</p>
				
			</div>
			
		<p class="cen"><span class="validation"><?php echo $ok;?></span></p>
			
			<fieldset style="width:95%; margin: 1.25%;" >
				
				<legend class="lg"> Produit/Prestation : </legend>

<?php

	if (isset($_POST['prod'])) $prod=$_POST['prod'];
		else $prod="";
		
	if ($prod=="")
		{
		if (isset($_POST['ListProd'])) $prod=$_POST['ListProd'];
			else $prod="";
		}
		
	if (isset($_GET['prod'])) $prod=$_GET['prod'];
		else $prod=$prod;
	
	if (isset($_POST['prest'])) $prest=$_POST['prest'];
		else $prest="";
		
	if ($prest=="")
		{
		if (isset($_POST['ListPrest'])) $prest=$_POST['ListPrest'];
			else $prest="";
		}
		
	if (isset($_GET['prest'])) $prest=$_GET['prest'];
		else $prest=$prest;

		if ($prod=="" && $prest=="")
			{
				echo "Aucune saisie pour faire la liste";
			}
			
			else
			{	
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
					
				$prod = mysqli_real_escape_string($db, $prod);
				$prest = mysqli_real_escape_string($db, $prest);
		
				if ($prod!="")
					{
					$Requete = "SELECT * FROM $db_prod_prest WHERE designation LIKE '".$prod."%' AND nature = 'produit' ORDER BY designation";
					}
				else
					{
					$Requete = "SELECT * FROM $db_prod_prest WHERE designation LIKE '".$prest."%' AND nature = 'prestation' ORDER BY designation";
					}
					
				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, pi&egrave;ce incorrecte ou inexistante</span>'); 
				$nbenreg = mysqli_num_rows($ResReq);
				$nbchamps = mysqli_num_fields($ResReq);
				$Tabdo[$nbenreg][$nbchamps]="";
				$Tabchamps[$nbchamps]="";
		
							for($I=0; $I < $nbchamps; $I++) 
								{
								$tabchamps[$I] = mysqli_fetch_field_direct($ResReq,$I);
								}
  		
							$I=0;
							while ($donnees = mysqli_fetch_array($ResReq))
								{
									$Tabdo[$I][1]=$donnees["ref_produits"];
									$Tabdo[$I][2]=$donnees["designation"];
									$Tabdo[$I][3]=$donnees["nature"];
									$Tabdo[$I][4]=$donnees["reference"];
									$Tabdo[$I][5]=$donnees["quantite"];
									$Tabdo[$I][6]=$donnees["qte_limite"];
									$Tabdo[$I][7]=$donnees["prix_achat"];
									$Tabdo[$I][8]=$donnees["coef"];
									$Tabdo[$I][9]=$donnees["prix_vente"];
									$Tabdo[$I][10]=$donnees["taux_TVA"];
									$Tabdo[$I][11]=$donnees["remise"];
									$Tabdo[$I][12]=$donnees["prix_TTC"];
									$Tabdo[$I][13]=$donnees["ref_fournisseur"];
									$Tabdo[$I][14]=$donnees["ref_prod_fournisseur"];
									$Tabdo[$I][15]=$donnees["informations"];
									$ref_produits=$Tabdo[$I][1];
									$designation=$Tabdo[$I][2];
									$nature=$Tabdo[$I][3];
									$reference=$Tabdo[$I][4];
									$quantite=$Tabdo[$I][5];
									$qte_limite=$Tabdo[$I][6];
									$prix_achat=$Tabdo[$I][7];
									$coef=$Tabdo[$I][8];
									$prix_vente=$Tabdo[$I][9];
									$taux_TVA=$Tabdo[$I][10];
									$remise=$Tabdo[$I][11];
									$prix_TTC=$Tabdo[$I][12];
									$ref_fournisseur=$Tabdo[$I][13];
									$ref_prod_fournisseur=$Tabdo[$I][14];
									$informations=$Tabdo[$I][15];
									$designation_anc=$designation;
									$reference_anc=$reference;
									
			$Requete2 = "SELECT ref_fournisseur, nom FROM $db_fournisseurs";
			
			$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 

			$Requete3 = "SELECT ref_fournisseur, nom FROM $db_fournisseurs WHERE ref_fournisseur='$ref_fournisseur'";

			$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
			$Donnees3 = mysqli_fetch_array($ResReq3);
			
			$reffour=$Donnees3["ref_fournisseur"];
			$nomfour=$Donnees3["nom"];
			
	if (substr($remise, -1)=="%")
		{
		$prix_net=$prix_TTC-($prix_TTC*(substr($remise, 0, -1)/100));
		}
	else
		{
		$prix_net=$prix_TTC-$remise;
		}
?>
				<form action="req_cons_prod.php" method="post">
				
				<input type="hidden" name="ref_produits" value="<?php echo $ref_produits;?>" />
				<input type="hidden" name="prod" value="<?php echo $prod;?>" />
				<input type="hidden" name="prest" value="<?php echo $prest;?>" />
				<input type="hidden" name="quantite" value="<?php echo $quantite;?>" />
				<input type="hidden" name="designation_anc" value="<?php echo $designation_anc;?>" />
				<input type="hidden" name="reference_anc" value="<?php echo $reference_anc;?>" />
				<input type="hidden" name="prix_TTC" value="<?php echo $prix_TTC;?>" />
						
				<table style="width:100%;">
				
					<tr>
						<th colspan="3">D&eacute;signation</th>
						<th colspan="3">R&eacute;f&eacute;rence</th>
						<td rowspan="10"><input type="submit" value="Md."/></td>
					</tr>
					
					<tr>
						<td colspan="3"><input type="text" name="designation" value="<?php echo $designation;?>" /></td>
						<td colspan="3"><input type="text" name="reference" value="<?php echo $reference;?>" /></td>
					</tr>
					
					<tr>
						<th colspan="3">Nature</th>
						<th colspan="3">Informations</th>
					</tr>
					
					<tr>
						<td colspan="3">
						<select name="nature">
						<option value="<?php echo $nature;?>"><?php echo $nature;?></option>
						<option value="produit">Produit</option>
						<option value="prestation">Prestation</option>
						</select>
						</td>
						<td colspan="3"><textarea id="informations" name="informations"><?php echo $informations; ?></textarea></td>
					</tr>
					
					<tr>
						<th>Qte.</th>
						<th>Qte. limite</th>
						<th>Prix achat (&euro;)</th>
						<th>Coef.</th>
						<th>Taux TVA</th>
						<th>Prix HT (&euro;)</th>
					</tr>
					
					<tr>
						<td>
						<p><strong>Tot : <?php echo $quantite;?></strong></p>
						<p><input class="cons2" type="text" name="quant" /></p>
						</td>
						<td><input class="cons2" type="text" name="qte_limite" value="<?php echo $qte_limite;?>" /></td>
						<td><input class="cons2" type="text" name="prix_achat" value="<?php echo $prix_achat;?>" /></td>
						<td><input class="cons2" type="text" name="coef" value="<?php echo $coef;?>" /></td>
						<td>
						<select class="cons2" name="taux_TVA">
						<option value="<?php echo $taux_TVA;?>"><?php echo $taux_TVA;?></option>
						<option value="20">20</option>
						<option value="10">10</option>
						<option value="5.5">5.5</option>
						<option value="2.1">2.1</option>
						<option value="">Pas de TVA</option>
						</select>
						</td>
						<td><input class="cons2" type="text" name="prix_vente" value="<?php echo $prix_vente;?>" /></td>
					</tr>
					
					<tr>
						<th colspan="3">Fournisseur</th>
						<th colspan="3">Remise</th>
					</tr>
					
					<tr>
						<td colspan="3"><p><select name="ref_fournisseur">
						<option value="<?php echo $reffour;?>"><?php echo $nomfour;?></option>
						<?php while ($LigneDo2 = mysqli_fetch_array($ResReq2)) 
							{
							$Nref_four = $LigneDo2["ref_fournisseur"];
							$Nm_four = $LigneDo2["nom"];
							echo '<option value="'.$Nref_four.'">'.$Nm_four.'</option>';
							}
						echo '<option value="">Aucun</option>;';?>
						</select></p>
						<p><input type="text" name="ref_prod_fournisseur" value="<?php echo $ref_prod_fournisseur;?>" /></p></td>
						<td colspan="3"><input type="text" name="remise" value="<?php echo $remise;?>" /></td>
					</tr>
					
					<tr>
						<th colspan="3">Prix TTC (&euro;)</th>
						<th colspan="3">Net (&euro;)</th>
					</tr>
					
					<tr>
						<td colspan="3"><input class="cons2" type="text" name="prix_TTC_nouv" value="<?php echo $prix_TTC;?>" /></td>
						<td colspan="3"><strong><?php echo $prix_net;?> &euro;</strong></td>
					</tr>
					
				</table>
				
				</form>
				
				<p class="cen">--------------------------------------</p>
						
<?php
							$I++;
							}
						}
?>
				
			</fieldset>
		
		<p class="cen"><a href="liste_cons_prod.php">Revenir &agrave; la liste de consultation des produits/prestations</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>