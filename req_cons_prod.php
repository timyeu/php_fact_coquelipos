<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{

	$ok='1';

	if(isset($_POST['ref_produits']))      $ref_produits=$_POST['ref_produits'];
	else      $ref_produits="";
	
	if(isset($_POST['prod']))      $prod=$_POST['prod'];
	else      $prod="";
	
	if(isset($_POST['prest']))      $prest=$_POST['prest'];
	else      $prest="";
	
	if(isset($_POST['designation']))      $designation=$_POST['designation'];
	else      $designation="";
	
	if(isset($_POST['designation_anc']))      $designation_anc=$_POST['designation_anc'];
	else      $designation_anc="";
	
	if(isset($_POST['nature']))      $nature=$_POST['nature'];
	else      $nature="";
	
	if(isset($_POST['reference_anc']))      $reference_anc=$_POST['reference_anc'];
	else      $reference_anc="";
	
	if(isset($_POST['reference']))      $reference=$_POST['reference'];
	else      $reference="";
	
	if(isset($_POST['informations']))      $informations=$_POST['informations'];
	else      $informations="";

	if(isset($_POST['quant']))      $quant=$_POST['quant'];
	else      $quant="";
	
	if(isset($_POST['quantite']))      $quantite=$_POST['quantite'];
	else      $quantite="";
	
	if(isset($_POST['qte_limite']))      $qte_limite=$_POST['qte_limite'];
	else      $qte_limite="";
	
	if(isset($_POST['prix_achat']))      $prix_achat=$_POST['prix_achat'];
	else      $prix_achat="";
	
	if(isset($_POST['coef']))      $coef=$_POST['coef'];
	else      $coef="";
	
	if(isset($_POST['taux_TVA']))      $taux_TVA=$_POST['taux_TVA'];
	else      $taux_TVA="";
	
	if(isset($_POST['prix_vente']))      $prix_vente=$_POST['prix_vente'];
	else      $prix_vente="";
	
	if(isset($_POST['remise']))      $remise=$_POST['remise'];
	else      $remise="";
	
	if(isset($_POST['prix_TTC']))      $prix_TTC=$_POST['prix_TTC'];
	else      $prix_TTC="";
	
	if(isset($_POST['prix_TTC_nouv']))      $prix_TTC_nouv=$_POST['prix_TTC_nouv'];
	else      $prix_TTC_nouv="";
	
	if(isset($_POST['ref_fournisseur']))      $ref_fournisseur=$_POST['ref_fournisseur'];
	else      $ref_fournisseur="";
	
	if(isset($_POST['ref_prod_fournisseur']))      $ref_prod_fournisseur=$_POST['ref_prod_fournisseur'];
	else      $ref_prod_fournisseur="";

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	$designation = mysqli_real_escape_string($db, $designation);
	$nature = mysqli_real_escape_string($db, $nature);
	$reference = mysqli_real_escape_string($db, $reference);
	$informations = mysqli_real_escape_string($db, $informations);
	$quant = mysqli_real_escape_string($db, $quant);
	$quantite = mysqli_real_escape_string($db, $quantite);
	$qte_limite = mysqli_real_escape_string($db, $qte_limite);
	$prix_achat = mysqli_real_escape_string($db, $prix_achat);
	$coef = mysqli_real_escape_string($db, $coef);
	$taux_TVA = mysqli_real_escape_string($db, $taux_TVA);
	$prix_vente = mysqli_real_escape_string($db, $prix_vente);
	$remise = mysqli_real_escape_string($db, $remise);
	$prix_TTC = mysqli_real_escape_string($db, $prix_TTC);
	$prix_TTC_nouv = mysqli_real_escape_string($db, $prix_TTC_nouv);
	$ref_fournisseur = mysqli_real_escape_string($db, $ref_fournisseur);
	$ref_prod_fournisseur = mysqli_real_escape_string($db, $ref_prod_fournisseur);
	$designation_anc = mysqli_real_escape_string($db, $designation_anc);
	$reference_anc = mysqli_real_escape_string($db, $reference_anc);
	
	$quantite=$quantite+$quant;
	
	if ($prix_vente=="")
		{
		if ($nature=="produit")
			{
			$prix_vente=$prix_achat*$coef;
			}
		else
			{
			$prix_vente=$prix_vente;
			}
		}
	else
		{
		if ($prix_achat=="0" && $coef=="0")
			{
			$prix_vente=$prix_vente;
			}
		else
			{
			$prix_vente=$prix_achat*$coef;
			}
		}
		
	if ($prix_TTC_nouv==$prix_TTC)
		{
		$prix_TTC=$prix_vente+($prix_vente*($taux_TVA/100));
		}
	else
		{
		$prix_TTC=$prix_TTC_nouv;
		$prix_vente=$prix_TTC/(1+($taux_TVA/100));
		$prix_achat=$prix_vente/$coef;
		}

	$prix_vente=round($prix_vente, 2);
	$prix_TTC=round($prix_TTC, 2);
	
	if (substr($remise, -1)=="%")
		{
		$prix_net=$prix_TTC-($prix_TTC*(substr($remise, 0, -1)/100));
		}
	else
		{
		$prix_net=$prix_TTC-$remise;
		}
		
	mysqli_query($db, "UPDATE $db_prod_prest SET designation='$designation', nature='$nature', reference='$reference', informations='$informations', quantite='$quantite', qte_limite='$qte_limite', prix_achat='$prix_achat', coef='$coef', taux_TVA='$taux_TVA', prix_vente='$prix_vente', remise='$remise', prix_TTC='$prix_TTC', ref_fournisseur='$ref_fournisseur', ref_prod_fournisseur='$ref_prod_fournisseur' WHERE ref_produits = '$ref_produits'")
	or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
	mysqli_query($db, "UPDATE $db_facture_detail SET designation='$designation', reference='$reference' WHERE designation='$designation_anc' AND reference='$reference_anc'")
	or die('<span class="err_bdd">Erreur de modification, veuillez resaisir</span>');
	
	header('Location: http://'.$link_domain.'/cons_prod.php?prod='.$prod.'&prest='.$prest.'&ok='.$ok);
	exit;
		}
?>