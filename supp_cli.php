<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Suppression client</h2>
			
				<p>
				Ce formulaire vous permet de <strong>supprimer un client existant</strong>.
				</p>
				
			</div>

<?php

	if (isset($_POST['ListCli'])) $ListCli=$_POST['ListCli'];
		else $ListCli="";
		
	if (isset($_GET['ref'])) $ListCli=$_GET['ref'];
		else $ListCli=$ListCli;
		
	$Requete = "SELECT * FROM $db_compte_client WHERE ref_clients ='$ListCli'";

		if (empty($ListCli))
			{
				echo "Il faut selectionner un client dans la liste";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
				
				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref_clients=$Donnees["ref_clients"];
				$nom=$Donnees["nom"];
				$contact=$Donnees["contact"];
				$fonc_contact=$Donnees["fonc_contact"];
				$contact_autre=$Donnees["contact_autre"];
				$fonc_contact_autre=$Donnees["fonc_contact_autre"];
				$nature=$Donnees["nature"];
				$adresse=$Donnees["adresse"];
				$ville=$Donnees["ville"];
				$code_postal=$Donnees["code_postal"];
				$tel=$Donnees["tel"];
				$tel_portable=$Donnees["tel_portable"];
				$fax=$Donnees["fax"];
				$mail=$Donnees["mail"];
				$activite=$Donnees["activite"];
				$site_web=$Donnees["site_web"];
				$siret=$Donnees["siret"];
				$TVA_intra=$Donnees["TVA_intra"];
				$encours=$Donnees["encours"];
				$liste_commandes=$Donnees["liste_commandes"];
				$nb_facture=$Donnees["nb_facture"];
				$date_anniv=$Donnees["date_anniv"];
				$date_autre=$Donnees["date_autre"];
				$notes=$Donnees["notes"];
			}

?>
			
		<form action="req_supp_cli.php" method="post">
		<input type="hidden" name="ref_clients" id="ref_clients" value="<?php echo $ref_clients;?>" />
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif client : </legend>
				
				<p>Nom : <strong><?php echo $nom;?></strong></p>
				
				<p>Nom contact pricipal: <strong><?php echo $contact;?></strong></p>
				
				<p>Fonction contact pricipal: <strong><?php echo $fonc_contact;?></strong></p>
				
				<p>Nom contact autre: <strong><?php echo $contact_autre;?></strong></p>
				
				<p>Fonction contact autre: <strong><?php echo $fonc_contact_autre;?></strong></p>
				
				<p>Nature : <strong><?php echo $nature;?></strong></p>
				
				<p>Adresse : <strong><?php echo $adresse;?></strong></p>
				
				<p>Ville : <strong><?php echo $ville;?></strong></p>
				
				<p>Code postal : <strong><?php echo $code_postal;?></strong></p>
				
				<p>Telephone : <strong><?php echo $tel;?></strong></p>
				
				<p>Telephone portable : <strong><?php echo $tel_portable;?></strong></p>
				
				<p>Fax : <strong><?php echo $fax;?></strong></p>
				
				<p>E-mail : <strong><?php echo $mail;?></strong></p>
				
				<p>Activite : <strong><?php echo $activite;?></strong></p>
				
				<p>Site web : <strong><?php echo $site_web;?></strong></p>
				
				<p>Siret : <strong><?php echo $siret;?></strong></p>
				
				<p>TVA intra-communautaire : <strong><?php echo $TVA_intra;?></strong></p>
				
				<p>Encours : <strong><?php echo $encours;?></strong></p>
				
				<p>Liste commandes : <strong><?php echo $liste_commandes;?></strong></p>
				
				<p>Date anniversaire : <strong><?php echo $date_anniv;?></strong></p>
				
				<p>Date autre : <strong><?php echo $date_autre;?></strong></p>
				
				<p>Notes : <strong><?php echo $notes;?></strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Supprimer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_supp_cli.php">Revenir &agrave; la liste de suppression des clients</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>