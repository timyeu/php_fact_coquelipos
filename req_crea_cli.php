<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
	
	$date=date('d/m/Y');

?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout client</h2>
			
			</div>
			
<?php

	if(isset($_POST['nom']))      $nom=$_POST['nom'];
	else      $nom="";
	
	if(isset($_POST['contact']))      $contact=$_POST['contact'];
	else      $contact="";
	
	if(isset($_POST['fonc_contact']))      $fonc_contact=$_POST['fonc_contact'];
	else      $fonc_contact="";
	
	if(isset($_POST['contact_autre']))      $contact_autre=$_POST['contact_autre'];
	else      $contact_autre="";
	
	if(isset($_POST['fonc_contact_autre']))      $fonc_contact_autre=$_POST['fonc_contact_autre'];
	else      $fonc_contact_autre="";
	
	if(isset($_POST['nature']))      $nature=$_POST['nature'];
	else      $nature="";
	
	if(isset($_POST['adresse']))      $adresse=$_POST['adresse'];
	else      $adresse="";
	
	if(isset($_POST['ville']))      $ville=$_POST['ville'];
	else      $ville="";

	if(isset($_POST['code_postal']))      $code_postal=$_POST['code_postal'];
	else      $code_postal="";
	
	if(isset($_POST['tel']))      $tel=$_POST['tel'];
	else      $tel="";
	
	if(isset($_POST['tel_portable']))      $tel_portable=$_POST['tel_portable'];
	else      $tel_portable="";
	
	if(isset($_POST['fax']))      $fax=$_POST['fax'];
	else      $fax="";
	
	if(isset($_POST['mail']))      $mail=$_POST['mail'];
	else      $mail="";
	
	if(isset($_POST['activite']))      $activite=$_POST['activite'];
	else      $activite="";
	
	if(isset($_POST['site_web']))      $site_web=$_POST['site_web'];
	else      $site_web="";
	
	if(isset($_POST['siret']))      $siret=$_POST['siret'];
	else      $siret="";
	
	if(isset($_POST['TVA_intra']))      $TVA_intra=$_POST['TVA_intra'];
	else      $TVA_intra="";
	
	if(isset($_POST['encours']))      $encours=$_POST['encours'];
	else      $encours="";
	
	if(isset($_POST['liste_commandes']))      $liste_commandes=$_POST['liste_commandes'];
	else      $liste_commandes="";
	
	if(isset($_POST['nb_facture']))      $nb_facture=$_POST['nb_facture'];
	else      $nb_facture="";
	
	if(isset($_POST['date_anniv']))      $date_anniv=$_POST['date_anniv'];
	else      $date_anniv="";
	
	if(isset($_POST['date_autre']))      $date_autre=$_POST['date_autre'];
	else      $date_autre="";
	
	if(isset($_POST['notes']))      $notes=$_POST['notes'];
	else      $notes="";

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	

	$nom = mysqli_real_escape_string($db, $nom);
	$contact = mysqli_real_escape_string($db, $contact);
	$fonc_contact = mysqli_real_escape_string($db, $fonc_contact);
	$contact_autre = mysqli_real_escape_string($db, $contact_autre);
	$fonc_contact_autre = mysqli_real_escape_string($db, $fonc_contact_autre);
	$nature = mysqli_real_escape_string($db, $nature);
	$adresse = mysqli_real_escape_string($db, $adresse);
	$ville = mysqli_real_escape_string($db, $ville);
	$code_postal = mysqli_real_escape_string($db, $code_postal);
	$tel = mysqli_real_escape_string($db, $tel);
	$tel_portable = mysqli_real_escape_string($db, $tel_portable);
	$fax = mysqli_real_escape_string($db, $fax);
	$mail = mysqli_real_escape_string($db, $mail);
	$activite = mysqli_real_escape_string($db, $activite);
	$site_web = mysqli_real_escape_string($db, $site_web);
	$siret = mysqli_real_escape_string($db, $siret);
	$TVA_intra = mysqli_real_escape_string($db, $TVA_intra);
	$encours = mysqli_real_escape_string($db, $encours);
	$liste_commandes = mysqli_real_escape_string($db, $liste_commandes);
	$nb_facture = mysqli_real_escape_string($db, $nb_facture);
	$date_anniv = mysqli_real_escape_string($db, $date_anniv);
	$date_autre = mysqli_real_escape_string($db, $date_autre);
	$notes = mysqli_real_escape_string($db, $notes);

	mysqli_query($db, "INSERT into $db_compte_client (nom, contact, fonc_contact, contact_autre, fonc_contact_autre, nature, adresse, ville, code_postal, tel, tel_portable, fax, mail, activite, site_web, siret, TVA_intra, encours, liste_commandes, nb_facture, date_anniv, date_autre, notes, date_crea) VALUES ('$nom','$contact','$fonc_contact','$contact_autre','$fonc_contact_autre','$nature','$adresse','$ville','$code_postal','$tel','$tel_portable','$fax','$mail','$activite','$site_web','$siret','$TVA_intra','$encours','$liste_commandes','$nb_facture','$date_anniv','$date_autre','$notes','$date')")
	or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
	
	$nom = stripslashes($nom);
	$contact = stripslashes($contact);
	$fonc_contact = stripslashes($fonc_contact);
	$contact_autre = stripslashes($contact_autre);
	$fonc_contact_autre = stripslashes($fonc_contact_autre);
	$nature = stripslashes($nature);
	$adresse = stripslashes($adresse);
	$ville = stripslashes($ville);
	$code_postal = stripslashes($code_postal);
	$tel = stripslashes($tel);
	$tel_portable = stripslashes($tel_portable);
	$fax = stripslashes($fax);
	$mail = stripslashes($mail);
	$activite = stripslashes($activite);
	$site_web = stripslashes($site_web);
	$siret = stripslashes($siret);
	$TVA_intra = stripslashes($TVA_intra);
	$encours = stripslashes($encours);
	$liste_commandes = stripslashes($liste_commandes);
	$nb_facture = stripslashes($nb_facture);
	$date_anniv = stripslashes($date_anniv);
	$date_autre = stripslashes($date_autre);
	$notes = stripslashes($notes);
	
?>

	<p class="cen"><span class="validation">Informations enregistr&eacute;es</span></p>
	
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif client : </legend>
				
				<p>Nom : <strong><?php echo $nom;?></strong></p>
				
				<p>Nom contact pricipal: <strong><?php echo $contact;?></strong></p>
				
				<p>Fonction contact pricipal: <strong><?php echo $fonc_contact;?></strong></p>
				
				<p>Nom contact autre: <strong><?php echo $contact_autre;?></strong></p>
				
				<p>Fonction contact autre: <strong><?php echo $fonc_contact_autre;?></strong></p>
				
				<p>Nature : <strong><?php echo $nature;?></strong></p>
				
				<p>Adresse : <strong><?php echo $adresse;?></strong></p>
				
				<p>Ville : <strong><?php echo $ville;?></strong></p>
				
				<p>Code postal : <strong><?php echo $code_postal;?></strong></p>
				
				<p>Telephone : <strong><?php echo $tel;?></strong></p>
				
				<p>Telephone portable : <strong><?php echo $tel_portable;?></strong></p>
				
				<p>Fax : <strong><?php echo $fax;?></strong></p>
				
				<p>E-mail : <strong><?php echo $mail;?></strong></p>
				
				<p>Activite : <strong><?php echo $activite;?></strong></p>
				
				<p>Site web : <strong><?php echo $site_web;?></strong></p>
				
				<p>Siret : <strong><?php echo $siret;?></strong></p>
				
				<p>TVA intra-communautaire : <strong><?php echo $TVA_intra;?></strong></p>
				
				<p>Encours : <strong><?php echo $encours;?></strong></p>
				
				<p>Liste commandes : <strong><?php echo $liste_commandes;?></strong></p>
				
				<p>Date anniversaire : <strong><?php echo $date_anniv;?></strong></p>
				
				<p>Date autre : <strong><?php echo $date_autre;?></strong></p>
				
				<p>Notes : <strong><?php echo $notes;?></strong></p>
		
			</fieldset>
	
		<p class="cen"><a href="crea_cli.php">Revenir au formulaire d'ajout</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
	
		</div>
	
	</div>
		
<?php
		}
require_once 'Main_ft.php'; 
?>