<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout fournisseur</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter un nouveau fournisseur</strong>.
				</p>
				
			</div>
			<style>
				.fournisseur{
					width: 50%;
					margin: auto;
				}
				#feuille_para{
					height: 90px;
				}
			</style>
		<form action="req_crea_four.php" method="post" class="fournisseur">
			
			<fieldset>
				
				<legend class="lg"> Detail fournisseur : </legend>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom"/></p>
				
				<p><label class="gauche" for="adresse">Adresse :</label>
				<input class="droit" id="adresse" type="text" name="adresse"/></p>
				
				<p><label class="gauche" for="ville">Ville :</label>
				<input class="droit" id="ville" type="text" name="ville"/></p>
				
				<p><label class="gauche" for="code_postal">Code postal :</label>
				<input class="droit" id="code_postal" type="text" name="code_postal"/></p>
				
				<p><label class="gauche" for="telephone">Telephone :</label>
				<input class="droit" id="telephone" type="text" name="telephone"/></p>

				<p><label class="gauche" for="fax">Fax :</label>
				<input class="droit" id="fax" type="text" name="fax"/></p>
				
				<p><label class="gauche" for="email">E-mail :</label>
				<input class="droit" id="email" type="text" name="email"/></p>
				
				<p><label class="gauche" for="pays">Pays :</label>
				<input class="droit" id="pays" type="text" name="pays"/></p>
				
				<p><label class="gauche" for="site_web">Site web :</label>
				<input class="droit" id="site_web" type="text" name="site_web"/></p>
				
				<p><label class="gauche" for="nom_contact">Nom contact :</label>
				<input class="droit" id="nom_contact" type="text" name="nom_contact"/></p>
				
				<p><label class="gauche" for="tel_contact">Telephone contact :</label>
				<input class="droit" id="tel_contact" type="text" name="tel_contact"/></p>
				
				<p><label class="gauche" for="siret">Siret :</label>
				<input class="droit" id="siret" type="text" name="siret"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA intra-communautaire :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra"/></p>
				
				<p><label class="gauche" for="prod_prop">Produits propos&eacute;s :</label>
				<textarea class="droit" id="prod_prop" name="prod_prop"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="memo">Memo :</label>
				<textarea class="droit" id="memo" name="memo"></textarea></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Effacer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>