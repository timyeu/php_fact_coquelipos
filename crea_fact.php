<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<script type="text/javascript">
    function recupSelection(src, dest)
		{
        var valeur = src.options[src.selectedIndex].value;
        if (valeur = '')
        return;
         
        dest.value += src.options[src.selectedIndex].value + '\n';
        src.selectedIndex = 0;
		}
    </script>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Cr&eacute;ation document</h2>
			
				<p>
				Ce formulaire vous permet de <strong>cr&eacute;er un nouveau document</strong>.
				</p>
				
			</div>
			
<?php
	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
	
	$Requete = "SELECT designation, reference FROM $db_prod_prest WHERE nature = 'produit' ORDER by designation";
	$Requete2 = "SELECT designation, reference FROM $db_prod_prest WHERE nature = 'prestation' ORDER by designation";
	$Requete3 = "SELECT nom, contact FROM $db_compte_client ORDER by nom";
	$Requete4 = "SELECT nom_param FROM $db_parametres ORDER by nom_param";
	
	$ResReq = mysqli_query($db, $Requete) or die(mysqli_error($db)); 
	$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
	$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
	$ResReq4 = mysqli_query($db, $Requete4) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>');
				
	
?>
			
		<form action="crea_fact_sec.php" method="post">
			
			<fieldset>
				
				<legend class="lg"> Ent&ecirc;te : </legend>
				
				<p><label class="gauche" for="numfact">Num&eacute;ro de document</label>
				<input class="droit" id="numfact" type="text" name="numfact"/></p>
				
				<p class="cen"><strong>Le num&eacute;ro de document est compos&eacute; d'un pr&eacute;fixe D,C ou F 
				(pour devis, commande ou facture) et d'un suffixe mois/ann&eacute;e</strong></p>
			
				<p><label class="gauche" for="nom_param">S&eacute;lection des param&egrave;tres</label>
				<select id="nom_param" class="droit" name="nom_param">
					<option value=""></option>
					<?php
					while ($LigneDo4 = mysqli_fetch_array($ResReq4)) 
						{
						$nom_param = $LigneDo4["nom_param"];
						echo '<option value="'.$nom_param.'">'.$nom_param.'</option>';
						}
					?>
				</select></p>
				
				<p><label class="gauche" for="nom_client">S&eacute;lection du client (et contact si indiqu&eacute;)</label>
				<select id="nom_client" class="droit" name="nom_client">
					<option value=""></option>
					<?php
					while ($LigneDo3 = mysqli_fetch_array($ResReq3)) 
						{
						$nom_client = $LigneDo3["nom"];
						$contact_client = $LigneDo3["contact"];
						echo '<option value="'.$nom_client.'">'.$nom_client.' '.$contact_client.'</option>';
						}
					?>
				</select></p>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> S&eacute;lection de produits : </legend>
					
					<p><label class="gauche" for="produit_s">S&eacute;lection par d&eacute;signation</label>
					<select id="produit_s" class="droit" name="produit_s" onchange="recupSelection(this, this.form.produit)">
						<option value=""></option>
						<?php
						while ($LigneDo = mysqli_fetch_array($ResReq)) 
							{
							$designation_a = $LigneDo["designation"];
							$reference_a = $LigneDo["reference"];
							echo '<option value="'.$designation_a.' | '.$reference_a.' | ">'.$designation_a.' '.$reference_a.'</option>';
							}
						?>
					</select></p>
					
					<p class="cen"><textarea name="produit"></textarea></p>
					
					<p class="cen"><strong>Une fois la liste s&eacute;lectionn&eacute;e, 
					utilisez les touches du clavier pour une recherche rapide, s&eacute;lectionnez ensuite le produit voulu 
					pour l'inscrire dans le cadre ci-dessus. Vous pouvez ajouter des informations &agrave la suite</strong></p>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> S&eacute;lection des prestations : </legend>
			
					<p><label class="gauche" for="prestation_s">S&eacute;lection par d&eacute;signation</label>
					<select id="prestation_s" class="droit" name="prestation_s" onchange="recupSelection(this, this.form.prestation)">
						<option value=""></option>
						<?php
						while ($LigneDo2 = mysqli_fetch_array($ResReq2)) 
							{
							$designation_p = $LigneDo2["designation"];
							$reference_p = $LigneDo2["reference"];
							echo '<option value="'.$designation_p.' | '.$reference_p.' | ">'.$designation_p.' '.$reference_p.'</option>';
							}
						?>
					</select></p>
					
					<p class="cen"><textarea name="prestation"></textarea></p>
					
					<p class="cen"><strong>Une fois la liste s&eacute;lectionn&eacute;e, 
					utilisez les touches du clavier pour une recherche rapide, s&eacute;lectionnez ensuite la prestation voulue 
					pour l'inscrire dans le cadre ci-dessus. Vous pouvez ajouter des informations &agrave la suite</strong></p>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg">Date de validit&eacute; :</legend>
				
				<p><label class="gauche" for="date_valid_deb">Du :</label>
				<input class="droit" id="date_valid_deb" type="date" name="date_valid_deb"/></p>
			
				<p><label class="gauche" for="date_valid_fin">Au :</label>
				<input class="droit" id="date_valid_fin" type="date" name="date_valid_fin"/></p>
						
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Annuler"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>