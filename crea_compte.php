<?php 

session_start();

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	
		
require_once 'Main_hd.php'; 
include 'ccg_coquelipos_fact.php';

	if ($connect == "1") // Authentification
		{ 

?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout compte</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter un nouveau compte</strong>.
				</p>
				
			</div>
			
		<form action="req_crea_compte.php" method="post">
			
			<fieldset>
				
				<legend class="lg"> Detail compte : </legend>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom"/></p>
				
				<p><label class="gauche" for="memo">M&eacute;mo :</label>
				<textarea class="droit" id="memo" name="memo"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="id">Assigner identifiant :</label>
				<input class="droit" id="id" type="text" name="id"/></p>
				
				<p><label class="gauche" for="mdp">Assigner mot de passe :</label>
				<input class="droit" id="mdp" type="text" name="mdp"/></p>
				
				<p class="cen"><strong>Accorder droits d'administration ?&nbsp;&nbsp;&nbsp;<input type="checkbox" name="info_admin" value="1"/></strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Effacer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php 
		}
	else
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		}

require_once 'Main_ft.php';

?>