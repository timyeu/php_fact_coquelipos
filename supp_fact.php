<?php

session_start();
date_default_timezone_set('Europe/Paris');

if (isset($_SESSION['connect'])) {
    $connect = $_SESSION['connect'];
} else {
    $connect = 0;
}

if (isset($_SESSION['log'])) {
    $nom_membre = $_SESSION['log'];
} else {
    $nom_membre = 0;
}

include 'ccg_coquelipos_fact.php';

if ($connect != "1" && $connect != "2") {
    header('Location: http://' . $link_domain . '/Accueil.php');
    exit;
} else {
    require_once 'Main_hd.php';
    ?>

	<div id="feuille_imp">

<?php

    if (isset($_POST['ListFact'])) {
        $ListFact = $_POST['ListFact'];
    } else {
        $ListFact = "";
    }

    if (isset($_GET['num_fact'])) {
        $ListFact = $_GET['num_fact'];
    } else {
        $ListFact = $ListFact;
    }

    $Net_prod      = "";
    $Net_prest     = "";
    $Net_prod_tot  = "";
    $Net_prest_tot = "";
    $Net           = "";
    $type_aff      = "";

    $db = mysqli_connect($db_server, $db_user, $db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
    mysqli_select_db($db, $db_database) or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

    $Requete = "SELECT * FROM $db_facture_entete WHERE num_fact ='$ListFact'";

    $ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');

    $Donnees = mysqli_fetch_array($ResReq);

    $num_fact       = $Donnees["num_fact"];
    $nom_param      = $Donnees["nom_param"];
    $date_fact      = $Donnees["date_fact"];
    $date_valid_deb = $Donnees["date_valid_deb"];
    $date_valid_fin = $Donnees["date_valid_fin"];
    $total_HT       = $Donnees["total_HT"];
    $total_TVA1     = $Donnees["total_TVA1"];
    $total_TVA2     = $Donnees["total_TVA2"];
    $total_TVA3     = $Donnees["total_TVA3"];
    $total_TVA4     = $Donnees["total_TVA4"];
    $tot_produits   = $Donnees["tot_produits"];
    $tot_prestas    = $Donnees["tot_prestas"];
    $remise         = $Donnees["remise"];
    $total_TTC      = $Donnees["total_TTC"];
    $avoir          = $Donnees["avoir"];
    $type_paiement  = $Donnees["type_paiement"];
    $nom_membre     = $Donnees["nom_membre"];
    $nom_client     = $Donnees["nom_client"];
    $type           = $Donnees["type"];
    $etat           = $Donnees["etat"];
    $num_fact_det   = explode("-", $num_fact);

    $Requete2 = "SELECT * FROM $db_compte_client WHERE ref_clients ='$nom_client'";

    $ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');

    $Donnees2 = mysqli_fetch_array($ResReq2);

    $nom_cli         = $Donnees2["nom"];
    $contact_cli     = $Donnees2["contact"];
    $adresse_cli     = $Donnees2["adresse"];
    $ville_cli       = $Donnees2["ville"];
    $code_postal_cli = $Donnees2["code_postal"];

    $Requete5 = "SELECT * FROM $db_parametres WHERE ref ='$nom_param'";

    $ResReq5 = mysqli_query($db, $Requete5) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>');

    $Donnees5 = mysqli_fetch_array($ResReq5);

    $nom_param      = $Donnees5["nom_param"];
    $nom_entreprise = $Donnees5["nom_entreprise"];
    $add_entreprise = $Donnees5["add_entreprise"];
    $cp_ville       = $Donnees5["cp_ville"];
    $tel            = $Donnees5["tel"];
    $lieu           = $Donnees5["lieu"];
    $TVA_intra      = $Donnees5["TVA_intra"];
    $cond_devis     = $Donnees5["cond_devis"];
    $cond_commande  = $Donnees5["cond_commande"];
    $cond_facture   = $Donnees5["cond_facture"];
    $mentions       = $Donnees5["mentions"];
    $coord_banque   = $Donnees5["coord_banque"];
    $coord_RIB      = $Donnees5["coord_RIB"];
    $coord_IBAN     = $Donnees5["coord_IBAN"];
    $app            = $Donnees5["app"];
    $clause         = $Donnees5["clause"];
    $capital        = $Donnees5["capital"];
    $SIREN          = $Donnees5["SIREN"];
    $RCS            = $Donnees5["RCS"];
    $APE            = $Donnees5["APE"];
    $info_autre     = $Donnees5["info_autre"];
    $logo_param     = $Donnees5["logo_param"];

    $app          = explode("-", $app);
    $app_devis    = $app[0];
    $app_commande = $app[1];
    $app_facture  = $app[2];

    $cond_devis    = nl2br($cond_devis);
    $cond_commande = nl2br($cond_commande);
    $cond_facture  = nl2br($cond_facture);

    $Requete3                      = "SELECT * FROM $db_facture_detail WHERE num_fact='$ListFact' AND type='produit'";
    $ResReq3                       = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, facture incorrecte ou inexistante</span>');
    $nbenreg3                      = mysqli_num_rows($ResReq3);
    $nbchamps3                     = mysqli_num_fields($ResReq3);
    $Tabdo3[$nbenreg3][$nbchamps3] = "";
    $Tabchamps3[$nbchamps3]        = "";

    for ($K = 0; $K < $nbchamps3; $K++) {
        $tabchamps3[$K] = mysqli_fetch_field_direct($ResReq3, $K);
    }

    $K = 0;

    while ($donnees3 = mysqli_fetch_array($ResReq3)) {
        $Tabdo3[$K][1]  = $donnees3["designation"];
        $Tabdo3[$K][2]  = $donnees3["reference"];
        $Tabdo3[$K][3]  = $donnees3["prix"];
        $Tabdo3[$K][4]  = $donnees3["quantite"];
        $Tabdo3[$K][5]  = $donnees3["taux_TVA"];
        $Tabdo3[$K][6]  = $donnees3["remise"];
        $Tabdo3[$K][7]  = $donnees3["totHT"];
        $Tabdo3[$K][8]  = $donnees3["TVA"];
        $Tabdo3[$K][9]  = $donnees3["totTTC"];
        $Tabdo3[$K][10] = $donnees3["info_comp"];
        $K++;
    }

    $Requete4                      = "SELECT * FROM $db_facture_detail WHERE num_fact='$ListFact' AND type='prestation'";
    $ResReq4                       = mysqli_query($db, $Requete4) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, facture incorrecte ou inexistante</span>');
    $nbenreg4                      = mysqli_num_rows($ResReq4);
    $nbchamps4                     = mysqli_num_fields($ResReq4);
    $Tabdo4[$nbenreg4][$nbchamps4] = "";
    $Tabchamps4[$nbchamps4]        = "";

    for ($L = 0; $L < $nbchamps4; $L++) {
        $tabchamps4[$L] = mysqli_fetch_field_direct($ResReq4, $L);
    }

    $L = 0;

    while ($donnees4 = mysqli_fetch_array($ResReq4)) {
        $Tabdo4[$L][1]  = $donnees4["designation"];
        $Tabdo4[$L][2]  = $donnees4["reference"];
        $Tabdo4[$L][3]  = $donnees4["prix"];
        $Tabdo4[$L][4]  = $donnees4["quantite"];
        $Tabdo4[$L][5]  = $donnees4["taux_TVA"];
        $Tabdo4[$L][6]  = $donnees4["remise"];
        $Tabdo4[$L][7]  = $donnees4["totHT"];
        $Tabdo4[$L][8]  = $donnees4["TVA"];
        $Tabdo4[$L][9]  = $donnees4["totTTC"];
        $Tabdo4[$L][10] = $donnees4["info_comp"];
        $L++;
    }

    ?>

		<table style="border: none;">

			<tr>

<?php
if ($logo_param != "") {
        echo '<td style="border: none; width: 40%; text-align: left;"><p><img style="margin-left: 10%;" src="Images/' . $logo_param . '"/></p></td>';
    } else {
        echo '<td style="border: none; width: 40%; text-align: left;"><p></p></td>';
    }

    if ($num_fact_det[0] == "FA") {
        $type_aff = 'facture d\'avoirs';
    } else {
        $type_aff = $type;
    }
    ?>
				<td style="border: none; width: 50%; text-align: center;">
					<p class="imp_6"><?php echo $type_aff; ?> N&deg; :&nbsp;<?php echo $num_fact; ?></p>
<?php
if ($date_valid_deb != "" && $date_valid_fin != "") {
        if ($type == "devis") {
            echo '<p class="imp_4">Date de validit&eacute; du : <strong class="imp_5">' . $date_valid_deb . '</strong> au : <strong class="imp_5">' . $date_valid_fin . '</strong></p>';
        }

        if ($type == "facture") {
            echo '<p class="imp_4">D&eacute;lais de paiement du : <strong class="imp_5">' . $date_valid_deb . '</strong> au : <strong class="imp_5">' . $date_valid_fin . '</strong></p>';
        }
    }
    ?>
				</td>

			</tr>

		</table>

		<table style="border: none;" >

			<tr>

				<td style="border: none; width: 60%; text-align: left;">
					<p class="imp_6"><?php echo $nom_entreprise; ?></p>
					<p class="imp_4"><?php echo $add_entreprise; ?></p>
					<p class="imp_4"><?php echo $cp_ville; ?></p>
					<p class="imp_4"><?php echo $tel; ?></p>
					<p class="imp_4"><?php echo $TVA_intra; ?></p>
<?php
if ($capital != "") {
        echo '<p class="imp_4">Capital social : ' . $capital . ' euros</p>';
    }
    if ($SIREN != "") {
        echo '<p class="imp_4">SIREN : ' . $SIREN . '</p>';
    }
    if ($RCS != "") {
        echo '<p class="imp_4">RCS : ' . $RCS . '</p>';
    }
    if ($APE != "") {
        echo '<p class="imp_4">Code APE : ' . $APE . '</p>';
    }
    ?>
				</td>
				<td style="border: none; width: 40%; text-align: left;">
					<p class="imp_6"><?php echo $nom_cli; ?></p>
					<p class="imp_4"><?php echo $contact_cli; ?></p>
					<p class="imp_4"><?php echo $adresse_cli; ?></p>
					<p class="imp_4"><?php echo $code_postal_cli . ' ' . $ville_cli; ?></p>
				</td>

			</tr>

		</table>

		<table style="border: none;">

			<tr>

				<td style="border: none; width: 60%;"></td>
				<td style="border: none; width: 40%; text-align: left;">
					<p class="imp_6">Date : <strong class="imp_5"><?php echo $date_fact; ?></strong></p>
				</td>

			</tr>

		</table>

<?php

    echo '<table style="page-break-inside: avoid;" id="table-title">

		<tr>

			<td style="width: 15%;"><p class="imp_4"><strong class="imp_5">R&Eacute;F&Eacute;RENCE</strong></p></td>
			<td style="width: 45%;"><p class="imp_4"><strong class="imp_5">DESCRIPTION</strong></p></td>
			<td style="width: 10%;"><p class="imp_4"><strong class="imp_5">QUANTIT&Eacute;</strong></p></td>
			<td style="width: 15%;"><p class="imp_4"><strong class="imp_5">PRIX UNITAIRE &euro; HT</strong></p></td>
			<td style="width: 15%;"><p class="imp_4"><strong class="imp_5">MONTANT &euro; HT</strong></p></td>

		</tr>';

    if ($nbenreg3 > 0) {
        for ($K = 0; $K < $nbenreg3; $K++) {
            $Requete6          = "SELECT informations FROM $db_prod_prest WHERE designation ='" . mysqli_real_escape_string($db, $Tabdo3[$K][1]) . "' AND reference ='" . mysqli_real_escape_string($db, $Tabdo3[$K][2]) . "'";
            $ResReq6           = mysqli_query($db, $Requete6) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>');
            $Donnees6          = mysqli_fetch_array($ResReq6);
            $informations_prod = $Donnees6["informations"];

            if (substr($Tabdo3[$K][6], -1) == "%") {
                $Net_prod   = $Tabdo3[$K][9] - ($Tabdo3[$K][9] * (substr($Tabdo3[$K][6], 0, -1) / 100));
                $aff_remise = $Tabdo3[$K][6];
            } else {
                $Net_prod   = $Tabdo3[$K][9] - $Tabdo3[$K][6];
                $aff_remise = sprintf('%.2f &euro;', $Tabdo3[$K][6]);
            }

            $Net_prod_tot = $Net_prod_tot + $Net_prod;
            $Net          = $Net + $Net_prod;

            echo '<tr>';

            if ($Tabdo3[$K][6] != "0") {
                echo '<td style="width: 15%; text-align: left;"><p class="imp_4">' . $Tabdo3[$K][2] . '</p></td>
							<td style="width: 45%; text-align: left;"><p class="imp_4">' . $Tabdo3[$K][1] . '</p>
							<p class="imp_4">' . $informations_prod . '</p>
							<p class="imp_4">' . $Tabdo3[$K][10] . '</p>
							<p class="imp_4"><strong class="imp_5">Remise : ' . $aff_remise . '</strong></p></td>';
            } else {
                echo '<td style="width: 15%; text-align: left;"><p class="imp_4">' . $Tabdo3[$K][2] . '</p></td>
							<td style="width: 45%; text-align: left;"><p class="imp_4">' . $Tabdo3[$K][1] . '</p>
							<p class="imp_4">' . $informations_prod . '</p>
							<p class="imp_4">' . $Tabdo3[$K][10] . '</p></td>';
            }
            echo '<td style="width: 10%; text-align: center;"><p class="imp_4">' . $Tabdo3[$K][4] . '</p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4">' . sprintf('%.2f &euro;', $Tabdo3[$K][3]) . '</p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4">' . sprintf('%.2f &euro;', $Tabdo3[$K][7]) . '</p></td>
						</tr>';
        }
        $nbvide = 5 - $nbenreg3;
        for ($V = 0; $V < $nbvide; $V++) {
            echo '<tr>
							<td style="width: 15%; text-align: left;"><p class="imp_4"> </p></td>
							<td style="width: 45%; text-align: left;"><p class="imp_4"> </p></td>
							<td style="width: 10%; text-align: center;"><p class="imp_4"> </p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4"> </p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4">- &euro;</p></td>
						</tr>';
        }
    }

    echo '</table>';

    if ($nbenreg4 > 0) {

        echo '<table style="page-break-inside: avoid;" id="table-data">';

        for ($L = 0; $L < $nbenreg4; $L++) {
            $Requete7           = "SELECT informations FROM $db_prod_prest WHERE designation ='" . mysqli_real_escape_string($db, $Tabdo4[$L][1]) . "' AND reference ='" . mysqli_real_escape_string($db, $Tabdo4[$L][2]) . "'";
            $ResReq7            = mysqli_query($db, $Requete7) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>');
            $Donnees7           = mysqli_fetch_array($ResReq7);
            $informations_prest = $Donnees7["informations"];

            if (substr($Tabdo4[$L][6], -1) == "%") {
                $Net_prest  = $Tabdo4[$L][9] - ($Tabdo4[$L][9] * (substr($Tabdo4[$L][6], 0, -1) / 100));
                $aff_remise = $Tabdo4[$L][6];
            } else {
                $Net_prest  = $Tabdo4[$L][9] - $Tabdo4[$L][6];
                $aff_remise = sprintf('%.2f &euro;', $Tabdo4[$L][6]);
            }

            $Net_prest_tot = $Net_prest_tot + $Net_prest;
            $Net           = $Net + $Net_prest;

            echo '<tr>';

            if ($Tabdo4[$L][6] != "0") {
                echo '<td style="width: 15%; text-align: left;"><p class="imp_4">' . $Tabdo4[$L][2] . '</p></td>
							<td style="width: 45%; text-align: left;"><p class="imp_4">' . $Tabdo4[$L][1] . '</p>
							<p class="imp_4">' . $informations_prest . '</p>
							<p class="imp_4">' . $Tabdo4[$L][10] . '</p>
							<p class="imp_4"><strong class="imp_5">Remise : ' . $aff_remise . '</strong></p></td>';
            } else {
                echo '<td style="width: 15%; text-align: left;"><p class="imp_4">' . $Tabdo4[$L][2] . '</p></td>
							<td style="width: 45%; text-align: left;"><p class="imp_4">' . $Tabdo4[$L][1] . '</p>
							<p class="imp_4">' . $informations_prest . '</p>
							<p class="imp_4">' . $Tabdo4[$L][10] . '</p></td>';
            }

            echo '<td style="width: 10%; text-align: center;"><p class="imp_4">' . $Tabdo4[$L][4] . '</p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4">' . sprintf('%.2f &euro;', $Tabdo4[$L][3]) . '</p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4">' . sprintf('%.2f &euro;', $Tabdo4[$L][7]) . '</p></td>
						</tr>';
        }

        $nbvide2 = 5 - $nbenreg4;
        for ($W = 0; $W < $nbvide2; $W++) {
            echo '<tr>
							<td style="width: 15%; text-align: left;"><p class="imp_4"> </p></td>
							<td style="width: 45%; text-align: left;"><p class="imp_4"> </p></td>
							<td style="width: 10%; text-align: center;"><p class="imp_4"> </p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4"> </p></td>
							<td style="width: 15%; text-align: right;"><p class="imp_4">- &euro;</p></td>
						</tr>';
        }
    }

    echo '</table>';

    ?>

	<table style="border: none;" id="table-total">

		<tr>
			<td colspan="2" style="border: none; width: 60%;"></td>
			<td colspan="2" style="width: 25%;"><p class="imp_6">Total Hors Taxes</p></td>
			<td style="width: 15%; text-align: right;"><p class="imp_6"><?php echo sprintf('%.2f &euro;', $total_HT); ?></strong></p></td>
		</tr>

<?php

    if ($total_TVA1 != "0") {
        echo '<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Tot TVA 20%</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6">' . sprintf('%.2f &euro;', $total_TVA1) . '</strong></p></td>
				</tr>';
    }
    if ($total_TVA2 != "0") {
        echo '<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Tot TVA 10%</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6">' . sprintf('%.2f &euro;', $total_TVA2) . '</strong></p></td>
				</tr>';
    }
    if ($total_TVA3 != "0") {
        echo '<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Tot TVA 5.5%</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6">' . sprintf('%.2f &euro;', $total_TVA3) . '</strong></p></td>
				</tr>';
    }
    if ($total_TVA4 != "0") {
        echo '<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Tot TVA 2.1%</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6">' . sprintf('%.2f &euro;', $total_TVA4) . '</strong></p></td>
				</tr>';
    }
    if ($remise != "") {
        echo '<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Remise</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6">' . sprintf('%.2f &euro;', $remise) . '</strong></p></td>
				</tr>';
    }
    if ($avoir != "0") {
        echo '<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Avoirs</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6">' . sprintf('%.2f &euro;', $avoir) . '</strong></p></td>
				</tr>';
    }

    if (substr($remise, -1) == "%") {
        $Net = $Net - ($Net * (substr($remise, 0, -1) / 100));
    } else {
        $Net = $Net - $remise;
    }
    $Net = $Net - $avoir;

    ?>
			<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Total TTC</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6"><?php echo sprintf('%.2f &euro;', $total_TTC); ?></strong></p></td>
			</tr>

			<tr>
				<td colspan="2" style="border: none; width: 60%;"></td>
				<td colspan="2" style="width: 25%;"><p class="imp_6">Net</p></td>
				<td style="width: 15%; text-align: right;"><p class="imp_6"><?php echo sprintf('%.2f &euro;', $Net); ?></strong></p></td>
			</tr>

	</table>

	<div style="margin-top: 20px; margin-bottom: 20px; padding: 0px 20px;">
		<button style="float: right; cursor: pointer; padding: 5px 10px; border-raduius: 2px" onclick="renderPdf()">Aperçu & Imprimer</button>
	</div>
	<script type="text/javascript">
	function renderPdf() {
		var form = document.createElement('form');
		form.method = "POST";
		form.target = "_blank";
		form.action = "/pdf_export/render";
		form.setAttribute("style","display: none");
		var input = document.createElement('input');
        var html = '';
        if(document.getElementById('table-title') != undefined) {
            html += document.getElementById('table-title').innerHTML;
        }
        if(document.getElementById('table-data') != undefined) {
            html += document.getElementById('table-data').innerHTML;
        }
        if(document.getElementById('table-total') != undefined) {
            html += document.getElementById('table-total').innerHTML;
        }
		var style = '<style type="text/stylesheet">table{font-size: 12px;border-collapse: collapse;} table td {border: 1px solid black; padding: 5px}</style>';
		var html = style + '<table>' + html + '</table>';
		input.name = "html";
		input.value = html;
		form.appendChild(input);
		document.body.appendChild(form);
		form.submit();
	}
	</script>

	<table style="border: none; page-break-inside: avoid;">

		<tr>

	<?php

    echo '<td style="border: none; width: 50%;">';
    if ($app_facture == "AF" && $type == "facture") {
        echo '<p class="imp_2">Coordonn&eacute;es bancaires RIB ' . $coord_RIB . '</p>
				<p class="imp_2">' . $coord_banque . ' IBAN ' . $coord_IBAN . '</p>
				<p></p>
				<p class="imp_2">' . $clause . '.</p>';
    }
    if ($app_commande == "AC" && $type == "commande") {
        echo '<p class="imp_2">Coordonn&eacute;es bancaires RIB ' . $coord_RIB . '</p>
				<p class="imp_2">' . $coord_banque . ' IBAN ' . $coord_IBAN . '</p>
				<p></p>
				<p class="imp_2">' . $clause . '.</p>';
    }
    if ($app_devis == "AD" && $type == "devis") {
        echo '<p class="imp_2">Coordonn&eacute;es bancaires RIB ' . $coord_RIB . '</p>
				<p class="imp_2">' . $coord_banque . ' IBAN ' . $coord_IBAN . '</p>
				<p></p>
				<p class="imp_2">' . $clause . '.</p>';
    }
    echo '</td>

			<td style="border: none; width: 10%;">
			</td>

			<td style="border: none; width: 40%;">';
    if ($type == "facture") {
        echo '<p class="imp_1">Conditions de r&egrave;glement :</p>
				<p class="imp_3">' . $cond_facture . '.</p>';
    }
    if ($type == "devis") {
        echo '<p class="imp_1">Conditions de r&egrave;glement :</p>
				<p class="imp_3">' . $cond_devis . '.</p>';
    }
    if ($type == "commande") {
        echo '<p class="imp_1">Conditions de r&egrave;glement :</p>
				<p class="imp_3">' . $cond_commande . '.</p>';
    }
    echo '<p class="imp_2">' . $mentions . '</p>
			</td>

		</tr>

	</table>';
    ?>

	<table style="border: none; page-break-inside: avoid;">

		<tr>

			<td style="border: none;"><p class="imp_3">
<?php
echo '</p>';
    if ($info_autre != "") {
        echo '<p class="imp_2">' . $info_autre . '</p>';
    }
    ?>
			</td>

		</tr>

	</table>

	<form action="req_supp_fact.php" method="post">

		<input type="hidden" name="ref_fact" value="<?php echo $num_fact; ?>" />
		<input type="hidden" name="type" value="<?php echo $type; ?>" />
		<input type="hidden" name="ref_client" value="<?php echo $nom_client; ?>" />
		<input type="hidden" name="etat_anc" value="<?php echo $etat; ?>" />
		<fieldset>

			<legend> Validation : </legend>

<?php

    echo '<p class="cen">Type de paiement : <strong>' . $type_paiement . '</strong></p>';
    if ($type == "facture") {
        echo '<p><label class="gauche" for="etat">Statut</label>
				<select id="etat" class="droit" name="etat">
					<option value="' . $etat . '">' . $etat . '</option>
					<option value="en cours">en cours</option>
					<option value="en attente">en attente</option>
					<option value="validation">validation</option>
					<option value="annulation">annulation</option>
					<option value="erreur client">erreur client</option>
					<option value="erreur saisie">erreur saisie</option>
					<option value="attente de paiement">attente de paiement</option>
				</select>
			</p>';
    } else if ($type == "commande") {
        echo '<p><label class="gauche" for="etat">Statut</label>
				<select id="etat" class="droit" name="etat">
					<option value="' . $etat . '">' . $etat . '</option>
					<option value="en cours">en cours</option>
					<option value="en attente">en attente</option>
					<option value="annulation">annulation</option>
					<option value="erreur client">erreur client</option>
					<option value="erreur saisie">erreur saisie</option>
				</select>
			</p>';
    } else {
        echo '<p><label class="gauche" for="etat">Statut</label>
				<select id="etat" class="droit" name="etat">
					<option value="' . $etat . '">' . $etat . '</option>
					<option value="en cours">en cours</option>
					<option value="en attente">en attente</option>
					<option value="annulation">annulation</option>
					<option value="perte">perte</option>
					<option value="erreur client">erreur client</option>
					<option value="erreur saisie">erreur saisie</option>
				</select>
			</p>';
    }
    ?>

			<p class="cen">
				<input type="submit" value="Changer le statut"/>
			</p>

		</fieldset>

	</form>

	</div>

<?php
}
require_once 'Main_ft.php';
?>