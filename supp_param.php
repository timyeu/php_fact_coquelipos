<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Suppression param&egrave;tres</h2>
			
				<p>
				Ce formulaire vous permet de <strong>supprimer des param&egrave;tres relatifs aux factures, devis et commandes existants</strong>.
				</p>
				
			</div>

<?php

	if (isset($_POST['ListParam'])) $ListParam=$_POST['ListParam'];
		else $ListParam="";
		
	if (isset($_GET['ref'])) $ListParam=$_GET['ref'];
		else $ListParam=$ListParam;
		
	$Requete = "SELECT * FROM $db_parametres WHERE ref ='$ListParam'";

		if (empty($ListParam))
			{
				echo "Il faut selectionner des param&egrave;tres dans la liste";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>'); 
				
				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref=$Donnees["ref"];
				$nom_param=$Donnees["nom_param"];
				$nom_entreprise=$Donnees["nom_entreprise"];
				$add_entreprise=$Donnees["add_entreprise"];
				$cp_ville=$Donnees["cp_ville"];
				$tel=$Donnees["tel"];
				$lieu=$Donnees["lieu"];
				$TVA_intra=$Donnees["TVA_intra"];
				$cond_devis=$Donnees["cond_devis"];
				$cond_commande=$Donnees["cond_commande"];
				$cond_facture=$Donnees["cond_facture"];
				$coord_banque=$Donnees["coord_banque"];
				$coord_RIB=$Donnees["coord_RIB"];
				$coord_IBAN=$Donnees["coord_IBAN"];
				$app=$Donnees["app"];
				$clause=$Donnees["clause"];
				$capital=$Donnees["capital"];
				$SIREN=$Donnees["SIREN"];
				$RCS=$Donnees["RCS"];
				$APE=$Donnees["APE"];
				$info_autre =$Donnees["info_autre"];
				
				$cond_devis=nl2br($cond_devis);
				$cond_commande=nl2br($cond_commande);
				$cond_facture=nl2br($cond_facture);
			}

?>
			
		<form action="req_supp_param.php" method="post">
		<input type="hidden" name="ref" id="ref" value="<?php echo $ref;?>" />
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif param&egrave;tres : </legend>
				
				<p>Raison sociale et nom de l'entreprise : <strong><?php echo $nom_param;?></strong></p>
				
				<p>Adresse : <strong><?php echo $nom_entreprise;?></strong></p>
				
				<p>Code Postal et ville : <strong><?php echo $add_entreprise;?></strong></p>
				
				<p>Telephone : <strong><?php echo $tel;?></strong></p>
				
				<p>Lieu d'ex&eacute;cution des travaux : <strong><?php echo $lieu;?></strong></p>
				
				<p>TVA Intra : <strong><?php echo $TVA_intra;?></strong></p>
				
				<p>Conditions de r&egrave;glement, mentions, infos associ&eacute;es (devis) : </p>
				
				<p><strong><?php echo $cond_devis;?></strong></p>
				
				<p>Conditions de r&egrave;glement, mentions, infos associ&eacute;es (commande) : </p>
				
				<p><strong><?php echo $cond_commande;?></strong></p>
				
				<p>Conditions de r&egrave;glement, mentions, infos associ&eacute;es (facture) : </p>
				
				<p><strong><?php echo $cond_facture;?></strong></p>
				
				<p>Mentions autre : <strong><?php echo $mentions;?></strong></p>
				
				<p>Banque : <strong><?php echo $coord_banque;?></strong></p>
				
				<p>RIB : <strong><?php echo $coord_RIB;?></strong></p>
				
				<p>IBAN : <strong><?php echo $coord_IBAN;?></strong></p>
				
				<p>Clause de r&eacute;serve de propri&eacute;t&eacute; : <strong><?php echo $clause;?></strong></p>
				
				<p>Capital social : <strong><?php echo $capital;?> &euro;</strong></p>
				
				<p>SIREN : <strong><?php echo $SIREN;?></strong></p>
				
				<p>RCS : <strong><?php echo $RCS;?></strong></p>
				
				<p>APE : <strong><?php echo $APE;?></strong></p>
				
				<p>Information autre : <strong><?php echo $info_autre;?></strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Supprimer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_supp_param.php">Revenir &agrave; la liste de suppression des param&egrave;tres</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>