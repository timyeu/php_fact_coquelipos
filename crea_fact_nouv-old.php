<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

$date_fact=date('d/m/Y');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>
	
	<script type="text/javascript">
	function valider()
		{
		if(document.formSaisie.numfact.value != "") 
			{  
			return true;
			}
		else 
			{
			alert("Il faut saisir un num&eacute;ro de document");
			return false;
			}
		}
	</script>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Cr&eacute;ation document</h2>
			
				<p>
				Ce formulaire vous permet de <strong>cr&eacute;er un nouveau document</strong>.
				</p>
				
			</div>
			
<?php

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
	
	$Requete = "SELECT designation, reference FROM $db_prod_prest WHERE nature = 'produit' ORDER by designation";
	$Requete2 = "SELECT designation, reference FROM $db_prod_prest WHERE nature = 'prestation' ORDER by designation";
	$Requete3 = "SELECT nom, contact FROM $db_compte_client ORDER by nom";
	$Requete4 = "SELECT nom_param FROM $db_parametres ORDER by nom_param";
	
	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
	$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
	$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
	$ResReq4 = mysqli_query($db, $Requete4) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>');

	$nbenreg = mysqli_num_rows($ResReq);
	$nbchamps = mysqli_num_fields($ResReq);
	$Tabdo[$nbenreg][$nbchamps]="";
	$Tabchamps[$nbchamps]="";
						
		for($I=0; $I < $nbchamps; $I++) 
			{
			$tabchamps[$I] = mysqli_fetch_field_direct($ResReq,$I);
			}
							
		$I=0;
								
		while ($donnees = mysqli_fetch_array($ResReq))
			{	
			$Tabdo[$I][1]=$donnees["designation"];
			$Tabdo[$I][2]=$donnees["reference"];
			$I++;
			}
			
	$nbenreg2 = mysqli_num_rows($ResReq2);
	$nbchamps2 = mysqli_num_fields($ResReq2);
	$Tabdo2[$nbenreg2][$nbchamps2]="";
	$Tabchamps2[$nbchamps2]="";
						
		for($K=0; $K < $nbchamps2; $K++) 
			{
			$tabchamps2[$K] = mysqli_fetch_field_direct($ResReq2,$K);
			}
							
		$K=0;
								
		while ($donnees2 = mysqli_fetch_array($ResReq2))
			{	
			$Tabdo2[$K][1]=$donnees2["designation"];
			$Tabdo2[$K][2]=$donnees2["reference"];
			$K++;
			}
?>
	    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
	    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/css/select2.min.css" rel="stylesheet" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	    <script src="js/fact.js"></script>	
		<form action="req_crea_fact_nouv.php" name="formSaisie" onsubmit="return valider()" method="post">
			
			<fieldset>
				
				<legend class="lg"> Ent&ecirc;te : </legend>
				
				<p><label class="gauche" for="numfact">Num&eacute;ro de document</label>
				<input class="droit" id="numfact" type="text" name="numfact"/></p>
				
				<p class="cen"><strong>Le num&eacute;ro de document est compos&eacute; d'un pr&eacute;fixe D,C ou F 
				(pour devis, commande ou facture) et d'un suffixe mois/ann&eacute;e</strong></p>
				
				<p><label class="gauche" for="date_fact">Date du document</label>
				<input class="droit" id="date_fact" type="text" name="date_fact" value="<?php echo $date_fact;?>"/></p>
				
				<p class="cen"><strong>La date de cr&eacute;ation du document peut &ecirc;tre chang&eacute;e</strong></p>
			
				<p>
					<label class="gauche" for="nom_param">S&eacute;lection des param&egrave;tres</label>
					<select id="nom_param" class="droit" name="nom_param">
						<option value=""></option>
						<?php
						while ($LigneDo4 = mysqli_fetch_array($ResReq4)) 
							{
							$nom_param = $LigneDo4["nom_param"];
							echo '<option value="'.$nom_param.'">'.$nom_param.'</option>';
							}
						?>
					</select>
				</p>
					
				<p class="select-client">
					<label class="gauche" for="nom_client">S&eacute;lection du client (et contact si indiqu&eacute;)</label>
					<select id="nom_client" class="droit" name="nom_client">
						<option value=""></option>
						<?php
						while ($LigneDo3 = mysqli_fetch_array($ResReq3)) 
							{
							$nom_client = $LigneDo3["nom"];
							$contact_client = $LigneDo3["contact"];
							echo '<option value="'.$nom_client.'">'.$nom_client.' '.$contact_client.'</option>';
							}
						?>
					</select>
				</p>
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> S&eacute;lection de produits : </legend>
					
					<table id="product-table">
						<tr>
							<th>Designation | R&eacute;f&eacute;rence</th>
							<th>Quantit&eacute;</th>
							<th>Informations</th>
						</tr>
						<tr class="product-row">
							<td>
								<select name="produit[]" class="select-product">
									<option value=""></option>
									<?php
									for ($J=0; $J < $nbenreg; $J++) {
										echo '<option value="'.$Tabdo[$J][1].'|'.$Tabdo[$J][2].'">'.$Tabdo[$J][1].' | '.$Tabdo[$J][2].'</option>';
									}
									?>
								</select>
								<span>
									<i class="fa fa-plus addProductBtn"></i>
								</span>
								<span>
									<i class="fa fa-minus removeProductBtn"></i>
								</span>
							</td>
							<td><input style="width: 50px;" type="text" name="quant[]"/></td>
							<td><input type="text" name="info_prod[]"/></td>
						</tr>
					</table>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> S&eacute;lection de prestations : </legend>
					
					<table class="desc-table">
						<thead>
							<tr>
								<th>Designation</th>
								<th>Quantit&eacute;</th>
								<th>Informations</th>
							</tr>
						</thead>
						<tbody>
							<tr class="desc-row">
								<td>
									<select name="prestation[]" class="select-desc">
										<option value=""></option>
										<?php
											for($L=0; $L < $nbenreg2; $L++) {
												echo '<option value="'.$Tabdo2[$L][1].'|'.$Tabdo2[$L][2].'">'.$Tabdo2[$L][1].' '.$Tabdo2[$L][2].'</option>';
											}
										?>
									</select>
									<span>
										<i class="fa fa-plus addDescBtn"></i>
									</span>
									<span>
										<i class="fa fa-minus removeDescBtn"></i>
									</span>
								</td>
								<td><input style="width: 50px;" type="text" name="quant2[]"/></td>
								<td><input type="text" name="info_prest[]"/></td>
							</tr>
						</tbody>						
					</table>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg">Date de validit&eacute; :</legend>
				
				<p><label class="gauche" for="date_valid_deb">Du :</label>
				<input class="droit" id="date_valid_deb" type="date" name="date_valid_deb"/></p>
			
				<p><label class="gauche" for="date_valid_fin">Au :</label>
				<input class="droit" id="date_valid_fin" type="date" name="date_valid_fin"/></p>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Remise : </legend>
					
					<p><label class="gauche" for="remise_tot">Remise :</label>
					<input class="droit" id="remise_tot" type="text" name="remise_tot"/></p>
						
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">Enregistrer ce document en tant que :</p>
				
				<p class="cen">
					Devis ? <input type="radio" name="type" checked value="devis"/>
					Commande ? <input type="radio" name="type" value="commande"/>
					Facture ? <input type="radio" name="type" value="facture"/>
					Facture d'avoirs ? <input type="radio" name="type" value="fact_av"/>
				</p>
				
				<p class="cen"><strong>L'option devis est coch&eacute;e par d&eacute;faut</strong></p>
				
				<p><label class="gauche" for="type_paiement">Type de paiement (si facture):</label>
					<select class="droit" id="type_paiement" name="type_paiement">
						<option value="carte">carte</option>
						<option value="ch&egrave;que">ch&egrave;que</option>
						<option value="esp&egrave;ces">esp&egrave;ces</option>
						<option value="autre">autre</option>
					</select>
				</p>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Annuler"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>