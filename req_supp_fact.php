<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>
	
	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Statut document</h2>
			
			</div>

<?php
		
//Récupération des variables
	
	if(isset($_POST['ref_fact']))      $ref_fact=$_POST['ref_fact'];
	else      $ref_fact="";
	
	if(isset($_POST['type']))      $type=$_POST['type'];
	else      $type="";
	
	if(isset($_POST['etat']))      $etat=$_POST['etat'];
	else      $etat="";
	
	if(isset($_POST['etat_anc']))      $etat_anc=$_POST['etat_anc'];
	else      $etat_anc="";
	
	if(isset($_POST['ref_client']))      $ref_client=$_POST['ref_client'];
	else      $ref_client="";
	
	$info_devis="";
	$info_commande="";
	$info_facture="";
		
	// Connection et ouverture de la base	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	$Requete = "SELECT * FROM $db_facture_detail WHERE num_fact='$ref_fact'";
	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection</span>'); 
	
	if ($etat=="annulation")
		{
		if ($type=="facture" || $type=="commande")
			{
			while ($LigneDo = mysqli_fetch_array($ResReq)) 
				{
				$Nmn = $LigneDo["designation"];
				$Nmr = $LigneDo["reference"];
				$Nmqte = $LigneDo["quantite"];
				$Nmn = mysqli_real_escape_string($db, $Nmn);
				$Nmr = mysqli_real_escape_string($db, $Nmr);
				$Requete2 = "SELECT nature, quantite FROM $db_prod_prest WHERE designation='$Nmn' AND reference='$Nmr'";
				$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection</span>'); 
				$Donnees = mysqli_fetch_array($ResReq2);
				$quant=$Donnees["quantite"];
				$nature=$Donnees["nature"];
				
				if ($nature=="produit")
					{
					$quantfin=$quant+$Nmqte;
					mysqli_query($db, "UPDATE $db_prod_prest SET quantite='$quantfin' WHERE designation='$Nmn' AND reference='$Nmr'")
					or die('<span class="err_bdd">Erreur lors de la suppression</span>');
					}
				}
			}
		}
		
	if ($etat_anc=="annulation")
		{
		if ($etat!="annulation")
			{
			if ($type=="facture" || $type=="commande")
				{
				while ($LigneDo = mysqli_fetch_array($ResReq)) 
					{
					$Nmn = $LigneDo["designation"];
					$Nmr = $LigneDo["reference"];
					$Nmqte = $LigneDo["quantite"];
					$Nmn = mysqli_real_escape_string($db, $Nmn);
					$Nmr = mysqli_real_escape_string($db, $Nmr);
					$Requete2 = "SELECT nature, quantite FROM $db_prod_prest WHERE designation='$Nmn' AND reference='$Nmr'";
					$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection</span>'); 
					$Donnees = mysqli_fetch_array($ResReq2);
					$quant=$Donnees["quantite"];
					$nature=$Donnees["nature"];
					
					if ($nature=="produit")
						{
						$quantfin=$quant-$Nmqte;
						mysqli_query($db, "UPDATE $db_prod_prest SET quantite='$quantfin' WHERE designation='$Nmn' AND reference='$Nmr'")
						or die('<span class="err_bdd">Erreur lors de la suppression</span>');
						}
					}
				}
			}
		}
		
	mysqli_query($db, "UPDATE $db_facture_entete SET etat='$etat' WHERE num_fact='$ref_fact'")
	or die('<span class="err_bdd">Erreur lors de la suppression</span>');
	
	$Requete3 = "SELECT * FROM $db_membres WHERE nom='$nom_membre'";
	$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Erreur de s&eacute;lection</span>'); 
	$Donnees3 = mysqli_fetch_array($ResReq3);
	$info_facture = $Donnees3["info_facture"];
	$info_devis = $Donnees3["info_devis"];
	$info_commande = $Donnees3["info_commande"];
		
	echo '<p class="cen"><strong>Le statut du document '.$type.' '.$ref_fact.' a &eacute;t&eacute; chang&eacute; en '.$etat.'</strong></p>
	<p class="cen"><a href="liste_supp_fact.php">Revenir &agrave; la liste de suppression des document</a></p>';
		
?>	
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
	
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>