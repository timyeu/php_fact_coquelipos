<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Ajout client</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter un nouveau client</strong>.
				</p>
				
			</div>
			
		<form action="crea_cli_sec.php" method="post" class="edit-form">
			
			<fieldset>
				
				<legend class="lg"> Selection type : </legend>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom"/></p>
				
				<p><label class="gauche" for="nature">Nature :</label>
				<select id="nature" class="droit" name="nature">
					<option value="particulier">particulier</option>
					<option value="professionnel">professionnel</option>
				</select></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Effacer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>