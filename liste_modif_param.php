<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation param&egrave;tres</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier des param&egrave;tres relatifs aux factures, devis et commandes existants</strong>.
				</p>
				
			</div>

<?php

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	$Requete = "SELECT * FROM $db_parametres ORDER by nom_param";

	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>'); 
	
?>

			<form action="modif_param.php" method="post">
			
			<fieldset>
				
				<legend> Liste des param&egrave;tres pr&eacute;sents : </legend>

				<?php
		
				$ld = "<label class='gauche' for='long_liste'>Nom du param&egrave;tre :</label>
				<select class='droit' id='long_liste' name='ListParam'>";
					while ($LigneDo = mysqli_fetch_array($ResReq)) 
						{
						$Nmr = $LigneDo["ref"];
						$Nmnom = $LigneDo["nom_param"];
						$ld .= '<option value="'.$Nmr.'">'.$Nmnom.'</option>';
						}
					$ld .= "</select>";
					
					print $ld;
							
				?>
						
				<p class="cen"><input type="submit" value="Valider"/></p>
			
			</fieldset>
			
			</form>
			
			<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
				
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>