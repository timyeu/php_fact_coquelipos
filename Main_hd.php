<?php

	require 'ccg_coquelipos_fact.php';
	date_default_timezone_set('Europe/Paris');
	
?>

<!DOCTYPE html>

<html lang="fr">

	<head>
		<title>Coquelipos Facturation</title>
		<link rel="stylesheet" type="text/css" title= "design" href="Style/Main.css" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex,nofollow" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/custom.js"></script>
		<style>
			.edit-form{
				width: 70% !important;
				margin: auto;
			}
			#feuille_para{
				height: 120px;
			}
		</style>
	</head>
	
	<body style="margin: 0; padding: 0; background: url(Images/Fond.png) repeat-x;" onLoad="Defaults()">
	
	<div id="conteneur">
		
		<div id="logo">
			<img alt="coquelipos" src="Images/Logo.png"/>
		</div>
		
		<div id="titre">
			<img alt="Coquelipos Facturation" src="Images/Titre.png"/>
		</div>
			
		<ul id="Menu_liste">
			<li><a href="Accueil.php">Accueil</a></li>
			<li><a href="calendrier.php">Agenda</a></li>
			<li class="sub"><a href="#">Stock/Prestations</a>
				<ul>
					<li><a href="crea_prod_sec.php">Ajout</a></li>
					<li><a href="liste_cons_prod.php">Consultation Modification</a></li>
					<li><a href="liste_supp_prod.php">Suppression</a></li>
				</ul>
			</li>
			<li class="sub"><a href="#">Fournisseurs</a>
				<ul>
					<li><a href="crea_four.php">Ajout</a></li>
					<li><a href="liste_modif_four.php">Consultation Modification</a></li>
					<li><a href="liste_supp_four.php">Suppression</a></li>
				</ul>
			</li>
			<li class="sub"><a href="#">Clients</a>
				<ul>
					<li><a href="crea_cli.php">Ajout</a></li>
					<li><a href="liste_modif_cli.php">Consultation Modification</a></li>
					<li><a href="liste_supp_cli.php">Suppression</a></li>
				</ul>
			</li>
			<li class="sub"><a href="#">Documents</a>
				<ul>
					<li><a href="crea_fact_nouv.php">Cr&eacute;ation</a></li>
					<li><a href="liste_modif_fact.php">Consultation Modification</a></li>
					<li><a href="liste_supp_fact.php">Statut</a></li>
				</ul>
			</li>
			<li class="sub"><a href="#">Param&egrave;tres</a>
				<ul>
					<li><a href="cons_compte.php">Detail du compte</a></li>
					<li><a href="gest_comptes.php">Gestion des comptes</a></li>
					<li><a href="gest_param.php">Gestion des param&egrave;tres d'impression</a></li>
				</ul>
			</li>
		</ul>   
		
		<div id="Contenu">