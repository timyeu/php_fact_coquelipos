<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation compte</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier un compte existant</strong>.
				</p>
				
			</div>

<?php

	if (isset($_POST['ListComp'])) $ListComp=$_POST['ListComp'];
		else $ListComp="";
		
	if (isset($_GET['ref'])) $ListComp=$_GET['ref'];
		else $ListComp=$ListComp;
		
	$Requete = "SELECT * FROM $db_membres WHERE ref ='$ListComp'";
	$Requete_cal = "SELECT ref, nom FROM $db_membres";

		if (empty($ListComp))
			{
				echo "Il faut selectionner un compte dans la liste";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
				
				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>'); 
				
				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref=$Donnees["ref"];
				$id=$Donnees["id"];
				$mdp=$Donnees["mdp"];
				$nom=$Donnees["nom"];
				$memo=$Donnees["memo"];
				$info_admin=$Donnees["info_admin"];
				$info_facture=$Donnees["info_facture"];
				$info_devis=$Donnees["info_devis"];
				$info_commande=$Donnees["info_commande"];
				
				$ResReq_cal = mysqli_query($db, $Requete_cal) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>'); 
			}

?>
			
		<form action="req_modif_compte.php" method="post">
		<input type="hidden" name="ref" id="ref" value="<?php echo $ref;?>" />
		<input type="hidden" name="info_admin" id="info_admin" value="<?php echo $info_admin;?>" />
		<input type="hidden" name="info_facture" id="info_facture" value="<?php echo $info_facture;?>" />
		<input type="hidden" name="info_devis" id="info_devis" value="<?php echo $info_devis;?>" />
		<input type="hidden" name="info_commande" id="info_commande" value="<?php echo $info_commande;?>" />
		
			<fieldset>
				
				<legend class="lg"> Detail du compte : </legend>
				
				<p>Nombre de factures cr&eacute;es : <strong><?php echo $info_facture?></p></strong>
				
				<p>Nombre de devis cr&eacute;es : <strong><?php echo $info_devis?></p></strong>
				
				<p>Nombre de commandes cr&eacute;es : <strong><?php echo $info_commande?></p></strong>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom" value="<?php echo $nom;?>"/></p>
				
				<p>
				<label class="gauche" for="nom_cal">Autoriser l'acc&egrave;s &agrave; mon agenda &agrave; un autre membre :</label>
					<select class="droit" id="nom_cal" name="nom_cal">
					<option value=""></option>
<?php
					while ($Donnees_cal = mysqli_fetch_array($ResReq_cal)) 
						{
						$ref_cal = $Donnees_cal["ref"];
						$nom_cal = $Donnees_cal["nom"];
						echo '<option value="'.$ref_cal.'">'.$nom_cal.'</option>';
						}
?>
					</select>
				</p>
				
				<p><label class="gauche" for="memo">M&eacute;mo :</label>
				<textarea class="droit" id="memo" name="memo"><?php echo $memo; ?></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="id">Assigner nouvel identifiant :</label>
				<input class="droit" id="id" type="text" name="id"/></p>
				
				<p><label class="gauche" for="mdp">Assigner nouveau mot de passe :</label>
				<input class="droit" id="mdp" type="text" name="mdp"/></p>
				
<?php

			if ($info_admin=="1")
				{
				echo '<p class="cen"><strong>Droits d\'administration accord&eacute;s</strong></p>
				<p class="cen"><strong>Supprimer droits d\'administration ?&nbsp;&nbsp;&nbsp;<input type="checkbox" name="del_admin" value="2"/></strong></p>';
				}
			else
				{
				echo '<p class="cen"><strong>Accorder droits d\'administration ?&nbsp;&nbsp;&nbsp;<input type="checkbox" name="aut_admin" value="1"/></strong></p>';
				}

?>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Modifier"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>