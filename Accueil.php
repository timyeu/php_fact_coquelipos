<?php
$domain = $_SERVER["SERVER_NAME"];
// echo $domain;
// exit();
?>
<?php

session_start();
date_default_timezone_set('Europe/Paris');

if (isset($_SESSION['connect'])) {
    $connect = $_SESSION['connect'];
} else {
    $connect = 0;
}

if (isset($_SESSION['log'])) {
    $nom_membre = $_SESSION['log'];
} else {
    $nom_membre = 0;
}

include 'ccg_coquelipos_fact.php';
require_once 'Main_hd.php';

if ($connect != "1" && $connect != "2") {
    ?>

	<div id="feuille">

		<div id="feuille_bloc" class="edit-form">

			<div id="feuille_para">

			<h2>Bienvenue sur le logiciel de facturation de Coquelipos</h2>

				<p>
				Vous devez vous <strong>connecter</strong> pour continuer.
				</p>

			</div>

			<form action="znver.php" method="post">

				<fieldset class="form-small">

					<legend> Connexion : </legend>

					<p><label class="gauche" for="ident">Identifiant :</label>
					<input class="droit" id="ident" type="text" name="zni"/></p>

					<p><label class="gauche" for="mot">Mot de passe :</label>
					<input class="droit" id="mot" type="password" name="znm"/></p>

				</fieldset>

				<fieldset>

					<legend> Validation : </legend>

					<p class="cen">
						<input type="submit" value="Valider"/>
						<input type="reset" value="R&eacute;initialiser"/>
					</p>

				</fieldset>

			</form>

		</div>

	</div>

<?php
} else {

    if (isset($_POST['date_deb'])) {
        $date_deb = $_POST['date_deb'];
    } else {
        $date_deb = "";
    }

    if (isset($_GET['date_deb'])) {
        $date_deb = $_GET['date_deb'];
    } else {
        $date_deb = $date_deb;
    }

    if (isset($_POST['date_fin'])) {
        $date_fin = $_POST['date_fin'];
    } else {
        $date_fin = "";
    }

    if (isset($_GET['date_fin'])) {
        $date_fin = $_GET['date_fin'];
    } else {
        $date_fin = $date_fin;
    }

    if (isset($_POST['affdatdeb'])) {
        $affdatdeb = $_POST['affdatdeb'];
    } else {
        $affdatdeb = "";
    }

    if (isset($_POST['affdatfin'])) {
        $affdatfin = $_POST['affdatfin'];
    } else {
        $affdatfin = "";
    }

    if (isset($_POST['num_nouv_cli'])) {
        $num_nouv_cli = $_POST['num_nouv_cli'];
    } else {
        $num_nouv_cli = "3";
    }

    unset($_SESSION['cli_agend']);

    $date_memo  = date('d/m/Y');
    $verif_memo = "";

    if (preg_match('`^[0-9]{2}/[0-9]{2}/[0-9]{4}$`', $date_deb)) {
        $tmp       = explode('/', $date_deb);
        $date_deb  = $tmp[2] . '/' . $tmp[1] . '/' . $tmp[0];
        $affdatdeb = $tmp[0] . '/' . $tmp[1] . '/' . $tmp[2];
    }

    if (preg_match('`^[0-9]{2}/[0-9]{2}/[0-9]{4}$`', $date_fin)) {
        $tmp       = explode('/', $date_fin);
        $date_fin  = $tmp[2] . '/' . $tmp[1] . '/' . $tmp[0];
        $affdatfin = $tmp[0] . '/' . $tmp[1] . '/' . $tmp[2];
    }

    $Requete          = "SELECT * FROM $db_membres WHERE nom ='$nom_membre'";
    $Requete_stock    = "SELECT * FROM $db_prod_prest WHERE qte_limite!=0 AND quantite<=qte_limite";
    $Requete_agend    = "SELECT * FROM $db_agenda WHERE nom_membre='$nom_membre' AND STR_TO_DATE(date_complete, '%d/%m/%Y')>= '$date_deb' AND STR_TO_DATE(date_complete, '%d/%m/%Y')<= '$date_fin' ORDER BY STR_TO_DATE(date_complete, '%d/%m/%Y') DESC";
    $Requete_nouv_cli = "SELECT * FROM $db_compte_client ORDER BY ref_clients ASC LIMIT $num_nouv_cli";
    $Requete_memo     = "SELECT * FROM $db_agenda WHERE nom_membre ='$nom_membre' AND date_complete='$date_memo' ORDER BY STR_TO_DATE(date_complete, '%d/%m/%Y') DESC";
    $Requete_fact     = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='facture' AND etat='en cours' ORDER by num_fact";
    $Requete_dev      = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='devis' AND etat='en cours' ORDER by num_fact";
    $Requete_comm     = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='commande' AND etat='en cours' ORDER by num_fact";
    $Requete_doc_val  = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND STR_TO_DATE(date_valid_fin, '%d/%m/%Y')<= STR_TO_DATE('$date_memo', '%d/%m/%Y') ORDER by num_fact";
    if (isset($_GET['search']) && $_GET['search'] != '') {
        $Requete_doc_val = "SELECT * FROM $db_facture_entete RIGHT JOIN $db_compte_client ON {$db_facture_entete}.nom_client = {$db_compte_client}.ref_clients WHERE {$db_compte_client}.nom LIKE '%{$_GET['search']}%' AND {$db_facture_entete}.nom_membre = '$nom_membre' AND STR_TO_DATE(date_valid_fin, '%d/%m/%Y')<= STR_TO_DATE('$date_memo', '%d/%m/%Y') ORDER by {$db_facture_entete}.num_fact";
    }

    $db = mysqli_connect($db_server, $db_user, $db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
    mysqli_select_db($db, $db_database) or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

    $ResReq       = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>');
    $ResReq_stock = mysqli_query($db, $Requete_stock) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, table incorrecte ou inexistante</span>');
    $ResReq_fact  = mysqli_query($db, $Requete_fact) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');
    $ResReq_dev   = mysqli_query($db, $Requete_dev) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');
    $ResReq_comm  = mysqli_query($db, $Requete_comm) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');

    $Donnees = mysqli_fetch_array($ResReq);

    $ref           = $Donnees["ref"];
    $id            = $Donnees["id"];
    $mdp           = $Donnees["mdp"];
    $nom           = $Donnees["nom"];
    $memo          = $Donnees["memo"];
    $info_admin    = $Donnees["info_admin"];
    $info_facture  = $Donnees["info_facture"];
    $info_devis    = $Donnees["info_devis"];
    $info_commande = $Donnees["info_commande"];

    $ResReq_agend                                 = mysqli_query($db, $Requete_agend) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
    $nbenreg_agend                                = mysqli_num_rows($ResReq_agend);
    $nbchamps_agend                               = mysqli_num_fields($ResReq_agend);
    $Tabdo_agend[$nbenreg_agend][$nbchamps_agend] = "";
    $Tabchamps_agend[$nbchamps_agend]             = "";

    for ($I = 0; $I < $nbchamps_agend; $I++) {
        $tabchamps_agend[$I] = mysqli_fetch_field_direct($ResReq_agend, $I);
    }

    $I = 0;

    while ($donnees_agend = mysqli_fetch_array($ResReq_agend)) {
        $Tabdo_agend[$I][1]  = $donnees_agend["ref"];
        $Tabdo_agend[$I][2]  = $donnees_agend["nom_membre"];
        $Tabdo_agend[$I][3]  = $donnees_agend["date_complete"];
        $Tabdo_agend[$I][4]  = $donnees_agend["intit_action"];
        $Tabdo_agend[$I][5]  = $donnees_agend["horaire_deb"];
        $Tabdo_agend[$I][6]  = $donnees_agend["horaire_fin"];
        $Tabdo_agend[$I][7]  = $donnees_agend["action"];
        $Tabdo_agend[$I][8]  = $donnees_agend["ListCli"];
        $Tabdo_agend[$I][9]  = $donnees_agend["ListFour"];
        $Tabdo_agend[$I][10] = $donnees_agend["ListProd"];
        $Tabdo_agend[$I][11] = $donnees_agend["ListPrest"];
        $Tabdo_agend[$I][12] = $donnees_agend["detail_agend"];
        $I++;
    }

    $ResReq_nouv_cli                                       = mysqli_query($db, $Requete_nouv_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
    $nbenreg_nouv_cli                                      = mysqli_num_rows($ResReq_nouv_cli);
    $nbchamps_nouv_cli                                     = mysqli_num_fields($ResReq_nouv_cli);
    $Tabdo_nouv_cli[$nbenreg_nouv_cli][$nbchamps_nouv_cli] = "";
    $Tabchamps_nouv_cli[$nbchamps_nouv_cli]                = "";

    for ($J = 0; $J < $nbchamps_nouv_cli; $J++) {
        $tabchamps_nouv_cli[$J] = mysqli_fetch_field_direct($ResReq_nouv_cli, $J);
    }

    $J = 0;

    while ($donnees_nouv_cli = mysqli_fetch_array($ResReq_nouv_cli)) {
        $Tabdo_nouv_cli[$J][1] = $donnees_nouv_cli["nom"];
        $Tabdo_nouv_cli[$J][2] = $donnees_nouv_cli["nature"];
        $Tabdo_nouv_cli[$J][3] = $donnees_nouv_cli["adresse"];
        $Tabdo_nouv_cli[$J][4] = $donnees_nouv_cli["ville"];
        $Tabdo_nouv_cli[$J][5] = $donnees_nouv_cli["code_postal"];
        $Tabdo_nouv_cli[$J][6] = $donnees_nouv_cli["tel"];
        $Tabdo_nouv_cli[$J][7] = $donnees_nouv_cli["tel_portable"];
        $Tabdo_nouv_cli[$J][8] = $donnees_nouv_cli["mail"];
        $Tabdo_nouv_cli[$J][9] = $donnees_nouv_cli["activite"];
        $J++;
    }

    $ResReq_memo                               = mysqli_query($db, $Requete_memo) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
    $nbenreg_memo                              = mysqli_num_rows($ResReq_memo);
    $nbchamps_memo                             = mysqli_num_fields($ResReq_memo);
    $Tabdo_memo[$nbenreg_memo][$nbchamps_memo] = "";
    $Tabchamps_memo[$nbchamps_memo]            = "";

    for ($K = 0; $K < $nbchamps_memo; $K++) {
        $tabchamps_memo[$K] = mysqli_fetch_field_direct($ResReq_memo, $K);
    }

    $K = 0;

    while ($donnees_memo = mysqli_fetch_array($ResReq_memo)) {
        $Tabdo_memo[$K][1]  = $donnees_memo["ref"];
        $verif_memo         = $Tabdo_memo[$K][1];
        $Tabdo_memo[$K][2]  = $donnees_memo["nom_membre"];
        $Tabdo_memo[$K][3]  = $donnees_memo["date_complete"];
        $Tabdo_memo[$K][4]  = $donnees_memo["intit_action"];
        $Tabdo_memo[$K][5]  = $donnees_memo["horaire_deb"];
        $Tabdo_memo[$K][6]  = $donnees_memo["horaire_fin"];
        $Tabdo_memo[$K][7]  = $donnees_memo["action"];
        $Tabdo_memo[$K][8]  = $donnees_memo["ListCli"];
        $Tabdo_memo[$K][9]  = $donnees_memo["ListFour"];
        $Tabdo_memo[$K][10] = $donnees_memo["ListProd"];
        $Tabdo_memo[$K][11] = $donnees_memo["ListPrest"];
        $Tabdo_memo[$K][12] = $donnees_memo["detail_agend"];
        $K++;
    }

    $ResReq_doc_val                                     = mysqli_query($db, $Requete_doc_val) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');
    $nbenreg_doc_val                                    = mysqli_num_rows($ResReq_doc_val);
    $nbchamps_doc_val                                   = mysqli_num_fields($ResReq_doc_val);
    $Tabdo_doc_val[$nbenreg_doc_val][$nbchamps_doc_val] = "";
    $Tabchamps_doc_val[$nbchamps_doc_val]               = "";

    for ($L = 0; $L < $nbchamps_doc_val; $L++) {
        $tabchamps_doc_val[$L] = mysqli_fetch_field_direct($ResReq_doc_val, $L);
    }

    $L = 0;

    while ($donnees_doc_val = mysqli_fetch_array($ResReq_doc_val)) {
        $Tabdo_doc_val[$L][1] = $donnees_doc_val["num_fact"];
        $Tabdo_doc_val[$L][2] = $donnees_doc_val["date_fact"];
        $Tabdo_doc_val[$L][3] = $donnees_doc_val["date_valid_deb"];
        $Tabdo_doc_val[$L][4] = $donnees_doc_val["date_valid_fin"];
        $Tabdo_doc_val[$L][5] = $donnees_doc_val["nom_client"];
        $Tabdo_doc_val[$L][6] = $donnees_doc_val["type"];
        $Tabdo_doc_val[$L][7] = $donnees_doc_val["etat"];
        $L++;
    }

    ?>

	<div id="feuille">

		<div id="feuille_bloc">

			<div id="feuille_para">

			<h2>Bienvenue sur le logiciel de facturation de Coquelipos</h2>

				<p>
				Bienvenue <strong><?php echo $nom_membre ?></strong>.
				</p>

<?php
if ($connect == "1") {
        echo '<p>Vous &ecirc;tes actuellement en mode <strong>Administration</strong>.</p>';
    }
    ?>

			</div>

<?php
if ($memo != "") {
        ?>

		<fieldset>

			<legend> M&eacute;mo : </legend>

			<form action="req_cons_compte.php" method="post">

				<input type="hidden" name="ref" id="ref" value="<?php echo $ref; ?>" />
				<input type="hidden" name="eff_accueil" id="eff_accueil" value="O" />

				<p><label class="gauche" for="memo">M&eacute;mo :</label>
				<textarea class="droit_memo" id="memo" name="memo"><?php echo $memo; ?></textarea></p>

				<br /><br /><br /><br />

				<p class="cen">
					<input type="submit" value="Modifier"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>

			</form>

			<form action="imprime_memo.php" target="_blank" method="post">

				<input type="hidden" name="memo" id="memo" value="<?php echo $memo; ?>" />
				<p class="cen">
					<input type="submit" value="Imprimer"/>
				</p>

			</form>

		</fieldset>

<?php
}
    if ($verif_memo != "") {
        ?>

		<fieldset class="form-small">

			<legend> Ordre du jour : </legend>

			<table>

				<tr>

					<th>Date</th>
					<th>Intitul&eacute;</th>
					<th>Horaire</th>
					<th>Concernant</th>
					<th>D&eacute;tail</th>

				</tr>

			<?php
for ($K = 0; $K < $nbenreg_memo; $K++) {
            echo '<tr>
						<td>
							<p><strong>' . $Tabdo_memo[$K][3] . '</strong></p>
							<p>
								<form action="req_supp_agenda.php" method="post">
								<input type="hidden" name="ref" value="' . $Tabdo_memo[$K][1] . '"/>
								<input type="hidden" name="date_deb_s" value="' . $affdatdeb . '"/>
								<input type="hidden" name="date_fin_s" value="' . $affdatfin . '"/>
								<input type="submit" value="Sup."/></form>
							</p></td>';
            echo '<td>' . $Tabdo_memo[$K][4] . '</td>';
            if ($Tabdo_memo[$K][5] != "") {
                if ($Tabdo_memo[$K][6] != "") {
                    echo '<td><p>De : <strong>' . $Tabdo_memo[$K][5] . '</strong></p>
						<p>&Agrave; : <strong>' . $Tabdo_memo[$K][6] . '</strong></p></td>';
                } else {
                    echo '<td><p>Heure : <strong>' . $Tabdo_memo[$K][5] . '</strong></p></td>';
                }
            } else {
                echo '<td></td>';
            }
            echo '<td><p>' . $Tabdo_memo[$K][7] . '</p>';
            if ($Tabdo_memo[$K][8] != "") {
                $Requete_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='" . $Tabdo_memo[$K][8] . "'";
                $ResReq_cli  = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
                $Donnees_cli = mysqli_fetch_array($ResReq_cli);
                $nom_cli     = $Donnees_cli["nom"];
                echo '<p>Client : <strong>' . $nom_cli . '</strong></p></td>';
            }
            if ($Tabdo_memo[$K][9] != "") {
                $Requete_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='" . $Tabdo_memo[$K][9] . "'";
                $ResReq_four  = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>');
                $Donnees_four = mysqli_fetch_array($ResReq_four);
                $nom_four     = $Donnees_four["nom"];
                echo '<p>Fournisseur : <strong>' . $nom_four . '</strong></p></td>';
            }
            if ($Tabdo_memo[$K][10] != "") {
                $Requete_prod = "SELECT designation FROM $db_prod_prest WHERE nature='produit' AND ref_produits='" . $Tabdo_memo[$K][10] . "'";
                $ResReq_prod  = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>');
                $Donnees_prod = mysqli_fetch_array($ResReq_prod);
                $nom_prod     = $Donnees_prod["designation"];
                echo '<p>Produit : <strong>' . $nom_prod . '</strong></p></td>';
            }
            if ($Tabdo_memo[$K][11] != "") {
                $Requete_prest = "SELECT designation FROM $db_prod_prest WHERE nature='prestation' AND ref_produits='" . $Tabdo_memo[$K][11] . "'";
                $ResReq_prest  = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>');
                $Donnees_prest = mysqli_fetch_array($ResReq_prest);
                $nom_prest     = $Donnees_prest["designation"];
                echo '<p>Prestation : <strong>' . $nom_prest . '</strong></p></td>';
            }
            echo '<td style="width:50%;">' . $Tabdo_memo[$K][12] . '</td>
				</tr>';
        }
        ?>

			</table>

		</fieldset>

		<?php
}
    ?>

		<fieldset>

			<legend> Votre activit&eacute; : </legend>

			<form action="modif_fact.php" method="post">

<?php

    $ld = '<p><label class="gauche" for="liste_fact">Factures en cours : </label>
				<select class="droit" id="liste_fact" name="liste_fact">
				<option value=""></option>';
    while ($LigneDo_fact = mysqli_fetch_array($ResReq_fact)) {
        $Nmnum_fact = $LigneDo_fact["num_fact"];

        $ld .= '<option value="' . $Nmnum_fact . '">' . $Nmnum_fact . '</option>';
    }
    $ld .= '</select></p>';

    print $ld;

    $ld2 = '<p><label class="gauche" for="liste_dev">Devis en cours : </label>
				<select class="droit" id="liste_dev" name="liste_dev">
				<option value=""></option>';
    while ($LigneDo_dev = mysqli_fetch_array($ResReq_dev)) {
        $Nmnum_dev = $LigneDo_dev["num_fact"];

        $ld2 .= '<option value="' . $Nmnum_dev . '">' . $Nmnum_dev . '</option>';
    }
    $ld2 .= '</select></p>';

    print $ld2;

    $ld3 = '<p><label class="gauche" for="liste_comm">Commandes en cours : </label>
				<select class="droit" id="liste_comm" name="liste_comm">
				<option value=""></option>';
    while ($LigneDo_comm = mysqli_fetch_array($ResReq_comm)) {
        $Nmnum_comm = $LigneDo_comm["num_fact"];

        $ld3 .= '<option value="' . $Nmnum_comm . '">' . $Nmnum_comm . '</option>';
    }
    $ld3 .= '</select></p>';

    print $ld3;
    ?>

				<p class="cen">
					<input type="submit" value="Consulter"/>
				</p>

			</form>

		</fieldset>

		<fieldset>

			<legend> Agenda : </legend>

			<form action="Accueil.php" method="post">

			<p class="cen"><strong>Recherche date</strong></p>

			<p><label class="gauche" for="date_deb">Date de d&eacute;but :</label>
			<input class="droit" id="date_deb" type="text" name="date_deb" value="<?php echo $affdatdeb; ?>"/></p>

			<p><label class="gauche" for="date_fin">Date de fin :</label>
			<input class="droit" id="date_fin" type="text" name="date_fin" value="<?php echo $affdatfin; ?>"/></p>

			<p class="cen">
				<input type="submit" value="Recherche"/>
			</p>

			</form>

<?php
if ($affdatdeb != "" || $affdatfin != "") {
        ?>
			<p class="cen"><strong>Agenda du <?php echo $affdatdeb; ?> au <?php echo $affdatfin; ?></strong></p>

			<table>

				<tr>

					<th>Date</th>
					<th>Intitul&eacute;</th>
					<th>Horaire</th>
					<th>Concernant</th>
					<th>D&eacute;tail</th>

				</tr>

<?php
for ($I = 0; $I < $nbenreg_agend; $I++) {
            echo '<tr>
						<td>
							<p><strong>' . $Tabdo_agend[$I][3] . '</strong></p>
							<p>
								<form action="req_supp_agenda.php" method="post">
								<input type="hidden" name="ref" value="' . $Tabdo_agend[$I][1] . '"/>
								<input type="hidden" name="date_deb_s" value="' . $affdatdeb . '"/>
								<input type="hidden" name="date_fin_s" value="' . $affdatfin . '"/>
								<input type="submit" value="Sup."/></form>
							</p></td>';
            echo '<td>' . $Tabdo_agend[$I][4] . '</td>';
            if ($Tabdo_agend[$I][5] != "") {
                if ($Tabdo_agend[$I][6] != "") {
                    echo '<td><p>De <strong>' . $Tabdo_agend[$I][5] . '</strong></p>
						<p>Heure fin : <strong>' . $Tabdo_agend[$I][6] . '</strong></p></td>';
                } else {
                    echo '<td><p>&Agrave; : <strong>' . $Tabdo_agend[$I][5] . '</strong></p></td>';
                }
            } else {
                echo '<td></td>';
            }
            echo '<td><p>' . $Tabdo_agend[$I][7] . '</p>';
            if ($Tabdo_agend[$I][8] != "") {
                $Requete_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='" . $Tabdo_agend[$I][8] . "'";
                $ResReq_cli  = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
                $Donnees_cli = mysqli_fetch_array($ResReq_cli);
                $nom_cli     = $Donnees_cli["nom"];
                echo '<p>Client : <strong>' . $nom_cli . '</strong></p></td>';
            }
            if ($Tabdo_agend[$I][9] != "") {
                $Requete_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='" . $Tabdo_agend[$I][9] . "'";
                $ResReq_four  = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>');
                $Donnees_four = mysqli_fetch_array($ResReq_four);
                $nom_four     = $Donnees_four["nom"];
                echo '<p>Fournisseur : <strong>' . $nom_four . '</strong></p></td>';
            }
            if ($Tabdo_agend[$I][10] != "") {
                $Requete_prod = "SELECT designation FROM $db_prod_prest WHERE nature='produit' AND ref_produits='" . $Tabdo_agend[$I][10] . "'";
                $ResReq_prod  = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>');
                $Donnees_prod = mysqli_fetch_array($ResReq_prod);
                $nom_prod     = $Donnees_prod["designation"];
                echo '<p>Produit : <strong>' . $nom_prod . '</strong></p></td>';
            }
            if ($Tabdo_agend[$I][11] != "") {
                $Requete_prest = "SELECT designation FROM $db_prod_prest WHERE nature='prestation' AND ref_produits='" . $Tabdo_agend[$I][11] . "'";
                $ResReq_prest  = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>');
                $Donnees_prest = mysqli_fetch_array($ResReq_prest);
                $nom_prest     = $Donnees_prest["designation"];
                echo '<p>Prestation : <strong>' . $nom_prest . '</strong></p></td>';
            }
            echo '<td style="width:50%;">' . $Tabdo_agend[$I][12] . '</td>
				</tr>';
        }
        ?>

			</table>

<?php
}
    if ($nbenreg_doc_val != 0) {
        ?>
		</fieldset>

		<fieldset>

		<legend class="lg"> Ech&eacute;ance des documents : </legend>

		<p class="cen"><strong>Appara&icirc;ssent dans ce cadre les documents ayant d&eacute;pass&eacute; leur date d'&eacute;ch&eacute;ance</strong></p>
            <div class="all">
                <input type="radio" name="all" value="all"><p>All</p>
                <input type="radio" name="rien" value="hidden-all" checked="checked"><p>Rien</p>
                <form action="">
                    <label for="search">Search:</label>
                    <input type="text" name="search">
                </form>
            </div>
            <div class="filter-radio">
            <p>Type:</p>
                <input type="radio" name="type" value="commande"><p>Commande</p>
                <input type="radio" name="type" value="devis"><p>Devis</p>
                <input type="radio" name="type" value="facture"><p>Facture</p>
                <input type="radio" name="type" value="fact_av"><p>Avoir</p>
                <input type="radio" name="type" value="noselecttype"><p>All</p>
            </div>
            <div class="filter-radio">
            <p>Etat:</p>
                <input type="radio" name="etat" value="en cours"><p>En cours</p>
                <input type="radio" name="etat" value="annulation"><p>Annulation</p>
                <input type="radio" name="etat" value="erreur saisie"><p>Erreur saisie</p>
                <input type="radio" name="etat" value="noselectetat"><p>All</p>
            </div>
            <style>
            .filter-radio,.all{text-align: center;margin-bottom: 20px;}
                .filter-radio p, .all p{display: inline;margin: 0px 10px;}
            }
            </style>
			<table>

<?php
echo '<tr>
				<th>Num&eacute;ro</th>
				<th>Type</th>
				<th>Date cr&eacute;ation</th>
				<th>Validit&eacute;</th>
				<th>Client</th>
				<th>Etat</th>
				<th>Modifier</th>
			</tr>';

        for ($L = 0; $L < $nbenreg_doc_val; $L++) {
            echo '<form action="supp_fact.php" method="post">
				<input type="hidden" name="ListFact" value="' . $Tabdo_doc_val[$L][1] . '" />
				<tr>
				<td>' . $Tabdo_doc_val[$L][1] . '</td>
				<td class="type" name="' . $Tabdo_doc_val[$L][6] . '">' . $Tabdo_doc_val[$L][6] . '</td>
				<td>' . $Tabdo_doc_val[$L][2] . '</td>
				<td>';
            if ($Tabdo_doc_val[$L][3] != "" && $Tabdo_doc_val[$L][4] != "") {
                echo 'Du ' . $Tabdo_doc_val[$L][3] . ' au ' . $Tabdo_doc_val[$L][4];
            }
            echo '</td>';
            if ($Tabdo_doc_val[$L][5] != "") {
                $Requete_cli_ech = "SELECT nom FROM $db_compte_client WHERE ref_clients='" . $Tabdo_doc_val[$L][5] . "'";
                $ResReq_cli_ech  = mysqli_query($db, $Requete_cli_ech) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
                $Donnees_cli_ech = mysqli_fetch_array($ResReq_cli_ech);
                $nom_cli_ech     = $Donnees_cli_ech["nom"];
                echo '<td>' . $nom_cli_ech . '</td>';
            } else {
                echo '<td></td>';
            }
            echo '<td class="etat" name="' . $Tabdo_doc_val[$L][7] . '">' . $Tabdo_doc_val[$L][7] . '</td>
				<td><a href="/modif_fact.php?num_fact=' . $Tabdo_doc_val[$L][1] . '">Modifier</a></td>
				<td><input type="submit" name="supp_fact" value="St."/></td>
				</tr>
				</form>';
        }
        ?>

			</table>

		</fieldset>

<?php
}
    ?>

		<fieldset>

			<legend> Derniers clients ajout&eacute;s : </legend>

			<form action="Accueil.php" method="post">

			<p class="cen"><strong>Affichage client</strong></p>

			<p><label class="gauche" for="num_nouv_cli">Entrez le nombre des derniers enregistrements &agrave; afficher</label>
			<input class="droit" id="num_nouv_cli" type="text" name="num_nouv_cli" value="<?php echo $num_nouv_cli; ?>"/></p>

<?php
if ($num_nouv_cli != "0") {
        ?>

			<table>

				<tr>

					<th>Nom</th>
					<th>Adresse</th>
					<th>T&eacute;lephone</th>
					<th>Mail</th>
					<th>Activit&eacute;</th>

				</tr>

<?php
for ($J = 0; $J < $nbenreg_nouv_cli; $J++) {
            echo '<tr>
				<td><p>' . $Tabdo_nouv_cli[$J][1] . '</p><p><strong>' . $Tabdo_nouv_cli[$J][2] . '</strong></p></td>
				<td><p>' . $Tabdo_nouv_cli[$J][3] . '</p><p>' . $Tabdo_nouv_cli[$J][5] . ' ' . $Tabdo_nouv_cli[$J][4] . '</p></td>
				<td><p>' . $Tabdo_nouv_cli[$J][6] . '</p><p><strong>' . $Tabdo_nouv_cli[$J][7] . '</strong></p></td>
				<td><p>' . $Tabdo_nouv_cli[$J][8] . '</p></td>
				<td><p>' . $Tabdo_nouv_cli[$J][9] . '</p></td>
				</tr>';
        }

        echo '</table>';

    }
    ?>
			<p class="cen">
				<input type="submit" value="Afficher"/>
			</p>

			</form>

		</fieldset>

		<fieldset>

			<legend> Etat des stocks : </legend>

			<p class="cen"><a href="imprime_stock_r.php" target="_blank">Consulter et/ou imprimer stock &agrave; renouveler</a></p>

			<p>
				<ul>
				<?php
while ($LigneDo_stock = mysqli_fetch_array($ResReq_stock)) {
        $designation_stock = $LigneDo_stock["designation"];
        $reference_stock   = $LigneDo_stock["reference"];
        $quantite_stock    = $LigneDo_stock["quantite"];
        echo '<li><p>D&eacute;signation et r&eacute;f&eacute;rence : <strong>' . $designation_stock . ' - ' . $reference_stock . '</strong>, Quantit&eacute; restante : <strong>' . $quantite_stock . '</strong></p></li>';
    }
    ?>
				</ul>
			</p>

		</fieldset>

		<fieldset>

			<legend> Fermer session : </legend>

			<form action="dcn_coquelipos_fact.php" method="post">
				<p class="cen">
					<input type="submit" value="D&eacute;connexion" />
				</p>
			</form>

		</fieldset>

		</div>

	</div>

<?php
}
require_once 'Main_ft.php';
?>