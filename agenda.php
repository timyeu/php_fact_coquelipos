<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Agenda compte <?php echo $nom_membre;?></h2>
			
				<p>
				Ce formulaire vous permet de <strong>g&eacute;rer votre agenda</strong>.
				</p>
				
			</div>
			
<?php

	if(isset($_POST['date_deb_c']))      $date_deb_c=$_POST['date_deb_c'];
	else      $date_deb_c="";
		
	if(isset($_POST['date_fin_c']))      $date_fin_c=$_POST['date_fin_c'];
	else      $date_fin_c="";
	
	if (isset($_POST['date_agend'])) $date_agend=$_POST['date_agend'];
		else $date_agend="";
		
	if (isset($_POST['mois_agend'])) $mois_agend=$_POST['mois_agend'];
		else $mois_agend="";
		
	if (isset($_POST['annee_agend'])) $annee_agend=$_POST['annee_agend'];
		else $annee_agend="";
		
	if (isset($_POST['cli_agend'])) $cli_agend=$_POST['cli_agend'];
		else $cli_agend="";
		
	$date_complete=$date_agend.'/'.$mois_agend.'/'.$annee_agend;
	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	if ($cli_agend=="")
		{
		$Requete_cli = "SELECT * FROM $db_compte_client ORDER by nom";
		}
	else
		{
		$Requete_cli = "SELECT * FROM $db_compte_client WHERE ref_clients='$cli_agend'";
		}
	$Requete_four = "SELECT * FROM $db_fournisseurs ORDER by nom";
	$Requete_prod = "SELECT * FROM $db_prod_prest WHERE nature='produit' ORDER by designation";
	$Requete_prest = "SELECT * FROM $db_prod_prest WHERE nature='prestation' ORDER by designation";
	$Requete_agend = "SELECT * FROM $db_agenda WHERE nom_membre='$nom_membre' AND date_complete='$date_complete'";

	$ResReq_cli = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
	$ResReq_four = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
	$ResReq_prod = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
	$ResReq_prest = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
	
	$ResReq_agend = mysqli_query($db, $Requete_agend) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
	$nbenreg_agend = mysqli_num_rows($ResReq_agend);
	$nbchamps_agend = mysqli_num_fields($ResReq_agend);
	$Tabdo_agend[$nbenreg_agend][$nbchamps_agend]="";
	$Tabchamps_agend[$nbchamps_agend]="";
	
			for($I=0; $I < $nbchamps_agend; $I++) 
				{
				$tabchamps_agend[$I] = mysqli_fetch_field_direct($ResReq_agend,$I);
				}
		
			$I=0;
			
			while ($donnees_agend = mysqli_fetch_array($ResReq_agend))
				{	
				$Tabdo_agend[$I][1]=$donnees_agend["ref"];
				$Tabdo_agend[$I][2]=$donnees_agend["nom_membre"];
				$Tabdo_agend[$I][3]=$donnees_agend["date_complete"];
				$Tabdo_agend[$I][4]=$donnees_agend["intit_action"];
				$Tabdo_agend[$I][5]=$donnees_agend["action"];
				$Tabdo_agend[$I][6]=$donnees_agend["ListCli"];
				$Tabdo_agend[$I][7]=$donnees_agend["ListFour"];
				$Tabdo_agend[$I][8]=$donnees_agend["ListProd"];
				$Tabdo_agend[$I][9]=$donnees_agend["ListPrest"];
				$Tabdo_agend[$I][10]=$donnees_agend["detail_agend"];
				$Tabdo_agend[$I][11]=$donnees_agend["couleur"];
				$couleur=$Tabdo_agend[$I][11];
				$I++;
				}
	
?>
		<form action="req_crea_agenda.php" method="post">
		<input type="hidden" name="date_complete" id="date_complete" value="<?php echo $date_complete;?>" />
		<input type="hidden" name="date_deb_c" value="<?php echo $date_deb_c;?>" />
		<input type="hidden" name="date_fin_c" value="<?php echo $date_fin_c;?>" />
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif agenda du <?php echo $date_complete;?> : </legend>
				
				<p><label class="gauche" for="intit_action">Intitul&eacute; de l'action :</label>
				<input class="droit" id="intit_action" type="text" name="intit_action"/></p>
				
				<p><label class="gauche" for="horaire_deb">Heure de d&eacute;but :</label>
				<input class="droit" id="horaire_deb" type="text" name="horaire_deb"/></p>
				
				<p><label class="gauche" for="horaire_fin">Heure de fin :</label>
				<input class="droit" id="horaire_fin" type="text" name="horaire_fin"/></p>
				
				<p>
				
				<label class="gauche" for="action">Type d'action :</label>
				<select class="droit" id="action" name="action">
					<option value=""></option>
					<option value="Rendez-vous">Rendez-vous</option>
					<option value="Entretien t&eacute;l&eacute;phonique">Entretien t&eacute;l&eacute;phonique</option>
					<option value="Relance">Relance</option>
					<option value="Installation">Installation</option>
					<option value="D&eacute;pannage">D&eacute;pannage</option>
					<option value="D&eacute;jeuner">D&eacute;jeuner</option>
					<option value="D&eacute;placement">D&eacute;placement</option>
					<option value="Perso">Perso</option>
					<option value="Cong&eacute;">Cong&eacute;</option>
				</select>
				
				</p>
				
				<p>
					
<?php				
				if ($cli_agend=="")
					{
					echo '<label class="gauche" for="ListCli">Nom du client :</label>
					<select class="droit" id="ListCli" name="ListCli">
					<option value=""></option>';

						while ($LigneDo_cli = mysqli_fetch_array($ResReq_cli)) 
							{
							$Nmr_clients = $LigneDo_cli["ref_clients"];
							$Nmnom_clients = $LigneDo_cli["nom"];
							echo '<option value="'.$Nmr_clients.'">'.$Nmnom_clients.'</option>';
							}
							
					echo '</select>';
					}
				else
					{
					$Donnees_cli = mysqli_fetch_array($ResReq_cli);
		
					$Nmr_clients=$Donnees_cli["ref_clients"];
					$Nmnom_clients=$Donnees_cli["nom"];
					echo '<input type="hidden" name="ListCli" value="'.$Nmr_clients.'"/>
					<input type="hidden" name="cli_agend" value="'.$cli_agend.'"/>
					Nom du client : <strong>'.$Nmnom_clients.'</strong>';
					}
					
				echo '</p>';
?>				

				<p>
						
				<label class="gauche" for="ListFour">Nom du fournisseur :</label>
				<select class="droit" id="ListFour" name="ListFour">
				<option value=""></option>
<?php
					while ($LigneDo_four = mysqli_fetch_array($ResReq_four)) 
						{
						$Nmr_fournisseur = $LigneDo_four["ref_fournisseur"];
						$Nmnom_fournisseur = $LigneDo_four["nom"];
						echo '<option value="'.$Nmr_fournisseur.'">'.$Nmnom_fournisseur.'</option>';
						}
?>
				</select>

				</p>
				
				<p>
						
				<label class="gauche" for="ListProd">Nom du produit :</label>
				<select class="droit" id="ListProd" name="ListProd">
				<option value=""></option>
<?php
					while ($LigneDo_prod = mysqli_fetch_array($ResReq_prod)) 
						{
						$Nmr_produits = $LigneDo_prod["ref_produits"];
						$Nmnom_produits = $LigneDo_prod["designation"];
						echo '<option value="'.$Nmr_produits.'">'.$Nmnom_produits.'</option>';
						}
?>
				</select>

				</p>
				
				<p>
						
				<label class="gauche" for="ListPrest">Nom de la prestation :</label>
				<select class="droit" id="ListPrest" name="ListPrest">
				<option value=""></option>
<?php
					while ($LigneDo_prest = mysqli_fetch_array($ResReq_prest)) 
						{
						$Nmr_prestations = $LigneDo_prest["ref_produits"];
						$Nmnom_prestations = $LigneDo_prest["designation"];
						echo '<option value="'.$Nmr_prestations.'">'.$Nmnom_prestations.'</option>';
						}
?>
				</select>

				</p>
				
				<p>
				
				<label class="gauche" for="couleur">Couleur :</label>
				<select class="droit" id="couleur" name="couleur">
					<option value="<?php echo $couleur;?>"><?php echo $couleur;?></option>
					<option value="cald">Aucune</option>
					<option value="cald1">Rouge</option>
					<option value="cald2">Orange</option>
					<option value="cald3">Jaune</option>
					<option value="cald4">Vert</option>
					<option value="cald5">Menthe</option>
					<option value="cald6">Cyan</option>
					<option value="cald7">Cobalt</option>
					<option value="cald8">Bleu</option>
					<option value="cald9">Violet</option>
					<option value="cald10">Magenta</option>
					<option value="cald11">Fuschia</option>
				</select>
				
				</p>
				
				<p class="cen">D&eacute;tail de l'action</p>
				
				<p class="cen"><textarea name="detail_agend"></textarea></p>
		
			</fieldset>
		
			<fieldset>
			
			<legend> Entr&eacute;es existantes : </legend>
			
			<table>
				
				<tr>
			
					<th>Date</th>
					<th>Intitul&eacute;</th>
					<th>Concernant</th>
					<th>D&eacute;tail</th>
						
				</tr>

<?php
			for($I=0; $I < $nbenreg_agend; $I++) 
				{
				echo '<tr>
						<td>
							<p><strong>'.$Tabdo_agend[$I][3].'</strong></p>
							<p>
								<form action="req_supp_agenda.php" method="post">
								<input type="hidden" name="ref" value="'.$Tabdo_agend[$I][1].'"/>
								<input type="submit" value="Sup."/></form>
							</p></td>';
				echo '<td>'.$Tabdo_agend[$I][4].'</td>';
				echo '<td><p>'.$Tabdo_agend[$I][5].'</p>';
				if ($Tabdo_agend[$I][6]!="")
					{
					$Requete_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='".$Tabdo_agend[$I][6]."'";
					$ResReq_cli = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
					$Donnees_cli = mysqli_fetch_array($ResReq_cli);
					$nom_cli=$Donnees_cli["nom"];
					echo '<p>Client : <strong>'.$nom_cli.'</strong></p></td>';
					}
				if ($Tabdo_agend[$I][7]!="")
					{
					$Requete_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='".$Tabdo_agend[$I][7]."'";
					$ResReq_four = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
					$Donnees_four = mysqli_fetch_array($ResReq_four);
					$nom_four=$Donnees_four["nom"];
					echo '<p>Fournisseur : <strong>'.$nom_four.'</strong></p></td>';
					}
				if ($Tabdo_agend[$I][8]!="")
					{
					$Requete_prod = "SELECT designation FROM $db_prod_prest WHERE nature='produit' AND ref_produits='".$Tabdo_agend[$I][8]."'";
					$ResReq_prod = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
					$Donnees_prod = mysqli_fetch_array($ResReq_prod);
					$nom_prod=$Donnees_prod["designation"];
					echo '<p>Produit : <strong>'.$nom_prod.'</strong></p></td>';
					}
				if ($Tabdo_agend[$I][9]!="")
					{
					$Requete_prest = "SELECT designation FROM $db_prod_prest WHERE nature='prestation' AND ref_produits='".$Tabdo_agend[$I][9]."'";
					$ResReq_prest = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
					$Donnees_prest = mysqli_fetch_array($ResReq_prest);
					$nom_prest=$Donnees_prest["designation"];
					echo '<p>Prestation : <strong>'.$nom_prest.'</strong></p></td>';
					}
				echo '<td style="width:50%;">'.$Tabdo_agend[$I][10].'</td>
				</tr>';
				}
?>
					
			</table>
			
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
						
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>
						
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>