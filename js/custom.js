// $(document).ready(function() {
//     var radio = $('.filter-radio input[type="radio"]').val();
//     if (radio == 'en cours') {
//         $(".etat").parent().css('display', 'table-row');
//         $(".etat").not("[name='en cours']").parent().css('display', 'none');
//     };
//     $('.filter-radio input[type="radio"]').change(function() {
//         if ($(this).val() == 'en cours') {
//             $(".etat").parent().css('display', 'table-row');
//             $(".etat").not("[name='en cours']").parent().css('display', 'none');
//         } else if ($(this).val() == 'annulation') {
//             $(".etat").parent().css('display', 'table-row');
//             $(".etat").not("[name='annulation']").parent().css('display', 'none');
//         } else if ($(this).val() == 'erreur saisie') {
//             $(".etat").parent().css('display', 'table-row');
//             $(".etat").not("[name='erreur saisie']").parent().css('display', 'none');
//         } else if ($(this).val() == 'commande') {
//             $(".type").parent().css('display', 'table-row');
//             $(".type").not("[name='commande']").parent().css('display','none');
//         } else if ($(this).val() == 'devis') {
//             $(".type").parent().css('display', 'table-row');
//             $(".type").not("[name='devis']").parent().css('display','none');
//         } else if ($(this).val() == 'facture') {
//             $(".type").parent().css('display', 'table-row');
//             $(".type").not("[name='facture']").parent().css('display','none');
//         } else if ($(this).val() == 'fact_av') {
//             $(".type").parent().css('display', 'table-row');
//             $(".type").not("[name='fact_av']").parent().css('display','none');
//         }
//     })
// })

$(document).ready(function() {
    var url = window.location.href;
    if (url.indexOf('search') >= 0) {
        $('.type').parent().css('display', 'table-row');
        $('input[type="radio"]').attr('checked', false);
    } else {
        $('.type').parent().css('display', 'none');
    }
    $('.filter-radio input[type="radio"]').change(function() {
        var etat = '[name="' + $('input[name=etat]:checked').val() + '"]';
        var type = '[name="' + $('input[name=type]:checked').val() + '"]';
        var etatVL = $('input[name=etat]:checked').val();
        var typeVL = $('input[name=type]:checked').val();
        console.log(etatVL);
        console.log(typeVL);
        if (etatVL != undefined && typeVL != undefined && etatVL != 'noselectetat' && typeVL != 'noselecttype') {
            $('.type').parent().css('display', 'none');
            // $(etat).parent().css('display', 'table-row');
            // $(type).parent().css('display', 'table-row');
            $(etat).parent().find(type).parent().css('display', 'table-row');
            $('.all input[type="radio"]').attr('checked', false);
            console.log('truog hợp 1');
        } else {
            console.log('truong hop 2');
            if (etatVL == 'noselectetat' || etatVL == undefined) {
                if (typeVL != 'noselecttype' && typeVL != undefined) {
                    $('.type').parent().css('display', 'none');
                    $(type).parent().css('display', 'table-row');
                    console.log('truong hợp 2.1.1');
                } else {
                    $('.type').parent().css('display', 'table-row');
                    console.log('truong hợp 2.1.2');
                }
            } else if (typeVL == 'noselecttype' || typeVL == undefined) {
                if (etatVL != 'noselectetat' && etatVL != undefined) {
                    $('.type').parent().css('display', 'none');
                    $(etat).parent().css('display', 'table-row');
                    console.log('truong hợp 2.2.1');
                } else {
                    $('.type').css('display', 'table-row');
                    console.log('truong hợp 2.2.2');
                }

            } else {
                console.log('truong hợp 2.3');
                $('.type').parent().css('display', 'table-row');
            }

            $('.all input[type="radio"]').attr('checked', false);
        }


    });
    $('.all input[name="all"]').click(function() {
        $('.filter-radio input[type="radio"]').attr('checked', false);
        $('.all input[name="rien"]').attr('checked', false);
        $('.type').parent().css('display', 'table-row');
    });
    $('.all input[name="rien"]').click(function() {
        $('.all input[name="all"]').attr('checked', false);
        $('.filter-radio input[type="radio"]').attr('checked', false);
        $('.type').parent().css('display', 'none');
    });

});
