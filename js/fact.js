$(document).ready(function() {
    $('#nom_client').select2();
    $('.select-product').select2();
    $('.select-desc').select2();

    $('.addProductBtn').click(function() {
        $('.select-product').select2('destroy');
        var el = $('.product-row:first').clone('click');
        el.insertAfter('.product-row:last');
        $('.select-product').select2();
    })

    $('.removeProductBtn').click(function(e) {
        if($('.product-row').length === 1) {
        	alert('ne peut pas supprimer cet article');
        	return;
        }
        var cf = confirm('Êtes-vous sûr de bien vouloir supprimer cet élément?');
        if (cf === undefined || cf === false) {
            return;
        }
        $(e.target).parent().parent().parent().remove();
    })

    $('.addDescBtn').click(function() {
        $('.select-desc').select2('destroy');
        var el = $('.desc-row:first').clone('click');
        el.insertAfter('.desc-row:last');
        $('.select-desc').select2();
    })

    $('.removeDescBtn').click(function(e) {
        if($('.desc-row').length === 1) {
        	alert('ne peut pas supprimer cet article');
        	return;
        }
        var cf = confirm('Êtes-vous sûr de bien vouloir supprimer cet élément?');
        if (cf === undefined || cf === false) {
            return;
        }
        $(e.target).parent().parent().parent().remove();
    })
    

})
