<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{

	if(isset($_POST['ref']))      $ref=$_POST['ref'];
	else      $ref="";

	if(isset($_POST['date_complete']))      $date_complete=$_POST['date_complete'];
	else      $date_complete="";
	
	if(isset($_POST['intit_action']))      $intit_action=$_POST['intit_action'];
	else      $intit_action="";
	
	if(isset($_POST['action']))      $action=$_POST['action'];
	else      $action="";
	
	if(isset($_POST['ListCli']))      $ListCli=$_POST['ListCli'];
	else      $ListCli="";
	
	if(isset($_POST['ListFour']))      $ListFour=$_POST['ListFour'];
	else      $ListFour="";
		
	if(isset($_POST['ListProd']))      $ListProd=$_POST['ListProd'];
	else      $ListProd="";
	
	if(isset($_POST['ListPrest']))      $ListPrest=$_POST['ListPrest'];
	else      $ListPrest="";
	
	if(isset($_POST['detail_agend']))      $detail_agend=$_POST['detail_agend'];
	else      $detail_agend="";
	
	if(isset($_POST['date_deb_c']))      $date_deb_c=$_POST['date_deb_c'];
	else      $date_deb_c="";
		
	if(isset($_POST['date_fin_c']))      $date_fin_c=$_POST['date_fin_c'];
	else      $date_fin_c="";
	
	if(isset($_POST['horaire_deb']))      $horaire_deb=$_POST['horaire_deb'];
	else      $horaire_deb="";
		
	if(isset($_POST['horaire_fin']))      $horaire_fin=$_POST['horaire_fin'];
	else      $horaire_fin="";

	if(isset($_POST['cli_agend']))      $cli_agend=$_POST['cli_agend'];
	else      $cli_agend="";
	
	if(isset($_POST['couleur']))      $couleur=$_POST['couleur'];
	else      $couleur="";
	
	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	$date_complete = mysqli_real_escape_string($db, $date_complete);
	$intit_action = mysqli_real_escape_string($db, $intit_action);
	$action = mysqli_real_escape_string($db, $action);
	$ListCli = mysqli_real_escape_string($db, $ListCli);
	$ListFour = mysqli_real_escape_string($db, $ListFour);
	$ListProd = mysqli_real_escape_string($db, $ListProd);
	$ListPrest = mysqli_real_escape_string($db, $ListPrest);
	$detail_agend = mysqli_real_escape_string($db, $detail_agend);
	$horaire_deb = mysqli_real_escape_string($db, $horaire_deb);
	$horaire_fin = mysqli_real_escape_string($db, $horaire_fin);
	$couleur = mysqli_real_escape_string($db, $couleur);
		
	mysqli_query($db, "INSERT into $db_agenda (nom_membre, date_complete, intit_action, horaire_deb, horaire_fin, action, ListCli, ListFour, ListProd, ListPrest, detail_agend, couleur) VALUES ('$nom_membre','$date_complete','$intit_action','$horaire_deb','$horaire_fin','$action','$ListCli','$ListFour','$ListProd','$ListPrest','$detail_agend','$couleur')")
	or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
	mysqli_query($db, "UPDATE $db_agenda SET couleur='$couleur' WHERE date_complete='$date_complete'")
	or die('<span class="err_bdd">Erreur d\'insertion, veuillez resaisir</span>');
	if ($cli_agend=="")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		header('Location: http://'.$link_domain.'/modif_cli.php?ref='.$cli_agend);
		exit;
		}
	}
?>