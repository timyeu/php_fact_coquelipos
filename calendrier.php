<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Calendrier</h2>
			
				<p>
				G&eacute;rez votre agenda avec ce <strong>calendrier</strong>.
				</p>
				
				<p>
				Nous sommes le <strong><?php echo date('d/m/Y');?></strong>.
				</p>
				
			</div>

		<fieldset>
			
			<legend> Calendrier : </legend>
				
<?php

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	$Requete_calend = "SELECT * FROM $db_agenda WHERE nom_membre='$nom_membre'";
	$ResReq_calend = mysqli_query($db, $Requete_calend) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>');
	$nbenreg_calend = mysqli_num_rows($ResReq_calend);
	$nbchamps_calend = mysqli_num_fields($ResReq_calend);
	$Tabdo_calend[$nbenreg_calend][$nbchamps_calend]="";
	$Tabchamps_calend[$nbchamps_calend]="";
	
	for($M=0; $M < $nbchamps_calend; $M++) 
		{
		$tabchamps_calend[$M] = mysqli_fetch_field_direct($ResReq_calend,$M);
		}
	
	$M=0;
			
	while ($donnees_calend = mysqli_fetch_array($ResReq_calend))
		{	
		$Tabdo_calend[$M][1]=$donnees_calend["ref"];
		$Tabdo_calend[$M][2]=$donnees_calend["nom_membre"];
		$Tabdo_calend[$M][3]=$donnees_calend["date_complete"];
		$Tabdo_calend[$M][4]=$donnees_calend["intit_action"];
		$Tabdo_calend[$M][5]=$donnees_calend["horaire_deb"];
		$Tabdo_calend[$M][6]=$donnees_calend["horaire_fin"];
		$Tabdo_calend[$M][7]=$donnees_calend["action"];
		$Tabdo_calend[$M][8]=$donnees_calend["ListCli"];
		$Tabdo_calend[$M][9]=$donnees_calend["ListFour"];
		$Tabdo_calend[$M][10]=$donnees_calend["ListProd"];
		$Tabdo_calend[$M][11]=$donnees_calend["ListPrest"];
		$Tabdo_calend[$M][12]=$donnees_calend["detail_agend"];
		$Tabdo_calend[$M][13]='blue';
		$M++;
		}
		
	$m = (isset($_GET['m'])) ? $_GET['m'] : date("n");
	$a = (isset($_GET['a'])) ? $_GET['a'] : date("Y");
	$mnom = Array("","Janvier","Fevrier","Mars" ,"Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");
	$dayone = date("w",mktime(1,1,1,$m,1,$a));
	$daytod = date('d');
	$an=$a;
	$mois=$m;
	$m_c=$m;
	$a_c=$a;
	$a_trce=0;
	$td_couleur="";
	$input_couleur="";
	$doublon_date="";
	if ($m_c<10)
		{
		$m_c='0'.$m_c;
		}
	
	if ($dayone==0) 
		{
		$dayone=7;
		}
	$url = "calendrier.php";
	
	for($i=1;$i<13;$i++)
		{   
		echo '<a href='.$url.'?m='.$i.'&a='.$a.'><input type="button" style="width: 8.3%;" value="'.$mnom[$i].'"/></a>';
		}
		
	$mois=$m-1;
	$an=$a;

	if($mois==0) 
		{
		$mois=12;
		$an=$a-1;
		}
	
	echo '<div id="NavBar">
			<table>
				<tr>
					<td><a href='.$url.'?m='.$mois.'&a='.$an.'><input type="button" value="<-- Pr&eacute;c"/></a></td>
					<td>'.$mnom[$m].' '.$a.'</td>';
	
	$an=$a;
	$mois=$m+1;
	
	if ($mois==13) 
		{
		$mois=1;
		$an=$a+1;
		}
		
	$jours_in_month=cal_days_in_month(CAL_GREGORIAN,$m,$a);
	$gg=$jours_in_month+$dayone-1;
	$nb_semaine=ceil($gg/7);
	$jours_a_afficher=$nb_semaine*7;
	
	echo '<td><a href='.$url.'?m='.$mois.'&a='.$an.'><input type="button" value="Suiv -->"/></a></td>
		</tr>
	</table>';
	
	if ($mois<10)
		{
		$mois='0'.$mois;
		}
	
	echo '<form name="when">
		<table>
			<tr>
				<th style="width:70px;">L</th>
				<th style="width:70px;">M</th>
				<th style="width:70px;">M</th>
				<th style="width:70px;">J</th>
				<th style="width:70px;">V</th>
				<th style="width:70px;">S</th>
				<th style="width:70px;">D</th>';
	for ($n=1;$n<=$jours_a_afficher;$n++)
		{
		echo '<form action="agenda.php" method="post">';
		if ($n%7 == 1)   
			{
			echo'</tr><tr>';
			}
			
		if ($n<($jours_in_month+$dayone) && $n>=$dayone)
			{		
			$a=$n-$dayone+1;
			if ($a<10)
				{
				$a='0'.$a;
				}
			$date_verif=$a.'/'.$m_c.'/'.$a_c;

			for($M=0; $M < $nbenreg_calend; $M++) 
				{
				if ($Tabdo_calend[$M][3]==$date_verif)
					{
					switch ($Tabdo_calend[$M][13]) 
						{
						case "cald":
						$td_couleur="cald";
						$input_couleur="calend";
						break;
						case "cald1":
						$td_couleur="cald1";
						$input_couleur="calend1";
						break;
						case "cald2":
						$td_couleur="cald2";
						$input_couleur="calend2";
						break;
						case "cald3":
						$td_couleur="cald3";
						$input_couleur="calend3";
						break;
						case "cald4":
						$td_couleur="cald4";
						$input_couleur="calend4";
						break;
						case "cald5":
						$td_couleur="cald5";
						$input_couleur="calend5";
						break;
						case "cald6":
						$td_couleur="cald6";
						$input_couleur="calend6";
						break;
						case "cald7":
						$td_couleur="cald7";
						$input_couleur="calend7";
						break;
						case "cald8":
						$td_couleur="cald8";
						$input_couleur="calend8";
						break;
						case "cald9":
						$td_couleur="cald9";
						$input_couleur="calend9";
						break;
						case "cald10":
						$td_couleur="cald10";
						$input_couleur="calend10";
						break;
						case "cald11":
						$td_couleur="cald11";
						$input_couleur="calend11";
						break;
						default:
						$td_couleur="cald";
						$input_couleur="calend";
						}
					echo '<input type="hidden" name="annee_agend" value="'.$a_c.'"/>
					<input type="hidden" name="mois_agend" value="'.$m_c.'"/>';
					if ($doublon_date!=$Tabdo_calend[$M][3])
						{
						echo '<td class="'.$td_couleur.'"><input type="submit" class="'.$input_couleur.'" name="date_agend" value="'.$a.'"/>
						</br></br>De <strong class="gl">'.$Tabdo_calend[$M][5].'</strong> &agrave; <strong class="gl">'.$Tabdo_calend[$M][6].'</strong>
						</br></br><strong class="gl">'.$Tabdo_calend[$M][4].'</strong>
						</br>'.$Tabdo_calend[$M][7];
						if ($Tabdo_calend[$M][8]!="")
							{
							$Requete_calend_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='".$Tabdo_calend[$M][8]."'";
							$ResReq_calend_cli = mysqli_query($db, $Requete_calend_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
							$Donnees_calend_cli = mysqli_fetch_array($ResReq_calend_cli);
							$nom_calend_cli=$Donnees_calend_cli["nom"];
							echo '</br></br>Client : <strong class="gl">'.$nom_calend_cli.'</strong>';
							}
						if ($Tabdo_calend[$M][9]!="")
							{
							$Requete_calend_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='".$Tabdo_calend[$M][9]."'";
							$ResReq_calend_four = mysqli_query($db, $Requete_calend_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
							$Donnees_calend_four = mysqli_fetch_array($ResReq_calend_four);
							$nom_calend_four=$Donnees_calend_four["nom"];
							echo '</br></br>Fournisseur : <strong class="gl">'.$nom_calend_four.'</strong>';
							}
						if ($Tabdo_calend[$M][10]!="")
							{
							$Requete_calend_prod = "SELECT designation FROM $db_prod_prest WHERE ref_produits='".$Tabdo_calend[$M][10]."'";
							$ResReq_calend_prod = mysqli_query($db, $Requete_calend_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
							$Donnees_calend_prod = mysqli_fetch_array($ResReq_calend_prod);
							$nom_calend_prod=$Donnees_calend_prod["designation"];
							echo '</br></br>Produit : <strong class="gl">'.$nom_calend_prod.'</strong>';
							}
						if ($Tabdo_calend[$M][11]!="")
							{
							$Requete_calend_prest = "SELECT designation FROM $db_prod_prest WHERE ref_produits='".$Tabdo_calend[$M][11]."'";
							$ResReq_calend_prest = mysqli_query($db, $Requete_calend_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
							$Donnees_calend_prest = mysqli_fetch_array($ResReq_calend_prest);
							$nom_calend_prest=$Donnees_calend_prest["designation"];
							echo '</br></br>Prestation : <strong class="gl">'.$nom_calend_prest.'</strong>';
							}
						echo '</br><em>'.$Tabdo_calend[$M][12].'</em>';
						}
					else
						{
						echo '</br></br>De <strong class="gl">'.$Tabdo_calend[$M][5].'</strong> &agrave; <strong class="gl">'.$Tabdo_calend[$M][6].'</strong>
						</br></br><strong class="gl">'.$Tabdo_calend[$M][4].'</strong>
						</br>'.$Tabdo_calend[$M][7];
						if ($Tabdo_calend[$M][8]!="")
							{
							$Requete_calend_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='".$Tabdo_calend[$M][8]."'";
							$ResReq_calend_cli = mysqli_query($db, $Requete_calend_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
							$Donnees_calend_cli = mysqli_fetch_array($ResReq_calend_cli);
							$nom_calend_cli=$Donnees_calend_cli["nom"];
							echo '</br></br>Client : <strong class="gl">'.$nom_calend_cli.'</strong>';
							}
						if ($Tabdo_calend[$M][9]!="")
							{
							$Requete_calend_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='".$Tabdo_calend[$M][9]."'";
							$ResReq_calend_four = mysqli_query($db, $Requete_calend_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
							$Donnees_calend_four = mysqli_fetch_array($ResReq_calend_four);
							$nom_calend_four=$Donnees_calend_four["nom"];
							echo '</br></br>Fournisseur : <strong class="gl">'.$nom_calend_four.'</strong>';
							}
						if ($Tabdo_calend[$M][10]!="")
							{
							$Requete_calend_prod = "SELECT designation FROM $db_prod_prest WHERE ref_produits='".$Tabdo_calend[$M][10]."'";
							$ResReq_calend_prod = mysqli_query($db, $Requete_calend_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
							$Donnees_calend_prod = mysqli_fetch_array($ResReq_calend_prod);
							$nom_calend_prod=$Donnees_calend_prod["designation"];
							echo '</br></br>Produit : <strong class="gl">'.$nom_calend_prod.'</strong>';
							}
						if ($Tabdo_calend[$M][11]!="")
							{
							$Requete_calend_prest = "SELECT designation FROM $db_prod_prest WHERE ref_produits='".$Tabdo_calend[$M][11]."'";
							$ResReq_calend_prest = mysqli_query($db, $Requete_calend_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
							$Donnees_calend_prest = mysqli_fetch_array($ResReq_calend_prest);
							$nom_calend_prest=$Donnees_calend_prest["designation"];
							echo '</br></br>Prestation : <strong class="gl">'.$nom_calend_prest.'</strong>';
							}
						echo '</br><em>'.$Tabdo_calend[$M][12].'</em>';
						}
					$doublon_date=$date_verif;
					$a_trce=$a;
					}
				}
			if ($a_trce!=$a)
				{
				echo '<input type="hidden" name="annee_agend" value="'.$a_c.'"/>
				<input type="hidden" name="mois_agend" value="'.$m_c.'"/>
				<td class="cald"><input type="submit" name="date_agend" value="'.$a.'"/></td>';
				}
			}
		else
			{
			echo '<td style="width:70px; height: 100px; background: #ECECEC;"></td>';
			}
		echo '</form>';
		}
	echo '</tr>
		</table>
</div>';

?>
			
			<p class="cen"><strong>S&eacute;lectionnez une date pour ajouter une information &agrave; votre agenda</strong></p>
			
		</fieldset>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>