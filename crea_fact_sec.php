<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Cr&eacute;ation document</h2>
			
				<p>
				Ce formulaire vous permet de <strong>cr&eacute;er un nouveau document</strong>.
				</p>
				
			</div>
			
<?php

	if(isset($_POST['numfact']))      $numfact=$_POST['numfact'];
	else      $numfact="";
	
	if(isset($_POST['nom_param']))      $nom_param=$_POST['nom_param'];
	else      $nom_param="";
	
	if(isset($_POST['nom_client']))      $nom_client=$_POST['nom_client'];
	else      $nom_client="";
	
	if(isset($_POST['contact']))      $contact=$_POST['contact'];
	else      $contact="";

	if(isset($_POST['produit']))      $produit=$_POST['produit'];
	else      $produit="";	
	
	if(isset($_POST['prestation']))      $prestation=$_POST['prestation'];
	else      $prestation="";	
	
	if(isset($_POST['date_valid_deb']))      $date_valid_deb=$_POST['date_valid_deb'];
	else      $date_valid_deb="";	
	
	if(isset($_POST['date_valid_fin']))      $date_valid_fin=$_POST['date_valid_fin'];
	else      $date_valid_fin="";	

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	
	
	$Requete3 = "SELECT nom, contact FROM $db_compte_client ORDER by nom";
	$ResReq3 = mysqli_query($db, $Requete3) or die('<span class="err_bdd">Erreur de s&eacute;lection, club incorrect ou inexistant</span>');
	
	$Requete4 = "SELECT nom_param FROM $db_parametres ORDER by nom_param";
	$ResReq4 = mysqli_query($db, $Requete4) or die('<span class="err_bdd">Erreur de s&eacute;lection, club incorrect ou inexistant</span>');
		
	
?>
			
		<form action="req_crea_fact.php" method="post">
			
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif ent&ecirc;te : </legend>
				
				<p><label class="gauche" for="numfact">Num&eacute;ro de document :</label>
				<input class="droit" id="numfact" type="text" name="numfact" value="<?php echo $numfact;?>"/></p>
				
				<p class="cen"><strong>Le num&eacute;ro de document est compos&eacute; d'un pr&eacute;fixe D,C ou F 
				(pour devis, commande ou facture) et d'un suffixe mois/ann&eacute;e</strong></p>
			
				<p><label class="gauche" for="nom_param">S&eacute;lection des param&egrave;tres</label>
				<select id="nom_param" class="droit" name="nom_param">
					<?php
					echo '<option value="'.$nom_param.'">'.$nom_param.'</option>';
					while ($LigneDo4 = mysqli_fetch_array($ResReq4)) 
						{
						$nom_param = $LigneDo4["nom_param"];
						echo '<option value="'.$nom_param.'">'.$nom_param.'</option>';
						}
					?>
				</select></p>
				
				<p><label class="gauche" for="nom_client">S&eacute;lection du client (et contact si indiqu&eacute;)</label>
				<select id="nom_client" class="droit" name="nom_client">
					<?php
					echo '<option value="'.$nom_client.'">'.$nom_client.' '.$contact.'</option>';
					while ($LigneDo3 = mysqli_fetch_array($ResReq3)) 
						{
						$nom_client = $LigneDo3["nom"];
						$contact_client = $LigneDo3["contact"];
						echo '<option value="'.$nom_client.'">'.$nom_client.' '.$contact_client.'</option>';
						}
					?>
				</select></p>
					
				<p class="cen">
				<strong>Vous pouvez &agrave; tout moment corriger votre saisie ent&ecirc;te en cas d'erreur</strong>
				</p>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Attribution des quantit&eacute;s des pi&egrave;ces (r&eacute;f. en rouge) : </legend>
			
				<?php
				
				if(isset($_POST['produit'])) 
					{
					$produit = explode("\n", $produit);
						foreach($produit as $val)
						{
							if ($val!="")
								{
								$ref_produit = explode(" | ", $val);
								$des_produit = $ref_produit[0];
								$ref_prod = $ref_produit[1];
								$info_produit = $ref_produit[2];
								echo '<input type="hidden" name="produit[]" value="'.$des_produit.'|'.$info_produit.'"/>
								<p><label class="gauche" for="quant">'.$des_produit.' <strong>'.$ref_prod.'</strong> '.$info_produit.'</label>
								<input class="droit" id="quant" type="text" name="quant[]" /></p>';
								}
						}
					}
				else
					{
					echo 'Pas de produits s&eacute;lectonn&eacute;s pour ce document';
					}
					
				?>
			
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Attribution des quantit&eacute;s des prestations (r&eacute;f. en rouge) : </legend>
			
				<?php
				
				if(isset($_POST['prestation'])) 
					{
					$prestation = explode("\n", $prestation);
						foreach($prestation as $val2)
						{
							if ($val2!="")
								{
								$ref_prestation = explode(" | ", $val2);
								$des_prestation = $ref_prestation[0];
								$ref_prest = $ref_prestation[1];
								$info_prestation = $ref_prestation[2];
								echo '<input type="hidden" name="prestation[]" value="'.$des_prestation.'|'.$info_prestation.'"/>
								<input type="hidden" name="info_prestation[]" value="'.$info_prestation.'"/>
								<p><label class="gauche" for="quant2">'.$des_prestation.' <strong>'.$ref_prest.'</strong> '.$info_prestation.'</label>
								<input class="droit" id="quant2" type="text" name="quant2[]" /></p>';
								}
						}
					}
				else
					{
					echo 'Pas de prestations s&eacute;lectonn&eacute;es pour ce document';
					}
					
				?>
			
			</fieldset>
			
			<fieldset>
				
				<legend class="lg">Date de validit&eacute; :</legend>
				
				<p><label class="gauche" for="date_valid_deb">Du :</label>
				<input class="droit" id="date_valid_deb" type="date" name="date_valid_deb" value="<?php echo $date_valid_deb;?>"/></p>
			
				<p><label class="gauche" for="date_valid_fin">Au :</label>
				<input class="droit" id="date_valid_fin" type="date" name="date_valid_fin" value="<?php echo $date_valid_fin;?>"/></p>
						
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Remise et avoir : </legend>
			
					<p><label class="gauche" for="avoir">Montant de l'avoir :</label>
					<input class="droit" id="avoir" type="text" name="avoir"/></p>
					
					<p><label class="gauche" for="remise_tot">Remise :</label>
					<input class="droit" id="remise_tot" type="text" name="remise_tot"/></p>
						
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p>Enregistrer ce document en tant que :</p>
				
				<p class="cen">
					Devis ? <input type="radio" name="type" checked value="devis"/>
					Commande ? <input type="radio" name="type" value="commande"/>
					Facture ? <input type="radio" name="type" value="facture"/>
				</p>
				
				<p class="cen"><strong>L'option devis est coch&eacute;e par d&eacute;faut :</strong></p>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Annuler"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="crea_fact.php">Revenir en arri&egrave;re</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>