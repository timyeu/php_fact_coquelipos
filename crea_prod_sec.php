<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');	

	$Requete = "SELECT * FROM $db_fournisseurs";

	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrecte ou inexistante</span>'); 
						
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">

			<h2>Ajout produit</h2>
			
				<p>
				Ce formulaire vous permet d'<strong>ajouter un nouveau produit</strong>.
				</p>		
				
			</div>
			<style>
				.produit{
					width: 50%;
					margin: auto;
				}
				#feuille_para{
					height: 90px;
				}
			</style>
		<form action="req_crea_prod.php" method="post" class="produit">
			
			<fieldset>
				
				<legend class="lg"> D&eacute;tail : </legend>
				
				<p><label class="gauche" for="designation">D&eacute;signation :</label>
				<input class="droit" id="designation" type="text" name="designation"/></p>
				
				<p><label class="gauche" for="reference">R&eacute;f&eacute;rence :</label>
				<input class="droit" id="reference" type="text" name="reference"/></p>
				
				<p><label class="gauche" for="nature">Nature :</label>
				<select id="nature" class="droit" name="nature">
						<option value="produit">Produit</option>
						<option value="prestation">Prestation</option>
				</select></p>
				
				<p><label class="gauche" for="informations">Informations :</label>
				<textarea class="droit" id="informations" name="informations"></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="quantite">Quantit&eacute; :</label>
				<input class="droit" id="quantite" type="text" name="quantite"/></p>
				
				<p><label class="gauche" for="qte_limite">Quantit&eacute; limite :</label>
				<input class="droit" id="qte_limite" type="text" name="qte_limite"/></p>
				
				<p><label class="gauche" for="prix_achat">Prix d'achat :</label>
				<input class="droit" id="prix_achat" type="text" name="prix_achat"/></p>
				
				<p><label class="gauche" for="coef">Coefficient :</label>
				<input class="droit" id="coef" type="text" name="coef"/></p>
				
				<p><label class="gauche" for="prix_vente">Prix de vente :</label>
				<input class="droit" id="prix_vente" type="text" name="prix_vente"/></p>
				
				<p><label class="gauche" for="taux_TVA">Taux TVA :</label>
				<select id="taux_TVA" class="droit" name="taux_TVA">
					<option value="20">20 %</option>
					<option value="10">10 %</option>
					<option value="5.5">5.5 %</option>
					<option value="2.1">2.1 %</option>
					<option value="">Pas de TVA</option>
				</select></p>
				
				<p><label class="gauche" for="prix_TTC">Prix TTC (prioritaire si saisi) :</label>
				<input class="droit" id="prix_TTC" type="text" name="prix_TTC"/></p>
				
				<p><label class="gauche" for="remise">Remise :</label>
				<input class="droit" id="remise" type="text" name="remise"/></p>

				<p><label class="gauche" for="ref_fournisseur">Fournisseur :</label>
				<select id="ref_fournisseur" class="droit" name="ref_fournisseur">
<?php
						while ($LigneDo = mysqli_fetch_array($ResReq)) 
							{
							$Nref_four = $LigneDo["ref_fournisseur"];
							$Nm_four = $LigneDo["nom"];
							echo '<option value="'.$Nref_four.'">'.$Nm_four.'</option>';
							}
?>
						<option value="">Aucun</option>
				</select></p>
				
				<p><label class="gauche" for="ref_prod_fournisseur">R&eacute;f&eacute;rence produit fournisseur :</label>
				<input class="droit" id="ref_prod_fournisseur" type="text" name="ref_prod_fournisseur"/></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>