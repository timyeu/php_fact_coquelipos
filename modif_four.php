<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc" class="edit-form">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation fournisseur</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier un fournisseur existant</strong>.
				</p>
				
			</div>

<?php
	
	if (isset($_POST['ListFour'])) $ListFour=$_POST['ListFour'];
		else $ListFour="";
		
	if (isset($_GET['ref'])) $ListFour=$_GET['ref'];
		else $ListFour=$ListFour;
		
	$Requete = "SELECT * FROM $db_fournisseurs WHERE ref_fournisseur ='$ListFour'";
	
		if (empty($ListFour))
			{
				echo "<p style='margin-left: 20px;'>Il faut selectionner un fournisseur dans la liste</p>";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
				
				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref_fournisseur=$Donnees["ref_fournisseur"];
				$nom=$Donnees["nom"];
				$adresse=$Donnees["adresse"];
				$ville=$Donnees["ville"];
				$code_postal=$Donnees["code_postal"];
				$telephone=$Donnees["telephone"];
				$fax=$Donnees["fax"];
				$email=$Donnees["email"];
				$pays=$Donnees["pays"];
				$site_web=$Donnees["site_web"];
				$nom_contact=$Donnees["nom_contact"];
				$tel_contact=$Donnees["tel_contact"];
				$siret=$Donnees["siret"];
				$TVA_intra=$Donnees["TVA_intra"];
				$prod_prop=$Donnees["prod_prop"];
				$memo=$Donnees["memo"];
			}

?>
			
		<form action="req_modif_four.php" method="post">
		<input type="hidden" name="ref_fournisseur" id="ref_fournisseur" value="<?php echo $ref_fournisseur;?>" />
		
			<fieldset>
				
				<legend class="lg"> Detail fournisseur : </legend>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom" value="<?php echo $nom;?>"/></p>
				
				<p><label class="gauche" for="adresse">Adresse :</label>
				<input class="droit" id="adresse" type="text" name="adresse" value="<?php echo $adresse;?>"/></p>
				
				<p><label class="gauche" for="ville">Ville :</label>
				<input class="droit" id="ville" type="text" name="ville" value="<?php echo $ville;?>"/></p>
				
				<p><label class="gauche" for="code_postal">Code postal :</label>
				<input class="droit" id="code_postal" type="text" name="code_postal" value="<?php echo $code_postal;?>"/></p>
				
				<p><label class="gauche" for="telephone">Telephone :</label>
				<input class="droit" id="telephone" type="text" name="telephone" value="<?php echo $telephone;?>"/></p>

				<p><label class="gauche" for="fax">Fax :</label>
				<input class="droit" id="fax" type="text" name="fax" value="<?php echo $fax;?>"/></p>
				
				<p><label class="gauche" for="email">E-mail :</label>
				<input class="droit" id="email" type="text" name="email" value="<?php echo $email;?>"/></p>
				
				<p><label class="gauche" for="pays">Pays :</label>
				<input class="droit" id="pays" type="text" name="pays" value="<?php echo $pays;?>"/></p>
				
				<p><label class="gauche" for="site_web">Site web :</label>
				<input class="droit" id="site_web" type="text" name="site_web" value="<?php echo $site_web;?>"/></p>
				
				<p><label class="gauche" for="nom_contact">Nom contact :</label>
				<input class="droit" id="nom_contact" type="text" name="nom_contact" value="<?php echo $nom_contact;?>"/></p>
				
				<p><label class="gauche" for="tel_contact">Telephone contact :</label>
				<input class="droit" id="tel_contact" type="text" name="tel_contact" value="<?php echo $tel_contact;?>"/></p>
				
				<p><label class="gauche" for="siret">Siret :</label>
				<input class="droit" id="siret" type="text" name="siret" value="<?php echo $siret;?>"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA intra-communautaire :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra" value="<?php echo $TVA_intra;?>"/></p>
				
				<p><label class="gauche" for="prod_prop">Produits propos&eacute;s :</label>
				<textarea class="droit" id="prod_prop" name="prod_prop"><?php echo $prod_prop;?></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="memo">Memo :</label>
				<textarea class="droit" id="memo" name="memo"><?php echo $memo;?></textarea></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Modifier"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_modif_four.php">Revenir &agrave; la liste de modification des fournisseurs</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>