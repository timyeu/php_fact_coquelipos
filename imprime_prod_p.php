<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
?>

<!DOCTYPE html>

<html lang="fr">

	<head>
		<title>Coquelipos Stock</title>
		<link rel="stylesheet" type="text/css" title= "design" href="Style/Main.css" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex,nofollow" />
		
		<script type="text/javascript">
			window.print();
		</script>
	</head>
	
	<body style="margin: 0; padding: 0; background: #FFFFFF;">

	<div id="feuille_imp">
	
		<table style="border: 2px solid #B01807;">
			
			<tr>
				<td style="background: #F14835;"><p class="imp_5">Fiche inventaire prestations</p></td>
			</tr>
			
		</table>
	
		<table style="border: solid 2px #396208;">
				
					<tr>
					
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">D&eacute;signation</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">R&eacute;f&eacute;rence</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Qte.</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Prix achat (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Coef.</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Taux TVA (%)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Prix HT (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Total HT (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Prix TTC (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Total TTC (&euro;)</p></td>
						
					</tr>

<?php

	$Glob_PA="";
	$Glob_HT="";
	$Glob_TTC="";
		
	$Requete = "SELECT * FROM $db_prod_prest WHERE nature = 'produit' ORDER BY designation";

		$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
		mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

		$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, pi&egrave;ce incorrecte ou inexistante</span>'); 
		$nbenreg = mysqli_num_rows($ResReq);
		$nbchamps = mysqli_num_fields($ResReq);
		$Tabdo[$nbenreg][$nbchamps]="";
		$Tabchamps[$nbchamps]="";

					for($I=0; $I < $nbchamps; $I++) 
						{
						$tabchamps[$I] = mysqli_fetch_field_direct($ResReq,$I);
						}
	
					$I=0;
					while ($donnees = mysqli_fetch_array($ResReq))
						{
							$Tabdo[$I][1]=$donnees["ref_produits"];
							$Tabdo[$I][2]=$donnees["designation"];
							$Tabdo[$I][3]=$donnees["nature"];
							$Tabdo[$I][4]=$donnees["reference"];
							$Tabdo[$I][5]=$donnees["quantite"];
							$Tabdo[$I][6]=$donnees["qte_limite"];
							$Tabdo[$I][7]=$donnees["prix_achat"];
							$Tabdo[$I][8]=$donnees["coef"];
							$Tabdo[$I][9]=$donnees["prix_vente"];
							$Tabdo[$I][10]=$donnees["taux_TVA"];
							$Tabdo[$I][11]=$donnees["remise"];
							$Tabdo[$I][12]=$donnees["prix_TTC"];
							$Tabdo[$I][13]=$donnees["ref_fournisseur"];
							$Tabdo[$I][14]=$donnees["ref_prod_fournisseur"];
							$ref_produits=$Tabdo[$I][1];
							$designation=$Tabdo[$I][2];
							$nature=$Tabdo[$I][3];
							$reference=$Tabdo[$I][4];
							$quantite=$Tabdo[$I][5];
							$qte_limite=$Tabdo[$I][6];
							$prix_achat=$Tabdo[$I][7];
							$coef=$Tabdo[$I][8];
							$prix_vente=$Tabdo[$I][9];
							$taux_TVA=$Tabdo[$I][10];
							$remise=$Tabdo[$I][11];
							$prix_TTC=$Tabdo[$I][12];
							$ref_fournisseur=$Tabdo[$I][13];
							$ref_prod_fournisseur=$Tabdo[$I][14];
							
							$Tot_HT=$prix_vente*$quantite;
							$Tot_TTC=$prix_TTC*$quantite;
							$Tot_achat=$prix_achat*$quantite;
							
							$Glob_PA=$Glob_PA+$Tot_achat;
							$Glob_HT=$Glob_HT+$Tot_HT;
							$Glob_TTC=$Glob_TTC+$Tot_TTC;
									
?>
					<tr>
					
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $designation;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $reference;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $quantite;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $prix_achat;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $coef;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $taux_TVA;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $prix_vente;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $Tot_HT;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $prix_TTC;?></p></td>
						<td style="border: solid 1px #396208;"><p class="imp_4"><?php echo $Tot_TTC;?></p></td>
						
					</tr>
						
<?php
							$I++;
						}
?>
					
					<tr>
					
						<td style="border: solid 1px #396208; background: #89EC14;" colspan="3"><p class="imp_6">Global achat (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6"><?php echo $Glob_PA;?></p></td>
						<td style="border: solid 1px #396208; background: #89EC14;" colspan="3"><p class="imp_6">Global HT (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6"><?php echo $Glob_HT;?></p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Global TTC (&euro;)</p></td>
						<td style="border: solid 1px #396208; background: #89EC14;"><p class="imp_6"><?php echo $Glob_TTC;?></p></td>
						
					</tr>
					
				</table>
	
	</div>
	
	</body>

</html>

<?php
		}
require_once 'Main_ft.php'; 
?>