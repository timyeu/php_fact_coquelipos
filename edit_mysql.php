<!DOCTYPE html>
<html lang="fr">

	<head>
		<title>Coquelipos Facturation</title>
		<link rel="stylesheet" type="text/css" title= "design" href="Style/Main.css" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex,nofollow" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/custom.js"></script>
		<script src="js/jquery.validate.js"></script>
		<style>
			.edit-form{
				width: 70% !important;
				margin: auto;
			}
			#feuille_para{
				height: 120px;
			}
		</style>
	</head>
	
	<body style="margin: 0; padding: 0; background: url(Images/Fond.png) repeat-x;" onLoad="Defaults()">
	
	<div id="conteneur">
		
		<div id="logo">
			<img alt="coquelipos" src="Images/Logo.png"/>
		</div>
		
		<div id="titre">
			<img alt="Coquelipos Facturation" src="Images/Titre.png"/>
		</div>
			
		
		<div id="Contenu" style="border-top:none !important;top: 150px;">







	<div id="feuille">
		<div id="feuille_bloc" class="edit-form">

			<div id="feuille_para">

			<h2>Bienvenue sur le logiciel de facturation de Coquelipos</h2>

				<p>
				Vous devez vous <strong>connecter</strong> pour continuer.
				</p>

			</div>

			<form action="action.php" method="post">

				<fieldset class="form-small">

					<legend> Connexion : </legend>

					<p><label class="gauche" for="ident">Server :</label>
					<input class="droit" id="ident" type="text" name="localhost" value="localhost" required /></p>

					<p><label class="gauche" for="ident">User :</label>
					<input class="droit" id="ident" type="text" name="user" value="" required /></p>

                    <p><label class="gauche" for="mot">Password :</label>
                    <input class="droit" id="mot" type="password" name="password" value="" /></p>

					<p><label class="gauche" for="mot">Name Database :</label>
					<input class="droit" id="mot" type="text" name="database" value="" required /></p>

				</fieldset>

				<fieldset>

					<legend> Validation : </legend>

					<p class="cen">
						<input type="submit" value="Valider"/>
						<input type="reset" value="R&eacute;initialiser"/>
					</p>

				</fieldset>

			</form>

		</div>

	</div>

<?php
require_once 'Main_ft.php';
?>