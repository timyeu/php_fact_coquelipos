<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation param&egrave;tres</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier des param&egrave;tres relatifs aux factures, devis et commandes existants</strong>.
				</p>
				
			</div>

<?php

	if (isset($_POST['ListParam'])) $ListParam=$_POST['ListParam'];
		else $ListParam="";
		
	if (isset($_GET['ref'])) $ListParam=$_GET['ref'];
		else $ListParam=$ListParam;
		
	$Requete = "SELECT * FROM $db_parametres WHERE ref ='$ListParam'";

		if (empty($ListParam))
			{
				echo "Il faut selectionner des param&egrave;tres dans la liste";
			}
			
			else
			{	
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
					
				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, param&egrave;tres incorrects ou inexistants</span>');

				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref=$Donnees["ref"];
				$nom_param=$Donnees["nom_param"];
				$nom_entreprise=$Donnees["nom_entreprise"];
				$add_entreprise=$Donnees["add_entreprise"];
				$cp_ville=$Donnees["cp_ville"];
				$tel=$Donnees["tel"];
				$lieu=$Donnees["lieu"];
				$TVA_intra=$Donnees["TVA_intra"];
				$cond_devis=$Donnees["cond_devis"];
				$cond_commande=$Donnees["cond_commande"];
				$cond_facture=$Donnees["cond_facture"];
				$mentions=$Donnees["mentions"];
				$coord_banque=$Donnees["coord_banque"];
				$coord_RIB=$Donnees["coord_RIB"];
				$coord_IBAN=$Donnees["coord_IBAN"];
				$app=$Donnees["app"];
				$clause=$Donnees["clause"];
				$capital=$Donnees["capital"];
				$SIREN=$Donnees["SIREN"];
				$RCS=$Donnees["RCS"];
				$APE=$Donnees["APE"];
				$info_autre=$Donnees["info_autre"];
				$logo_param=$Donnees["logo_param"];
				
				$app = explode("-", $app);
				$app_devis = $app[0];
				$app_commande = $app[1];
				$app_facture = $app[2];
			}

?>
			
		<form enctype="multipart/form-data" action="req_modif_param.php" method="post">
		<input type="hidden" name="ref" id="ref" value="<?php echo $ref;?>" />
		
			<fieldset>
				
				<legend class="lg"> Param&egrave;tres en-t&ecirc;te : </legend>
				
				<p><label class="gauche" for="nom_entreprise">Raison sociale et nom de l'entreprise :</label>
				<input class="droit" id="nom_entreprise" type="text" name="nom_entreprise" value="<?php echo $nom_entreprise;?>"/></p>
				
				<p><label class="gauche" for="add_entreprise">Adresse :</label>
				<input class="droit" id="add_entreprise" type="text" name="add_entreprise" value="<?php echo $add_entreprise;?>"/></p>
				
				<p><label class="gauche" for="cp_ville">Code Postal et ville :</label>
				<input class="droit" id="cp_ville" type="text" name="cp_ville" value="<?php echo $cp_ville;?>"/></p>
				
				<p><label class="gauche" for="tel">Telephone :</label>
				<input class="droit" id="tel" type="text" name="tel" value="<?php echo $tel;?>"/></p>
				
				<p><label class="gauche" for="lieu">Lieu d'ex&eacute;cution des travaux :</label>
				<input class="droit" id="lieu" type="text" name="lieu" value="<?php echo $lieu;?>"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA Intra :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra" value="<?php echo $TVA_intra;?>"/></p>
				
				<p><label class="gauche" for="logo_param">Choisir un logo (taille max : 160px par 160px, aucun caract&egrave;res sp&eacute;ciaux) :</label>
				
<?php
				if ($logo_param!="")
					{
					echo '<input class="droit" type="file" id="logo_param" name="logo_param"/></p>
					<p class="cen">Image actuelle :<strong>'.$logo_param.'</strong></p>
					<p class="cen"><img src="Images/'.$logo_param.'"/></p>
					<input type="hidden" name="anc_img" value="'.$logo_param.'"/>';
					}
				else
					{
					echo '<input class="droit" type="file" id="logo_param" name="logo_param"/></p>';
					}
?>
		
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Param&egrave;tres pied de page : </legend>
				
				<p><label class="gauche" for="cond_devis">Conditions de r&egrave;glement, mentions, infos associ&eacute;es (devis) :</label>
				<textarea class="droit" id="cond_devis" name="cond_devis"><?php echo $cond_devis;?></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="cond_commande">Conditions de r&egrave;glement, mentions, infos associ&eacute;es (commande) :</label>
				<textarea class="droit" id="cond_commande" name="cond_commande"><?php echo $cond_commande;?></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="cond_facture">Conditions de r&egrave;glement, mentions, infos associ&eacute;es (facture) :</label>
				<textarea class="droit" id="cond_facture" name="cond_facture"><?php echo $cond_facture;?></textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="mentions">Mentions autre :</label>
				<input class="droit" id="mentions" type="text" name="mentions" value="<?php echo $mentions;?>"/></p>	
		
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Cordonn&eacute;es bancaires: </legend>
				
				<p><label class="gauche" for="coord_banque">Banque :</label>
				<input class="droit" id="coord_banque" type="text" name="coord_banque" value="<?php echo $coord_banque;?>"/></p>
				
				<p><label class="gauche" for="coord_RIB">RIB :</label>
				<input class="droit" id="coord_RIB" type="text" name="coord_RIB" value="<?php echo $coord_RIB;?>"/></p>
				
				<p><label class="gauche" for="coord_IBAN">IBAN :</label>
				<input class="droit" id="coord_IBAN" type="text" name="coord_IBAN" value="<?php echo $coord_IBAN;?>"/></p>
				
				<p>Faire appara&icirc;tre ces coordonn&eacute;es sur :</p>
				
				<p class="cen">
				
				<?php
					
					if ($app_devis=="AD")
						{
						echo 'Devis ? <input type="checkbox" name="app_devis" checked value="AD"/>';
						}
					else
						{
						echo 'Devis ? <input type="checkbox" name="app_devis" value="AD"/>';
						}
						
					if ($app_commande=="AC")
						{
						echo 'Commande ? <input type="checkbox" name="app_commande" checked value="AC"/>';
						}
					else
						{
						echo 'Commande ? <input type="checkbox" name="app_commande" value="AC"/>';
						}
						
					if ($app_facture=="AF")
						{
						echo 'Facture ? <input type="checkbox" name="app_facture" checked value="AF"/>';
						}
					else
						{
						echo 'Facture ? <input type="checkbox" name="app_facture" value="AF"/>';
						}
					
				?>
				
				</p>
				
			</fieldset>
			
			<fieldset>
				
				<legend class="lg"> Cordonn&eacute;es entreprise: </legend>
				
				<p><label class="gauche" for="clause">Clause de r&eacute;serve de propri&eacute;t&eacute; :</label>
				<input class="droit" id="clause" type="text" name="clause" value="<?php echo $clause;?>"/></p>
				
				<p><label class="gauche" for="capital">Capital social :</label>
				<input class="droit" id="capital" type="text" name="capital" value="<?php echo $capital;?>"/></p>
				
				<p><label class="gauche" for="SIREN">SIREN :</label>
				<input class="droit" id="SIREN" type="text" name="SIREN" value="<?php echo $SIREN;?>"/></p>
				
				<p><label class="gauche" for="RCS">RCS :</label>
				<input class="droit" id="RCS" type="text" name="RCS" value="<?php echo $RCS;?>"/></p>
				
				<p><label class="gauche" for="APE">Code APE :</label>
				<input class="droit" id="APE" type="text" name="APE" value="<?php echo $APE;?>"/></p>
				
				<p><label class="gauche" for="info_autre">Information autre :</label>
				<input class="droit" id="info_autre" type="text" name="info_autre" value="<?php echo $info_autre;?>"/></p>
				
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p><label class="gauche" for="nom_param">Nom des param&egrave;tres :</label>
				<input class="droit" id="nom_param" type="text" name="nom_param" value="<?php echo $nom_param;?>"/></p>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Effacer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_modif_param.php">Revenir &agrave; la liste de modification des param&egrave;tres</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>