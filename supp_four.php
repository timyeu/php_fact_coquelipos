<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Suppression fournisseur</h2>
			
				<p>
				Ce formulaire vous permet de <strong>supprimer un fournisseur existant</strong>.
				</p>
				
			</div>

<?php
	
	if (isset($_POST['ListFour'])) $ListFour=$_POST['ListFour'];
		else $ListFour="";
		
	if (isset($_GET['ref'])) $ListFour=$_GET['ref'];
		else $ListFour=$ListFour;
		
	$Requete = "SELECT * FROM $db_fournisseurs WHERE ref_fournisseur ='$ListFour'";
		
		if (empty($ListFour))
			{
				echo "Il faut selectionner un fournisseur dans la liste";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');
					
				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
				
				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref_fournisseur=$Donnees["ref_fournisseur"];
				$nom=$Donnees["nom"];
				$adresse=$Donnees["adresse"];
				$ville=$Donnees["ville"];
				$code_postal=$Donnees["code_postal"];
				$telephone=$Donnees["telephone"];
				$fax=$Donnees["fax"];
				$email=$Donnees["email"];
				$pays=$Donnees["pays"];
				$site_web=$Donnees["site_web"];
				$nom_contact=$Donnees["nom_contact"];
				$tel_contact=$Donnees["tel_contact"];
				$siret=$Donnees["siret"];
				$TVA_intra=$Donnees["TVA_intra"];
				$prod_prop=$Donnees["prod_prop"];
				$memo=$Donnees["memo"];
			}

?>
			
		<form action="req_supp_four.php" method="post">
		<input type="hidden" name="ref_fournisseur" id="ref_fournisseur" value="<?php echo $ref_fournisseur;?>" />
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif fournisseurs : </legend>
				
				<p>Nom : <strong><?php echo $nom;?></strong></p>
				
				<p>Adresse : <strong><?php echo $adresse;?></strong></p>
				
				<p>Ville : <strong><?php echo $ville;?></strong></p>
				
				<p>Code postal : <strong><?php echo $code_postal;?></strong></p>
				
				<p>Telephone : <strong><?php echo $telephone;?></strong></p>
				
				<p>Fax : <strong><?php echo $fax;?></strong></p>
				
				<p>E-mail : <strong><?php echo $email;?></strong></p>
				
				<p>Pays : <strong><?php echo $pays;?></strong></p>
				
				<p>Site web : <strong><?php echo $site_web;?></strong></p>
				
				<p>Nom contact : <strong><?php echo $nom_contact;?></strong></p>
				
				<p>Telephone : <strong><?php echo $tel_contact;?></strong></p>
				
				<p>Siret : <strong><?php echo $siret;?></strong></p>
				
				<p>TVA intra-communautaire : <strong><?php echo $TVA_intra;?></strong></p>
				
				<p>Produits propos&eacute;s : <strong><?php echo $prod_prop;?></strong></p>
				
				<p>Memo : <strong><?php echo $memo;?></strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Supprimer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_supp_four.php">Revenir &agrave; la liste de suppression des fournisseurs</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>	