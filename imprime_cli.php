<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
?>

<!DOCTYPE html>

<html lang="fr">

	<head>
		<title>Coquelipos Fiche Client</title>
		<link rel="stylesheet" type="text/css" title= "design" href="Style/Main.css" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex,nofollow" />
		
		<script type="text/javascript">
			window.print();
		</script>
	</head>
	
	<body style="margin: 0; padding: 0; background: #FFFFFF;">

	<div id="feuille_imp">

<?php

	if(isset($_POST['ref_clients']))      $ref_clients=$_POST['ref_clients'];
	else      $ref_clients="";
		
	$Requete = "SELECT * FROM $db_compte_client WHERE ref_clients ='$ref_clients'";

		$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
		mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

		$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 

			$Donnees = mysqli_fetch_array($ResReq);
		
			$nom=$Donnees["nom"];
			$contact=$Donnees["contact"];
			$fonc_contact=$Donnees["fonc_contact"];
			$contact_autre=$Donnees["contact_autre"];
			$fonc_contact_autre=$Donnees["fonc_contact_autre"];
			$nature=$Donnees["nature"];
			$adresse=$Donnees["adresse"];
			$ville=$Donnees["ville"];
			$code_postal=$Donnees["code_postal"];
			$tel=$Donnees["tel"];
			$tel_portable=$Donnees["tel_portable"];
			$fax=$Donnees["fax"];
			$mail=$Donnees["mail"];
			$activite=$Donnees["activite"];
			$site_web=$Donnees["site_web"];
			$siret=$Donnees["siret"];
			$TVA_intra=$Donnees["TVA_intra"];
			$encours=$Donnees["encours"];
			$liste_commandes=$Donnees["liste_commandes"];
			$nb_facture=$Donnees["nb_facture"];
			$date_anniv=$Donnees["date_anniv"];
			$date_autre=$Donnees["date_autre"];
			$notes=$Donnees["notes"];
									
?>
	
		<table style="width: 70%; border: 2px solid #B01807;">
			
			<tr>
				<td style="background: #F14835;"><p class="imp_5">Fiche client</p></td>
			</tr>
			
		</table>
		
		<table style="width: 70%; border: 2px solid #396208;">
		
			<tr>	
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Nom</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $nom;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Nature</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $nature;?></p></td>
			</tr>
			
		</table>	
<?php
	if ($nature=="professionnel")
		{		
?>
		<table style="width: 70%; border: 2px solid #396208;">
		
			<tr>
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Nom contact</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $contact;?></p></td>
			</tr>
			<tr>
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Fonction contact</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $fonc_contact;?></p></td>
			</tr>
			<tr>
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Nom autre contact</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $contact_autre;?></p></td>
			</tr>
			<tr>
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Fonction autre contact</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $fonc_contact_autre;?></p></td>
			</tr>
			
		</table>	
<?php
		}		
?>
		<table style="width: 70%; border: 2px solid #396208;">
		
			<tr>	
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Adresse</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $adresse;?></p></td>
			</tr>
			<tr>
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Ville</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $ville;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Code postal</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $code_postal;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">T&eacute;lephone</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $tel;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">T&eacute;lephone portable</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $tel_portable;?></p></td>
			</tr>
<?php
	if ($nature=="professionnel")
		{		
?>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Fax</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $fax;?></p></td>
			</tr>
<?php
		}		
?>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Adresse mail</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $mail;?></p></td>
			</tr>
			
		</table>

		<table style="width: 70%; border: 2px solid #396208;">
<?php
	if ($nature=="professionnel")
		{	
?>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Activit&eacute;</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $activite;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Site web</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $site_web;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Siret</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $siret;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">TVA intra-communautaire</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $TVA_intra;?></p></td>
			</tr>	
<?php
		}	
?>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Encours</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $encours;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Liste commandes</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $liste_commandes;?></p></td>
			</tr>
			
		</table>

		<table style="width: 70%; border: 2px solid #396208;">
		
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Nombre factures</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $nb_facture;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Date anniversaire</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $date_anniv;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Date autre</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $date_autre;?></p></td>
			</tr>
			<tr>		
				<td style="width: 30%; border: solid 1px #396208; background: #89EC14;"><p class="imp_6">Notes</p></td>
				<td style="width: 70%; border: solid 1px #396208;"><p class="imp_4"><?php echo $notes;?></p></td>
			</tr>
			
		</table>
	
	</div>
	
	</body>

</html>

<?php
		}
require_once 'Main_ft.php'; 
?>