<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Suppression produit/prestation</h2>
			
				<p>
				Ce formulaire vous permet de <strong>supprimer un produit ou une prestation existant(e)</strong>.
				</p>
				
			</div>

<?php
	
	if (isset($_POST['ListProd'])) $ListProd=$_POST['ListProd'];
		else $ListProd="";
		
	if (isset($_GET['ref'])) $ListProd=$_GET['ref'];
		else $ListProd=$ListProd;
		
	$Requete = "SELECT * FROM $db_prod_prest WHERE ref_produits ='$ListProd'";

		if (empty($ListProd))
			{
				echo "Il faut selectionner un article dans la liste";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, article incorrect ou inexistant</span>'); 

				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref_produits=$Donnees["ref_produits"];
				$designation=$Donnees["designation"];
				$reference=$Donnees["reference"];
				$informations=$Donnees["informations"];
				$nature=$Donnees["nature"];
				$quantite=$Donnees["quantite"];
				$qte_limite=$Donnees["qte_limite"];
				$prix_achat=$Donnees["prix_achat"];
				$prix_vente=$Donnees["prix_vente"];
				$coef=$Donnees["coef"];
				$taux_TVA=$Donnees["taux_TVA"];
				$remise=$Donnees["remise"];
				$prix_TTC=$Donnees["prix_TTC"];
				$ref_fournisseur=$Donnees["ref_fournisseur"];
				$ref_prod_fournisseur=$Donnees["ref_prod_fournisseur"];
				$qte_limite=$Donnees["qte_limite"];
				$qte_vendu=$Donnees["qte_vendu"];
			}
			
	$Requete2 = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur ='$ref_fournisseur'";
	$ResReq2 = mysqli_query($db, $Requete2) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
	$Donnees2 = mysqli_fetch_array($ResReq2);
	$nom_fournisseur=$Donnees2["nom"];
	
	if (substr($remise, -1)=="%")
		{
		$prix_net=$prix_TTC-($prix_TTC*(substr($remise, 0, -1)/100));
		}
	else
		{
		$prix_net=$prix_TTC-$remise;
		}

?>
			
		<form action="req_supp_prod.php" method="post">
		<input type="hidden" name="ref_produits" id="ref_produits" value="<?php echo $ref_produits;?>" />
		
			<fieldset>
				
				<legend class="lg"> R&eacute;capitulatif produit/prestation : </legend>
				
				<p>D&eacute;signation : <strong><?php echo $designation;?></strong></p>
				
				<p>Nature : <strong><?php echo $nature;?></strong></p>
				
				<p>R&eacute;f&eacute;rence : <strong><?php echo $reference;?></strong></p>
				
				<p>Informations : <strong><?php echo $informations;?></strong></p>
				
				<p>Quantit&eacute; : <strong><?php echo $quantite;?></strong></p>
				
				<p>Quantit&eacute; limite : <strong><?php echo $qte_limite;?></strong></p>
				
				<p>Prix d'achat : <strong><?php echo $prix_achat;?> &euro;</strong></p>
				
				<p>Coefficient : <strong><?php echo $coef;?></strong></p>
				
				<p>Prix hors taxes : <strong><?php echo $prix_vente;?> &euro;</strong></p>
				
				<p>Taux TVA : <strong><?php echo $taux_TVA;?>%</strong></p>
				
				<p>Remise : <strong><?php echo $remise;?></strong></p>
				
				<p>Fournisseur : <strong><?php echo $nom_fournisseur;?></strong></p>
				
				<p>R&eacute;f&eacute;rence fournisseur : <strong><?php echo $ref_prod_fournisseur;?></strong></p>
				
				<p>Prix TTC : <strong><?php echo $prix_TTC;?> &euro;</strong></p>
				
				<p>Prix net : <strong><?php echo $prix_net;?> &euro;</strong></p>
		
			</fieldset>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Supprimer"/>
				</p>
				
			</fieldset>
			
		</form>
		
		<p class="cen"><a href="liste_supp_prod.php">Revenir &agrave; la liste de suppression des produit/prestation</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>