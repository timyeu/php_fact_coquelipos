<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Gestion des param&egrave;tres</h2>
			
				<p>
				Choix de l'option de <strong>gestion des param&egrave;tres</strong>.
				</p>
				
			</div>

			<form action="cons_prod.php" method="post">
			
			<fieldset>
				
				<legend> Filtres : </legend>
				
				<p class="cen"><a href="crea_param.php">Cr&eacute;ation de param&egrave;tres</a></p>
				<p class="cen"><a href="liste_modif_param.php">Modification de param&egrave;tres</a></p>
				<p class="cen"><a href="liste_supp_param.php">Suppression de param&egrave;tres</a></p>
				
			</fieldset>
				
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Valider"/>
					<input type="reset" value="Annuler"/>
				</p>
				
			</fieldset>
			
			</form>
			
			<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
				
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>