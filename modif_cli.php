<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc" class="edit-form">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation client</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier un client existant</strong>.
				</p>
				
			</div>

<?php

	if (isset($_POST['ListCli'])) $ListCli=$_POST['ListCli'];
		else $ListCli="";
		
	if (isset($_GET['ref'])) $ListCli=$_GET['ref'];
		else $ListCli=$ListCli;
		
	if (isset($_POST['cli_agend'])) $ListCli=$_POST['cli_agend'];
		else $ListCli=$ListCli;
		
	if (isset($_GET['cli_agend'])) $ListCli=$_GET['cli_agend'];
		else $ListCli=$ListCli;
		
	$Requete = "SELECT * FROM $db_compte_client WHERE ref_clients ='$ListCli'";

		if (empty($ListCli))
			{
				echo "Il faut selectionner un client dans la liste";
			}
			
			else
			{
				$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
				mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

				$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 

				$Donnees = mysqli_fetch_array($ResReq);
		
				$ref_clients=$Donnees["ref_clients"];
				$nom=$Donnees["nom"];
				$contact=$Donnees["contact"];
				$fonc_contact=$Donnees["fonc_contact"];
				$contact_autre=$Donnees["contact_autre"];
				$fonc_contact_autre=$Donnees["fonc_contact_autre"];
				$nature=$Donnees["nature"];
				$adresse=$Donnees["adresse"];
				$ville=$Donnees["ville"];
				$code_postal=$Donnees["code_postal"];
				$tel=$Donnees["tel"];
				$tel_portable=$Donnees["tel_portable"];
				$fax=$Donnees["fax"];
				$mail=$Donnees["mail"];
				$activite=$Donnees["activite"];
				$site_web=$Donnees["site_web"];
				$siret=$Donnees["siret"];
				$TVA_intra=$Donnees["TVA_intra"];
				$encours=$Donnees["encours"];
				$liste_commandes=$Donnees["liste_commandes"];
				$nb_facture=$Donnees["nb_facture"];
				$date_anniv=$Donnees["date_anniv"];
				$date_autre=$Donnees["date_autre"];
				$notes=$Donnees["notes"];
			}
			
	$Requete_agend = "SELECT * FROM $db_agenda WHERE ListCli ='$ref_clients' AND nom_membre ='$nom_membre' ORDER BY STR_TO_DATE(date_complete, '%d/%m/%Y') DESC";
	$ResReq_agend = mysqli_query($db, $Requete_agend) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
	$nbenreg_agend = mysqli_num_rows($ResReq_agend);
	$nbchamps_agend = mysqli_num_fields($ResReq_agend);
	$Tabdo_agend[$nbenreg_agend][$nbchamps_agend]="";
	$Tabchamps_agend[$nbchamps_agend]="";

			for($I=0; $I < $nbchamps_agend; $I++) 
				{
				$tabchamps_agend[$I] = mysqli_fetch_field_direct($ResReq_agend,$I);
				}

			$I=0;
			
			while ($donnees_agend = mysqli_fetch_array($ResReq_agend))
				{	
				$Tabdo_agend[$I][1]=$donnees_agend["ref"];
				$Tabdo_agend[$I][2]=$donnees_agend["nom_membre"];
				$Tabdo_agend[$I][3]=$donnees_agend["date_complete"];
				$Tabdo_agend[$I][4]=$donnees_agend["intit_action"];
				$Tabdo_agend[$I][5]=$donnees_agend["horaire_deb"];
				$Tabdo_agend[$I][6]=$donnees_agend["horaire_fin"];
				$Tabdo_agend[$I][7]=$donnees_agend["action"];
				$Tabdo_agend[$I][8]=$donnees_agend["detail_agend"];
				$I++;
				}

?>
			
		<form action="req_modif_cli.php" method="post">
		<input type="hidden" name="ref_clients" id="ref_clients" value="<?php echo $ref_clients;?>" />

<?php
		if ($nature=="particulier")
			{
			echo '<input type="hidden" name="fonc_contact" value="'.$fonc_contact.'"/>';
			echo '<input type="hidden" name="contact_autre" value="'.$contact_autre.'"/>';
			echo '<input type="hidden" name="fonc_contact_autre" value="'.$fonc_contact_autre.'"/>';
			echo '<fieldset>
				
				<legend class="lg"> Detail client : </legend>
				
				<p><label class="gauche" for="nom">Nom :</label>
				<input class="droit" id="nom" type="text" name="nom" value="'.$nom.'"/></p>
				
				<p><label class="gauche" for="contact">Nom contact :</label>
				<input class="droit" id="contact" type="text" name="contact" value="'.$contact.'"/></p>
				
				<p><label class="gauche" for="nature">Nature :</label>
				<select id="nature" class="droit" name="nature">
					<option value="'.$nature.'">'.$nature.'</option>
					<option value="particulier">particulier</option>
					<option value="professionnel">professionnel</option>
				</select></p>
				
				<p><label class="gauche" for="adresse">Adresse :</label>
				<input class="droit" id="adresse" type="text" name="adresse" value="'.$adresse.'"/></p>
				
				<p><label class="gauche" for="ville">Ville :</label>
				<input class="droit" id="ville" type="text" name="ville" value="'.$ville.'"/></p>
				
				<p><label class="gauche" for="code_postal">Code postal :</label>
				<input class="droit" id="code_postal" type="text" name="code_postal" value="'.$code_postal.'"/></p>
				
				<p><label class="gauche" for="tel">Telephone :</label>
				<input class="droit" id="tel" type="text" name="tel" value="'.$tel.'"/></p>
				
				<p><label class="gauche" for="tel_portable">Telephone portable :</label>
				<input class="droit" id="tel_portable" type="text" name="tel_portable" value="'.$tel_portable.'"/></p>

				<p><label class="gauche" for="fax">Fax :</label>
				<input class="droit" id="fax" type="text" name="fax" value="'.$fax.'"/></p>
				
				<p><label class="gauche" for="mail">E-mail :</label>
				<input class="droit" id="mail" type="text" name="mail" value="'.$mail.'"/></p>
				
				<p><label class="gauche" for="activite">Activite :</label>
				<input class="droit" id="activite" type="text" name="activite" value="'.$activite.'"/></p>
				
				<p><label class="gauche" for="site_web">Site web :</label>
				<input class="droit" id="site_web" type="text" name="site_web" value="'.$site_web.'"/></p>
				
				<p><label class="gauche" for="siret">Siret :</label>
				<input class="droit" id="siret" type="text" name="siret" value="'.$siret.'"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA intra-communautaire :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra" value="'.$TVA_intra.'"/></p>
				
				<p><label class="gauche" for="encours">Encours :</label>
				<input class="droit" id="encours" type="text" name="encours" value="'.$encours.'"/></p>
				
				<p><label class="gauche" for="liste_commandes">Liste commandes :</label>
				<textarea class="droit" id="liste_commandes" name="liste_commandes">'.$liste_commandes.'</textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="date_anniv">Date anniversaire :</label>
				<input class="droit" id="date_anniv" type="text" name="date_anniv" value="'.$date_anniv.'"/></p>
				
				<p><label class="gauche" for="date_autre">Date autre :</label>
				<input class="droit" id="date_autre" type="text" name="date_autre" value="'.$date_autre.'"/></p>
				
				<p><label class="gauche" for="notes">Notes :</label>
				<textarea class="droit" id="notes" name="notes">'.$notes.'</textarea></p>
		
			</fieldset>';
			}
		else
			{
			echo '<fieldset>
				
				<legend class="lg"> Detail client : </legend>
				
				<p><label class="gauche" for="nom">Nom (raison sociale si professionnel):</label>
				<input class="droit" id="nom" type="text" name="nom" value="'.$nom.'"/></p>
				
				<p><label class="gauche" for="contact">Nom contact principal (pr&eacute;ciser civilit&eacute;) :</label>
				<input class="droit" id="contact" type="text" name="contact" value="'.$contact.'"/></p>
				
				<p><label class="gauche" for="fonc_contact">Fonction contact principal :</label>
				<input class="droit" id="fonc_contact" type="text" name="fonc_contact" value="'.$fonc_contact.'"/></p>
				
				<p class="cen"><strong>Le contact principal appara&icirc;tra sur les documents si pr&eacute;cis&eacute;</strong></p>
				
				<p><label class="gauche" for="contact_autre">Nom contact autre (pr&eacute;ciser civilit&eacute;) :</label>
				<input class="droit" id="contact_autre" type="text" name="contact_autre" value="'.$contact_autre.'"/></p>
				
				<p><label class="gauche" for="fonc_contact_autre">Fonction contact autre :</label>
				<input class="droit" id="fonc_contact_autre" type="text" name="fonc_contact_autre" value="'.$fonc_contact_autre.'"/></p>
				
				<p><label class="gauche" for="nature">Nature :</label>
				<select id="nature" class="droit" name="nature">
					<option value="'.$nature.'">'.$nature.'</option>
					<option value="particulier">particulier</option>
					<option value="professionnel">professionnel</option>
				</select></p>
				
				<p><label class="gauche" for="adresse">Adresse :</label>
				<input class="droit" id="adresse" type="text" name="adresse" value="'.$adresse.'"/></p>
				
				<p><label class="gauche" for="ville">Ville :</label>
				<input class="droit" id="ville" type="text" name="ville" value="'.$ville.'"/></p>
				
				<p><label class="gauche" for="code_postal">Code postal :</label>
				<input class="droit" id="code_postal" type="text" name="code_postal" value="'.$code_postal.'"/></p>
				
				<p><label class="gauche" for="tel">Telephone :</label>
				<input class="droit" id="tel" type="text" name="tel" value="'.$tel.'"/></p>
				
				<p><label class="gauche" for="tel_portable">Telephone portable :</label>
				<input class="droit" id="tel_portable" type="text" name="tel_portable" value="'.$tel_portable.'"/></p>

				<p><label class="gauche" for="fax">Fax :</label>
				<input class="droit" id="fax" type="text" name="fax" value="'.$fax.'"/></p>
				
				<p><label class="gauche" for="mail">E-mail :</label>
				<input class="droit" id="mail" type="text" name="mail" value="'.$mail.'"/></p>
				
				<p><label class="gauche" for="activite">Activite :</label>
				<input class="droit" id="activite" type="text" name="activite" value="'.$activite.'"/></p>
				
				<p><label class="gauche" for="site_web">Site web :</label>
				<input class="droit" id="site_web" type="text" name="site_web" value="'.$site_web.'"/></p>
				
				<p><label class="gauche" for="siret">Siret :</label>
				<input class="droit" id="siret" type="text" name="siret" value="'.$siret.'"/></p>
				
				<p><label class="gauche" for="TVA_intra">TVA intra-communautaire :</label>
				<input class="droit" id="TVA_intra" type="text" name="TVA_intra" value="'.$TVA_intra.'"/></p>
				
				<p><label class="gauche" for="encours">Encours :</label>
				<input class="droit" id="encours" type="text" name="encours" value="'.$encours.'"/></p>
				
				<p><label class="gauche" for="liste_commandes">Liste commandes :</label>
				<textarea class="droit" id="liste_commandes" name="liste_commandes">'.$liste_commandes.'</textarea></p>
				
				<br /><br /><br /><br />
				
				<p><label class="gauche" for="date_anniv">Date anniversaire :</label>
				<input class="droit" id="date_anniv" type="text" name="date_anniv" value="'.$date_anniv.'"/></p>
				
				<p><label class="gauche" for="date_autre">Date autre :</label>
				<input class="droit" id="date_autre" type="text" name="date_autre" value="'.$date_autre.'"/></p>
				
				<p><label class="gauche" for="notes">Notes :</label>
				<textarea class="droit" id="notes" name="notes">'.$notes.'</textarea></p>
		
			</fieldset>';
			}
?>
			
			<fieldset>
			
				<legend> Validation : </legend>
				
				<p class="cen">
					<input type="submit" value="Modifier"/>
					<input type="reset" value="R&eacute;initialiser"/>
				</p>
				
			</fieldset>
			
		</form>
		
			<fieldset>
			
				<legend> Agenda relatif : </legend>
				
				<table>
					
					<tr>
				
						<th>Date</th>
						<th>Intitul&eacute;</th>
						<th>Horaire</th>
						<th>Concernant</th>
						<th>D&eacute;tail</th>
							
					</tr>

	<?php
				for($I=0; $I < $nbenreg_agend; $I++) 
					{
					echo '<tr>
						<td>
							<p><strong>'.$Tabdo_agend[$I][3].'</strong></p>
							<p>
								<form action="req_supp_agenda.php" method="post">
								<input type="hidden" name="ref" value="'.$Tabdo_agend[$I][1].'"/>
								<input type="hidden" name="agend_cli" value="'.$ref_clients.'"/>
								<input type="submit" value="Sup."/></form>
							</p></td>';
					echo '<td>'.$Tabdo_agend[$I][4].'</td>';
						if ($Tabdo_agend[$I][5]!="")
							{
							if ($Tabdo_agend[$I][6]!="")
								{
								echo '<td><p>De <strong>'.$Tabdo_agend[$I][5].'</strong></p>
								<p>Heure fin : <strong>'.$Tabdo_agend[$I][6].'</strong></p></td>';
								}
							else
								{
								echo '<td><p>&Agrave; : <strong>'.$Tabdo_agend[$I][5].'</strong></p></td>';
								}
							}
						else
							{
							echo '<td></td>';
							}
						echo '<td><p>'.$Tabdo_agend[$I][7].'</p>
						<td style="width:50%;">'.$Tabdo_agend[$I][8].'</td>
					</tr>';
					}
	?>
						
				</table>
			
			</fieldset>
			
		<fieldset>
		
			<p class="cen">
			Nous sommes le <strong><?php echo date('d/m/Y');?></strong>.
			</p>
			
			<legend> Calendrier : </legend>
				
<?php

	$Requete_calend = "SELECT * FROM $db_agenda WHERE nom_membre='$nom_membre'";
	$ResReq_calend = mysqli_query($db, $Requete_calend) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>');
	$nbenreg_calend = mysqli_num_rows($ResReq_calend);
	$nbchamps_calend = mysqli_num_fields($ResReq_calend);
	$Tabdo_calend[$nbenreg_calend][$nbchamps_calend]="";
	$Tabchamps_calend[$nbchamps_calend]="";
	
	for($M=0; $M < $nbchamps_calend; $M++) 
		{
		$tabchamps_calend[$M] = mysqli_fetch_field_direct($ResReq_calend,$M);
		}
	
	$M=0;
			
	while ($donnees_calend = mysqli_fetch_array($ResReq_calend))
		{	
		$Tabdo_calend[$M][1]=$donnees_calend["ref"];
		$Tabdo_calend[$M][2]=$donnees_calend["nom_membre"];
		$Tabdo_calend[$M][3]=$donnees_calend["date_complete"];
		$Tabdo_calend[$M][4]=$donnees_calend["intit_action"];
		$Tabdo_calend[$M][5]=$donnees_calend["horaire_deb"];
		$Tabdo_calend[$M][6]=$donnees_calend["horaire_fin"];
		$Tabdo_calend[$M][7]=$donnees_calend["action"];
		$Tabdo_calend[$M][8]=$donnees_calend["ListCli"];
		$Tabdo_calend[$M][9]=$donnees_calend["ListFour"];
		$Tabdo_calend[$M][10]=$donnees_calend["ListProd"];
		$Tabdo_calend[$M][11]=$donnees_calend["ListPrest"];
		$Tabdo_calend[$M][12]=$donnees_calend["detail_agend"];
		$M++;
		}
		
	$m = (isset($_GET['m'])) ? $_GET['m'] : date("n");
	$a = (isset($_GET['a'])) ? $_GET['a'] : date("Y");
	$mnom = Array("","Janvier","Fevrier","Mars" ,"Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");
	$dayone = date("w",mktime(1,1,1,$m,1,$a));
	$daytod = date('d');
	$an=$a;
	$mois=$m;
	$m_c=$m;
	$a_c=$a;
	$a_trce=0;
	if ($m_c<10)
		{
		$m_c='0'.$m_c;
		}
	
	if ($dayone==0) 
		{
		$dayone=7;
		}
	$url = "modif_cli.php";
	
	for($i=1;$i<13;$i++)
		{   
		echo '<a href='.$url.'?m='.$i.'&a='.$a.'><input type="button" style="width: 8.3%;" value="'.$mnom[$i].'"/></a>';
		}
		
	$mois=$m-1;
	$an=$a;

	if($mois==0) 
		{
		$mois=12;
		$an=$a-1;
		}
	
	echo '<div id="NavBar">
			<table>
				<tr>
					<td><a href='.$url.'?m='.$mois.'&a='.$an.'&cli_agend='.$ref_clients.'><input type="button" value="<-- Pr&eacute;c"/></a></td>
					<td>'.$mnom[$m].' '.$a.'</td>';
	
	$an=$a;
	$mois=$m+1;
	
	if ($mois==13) 
		{
		$mois=1;
		$an=$a+1;
		}
		
	$jours_in_month=cal_days_in_month(CAL_GREGORIAN,$m,$a);
	$gg=$jours_in_month+$dayone-1;
	$nb_semaine=ceil($gg/7);
	$jours_a_afficher=$nb_semaine*7;
	
	echo '<td><a href='.$url.'?m='.$mois.'&a='.$an.'&cli_agend='.$ref_clients.'><input type="button" value="Suiv -->"/></a></td>
		</tr>
	</table>';
	
	if ($mois<10)
		{
		$mois='0'.$mois;
		}
	
	echo '<table>
			<tr>
				<th style="width:70px;">L</th>
				<th style="width:70px;">M</th>
				<th style="width:70px;">M</th>
				<th style="width:70px;">J</th>
				<th style="width:70px;">V</th>
				<th style="width:70px;">S</th>
				<th style="width:70px;">D</th>';
	for ($n=1;$n<=$jours_a_afficher;$n++)
		{
		echo '<form action="agenda.php" method="post">';
		if ($n%7 == 1)   
			{
			echo'</tr><tr>';
			}
			
		if ($n<($jours_in_month+$dayone) && $n>=$dayone)
			{		
			$a=$n-$dayone+1;
			if ($a<10)
				{
				$a='0'.$a;
				}
			$date_verif=$a.'/'.$m_c.'/'.$a_c;

			for($M=0; $M < $nbenreg_calend; $M++) 
				{
				if ($Tabdo_calend[$M][3]==$date_verif)
					{
					echo '<input type="hidden" name="annee_agend" value="'.$a_c.'"/>
					<input type="hidden" name="mois_agend" value="'.$m_c.'"/>
					<td class="cald"><input type="submit" class="calend" name="date_agend" value="'.$a.'"/>
					</br></br>De <strong>'.$Tabdo_calend[$M][5].'</strong> &agrave; <strong>'.$Tabdo_calend[$M][6].'</strong>
					</br></br><strong>'.$Tabdo_calend[$M][4].'</strong>
					</br>'.$Tabdo_calend[$M][7];
					if ($Tabdo_calend[$M][8]!="")
						{
						$Requete_calend_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='".$Tabdo_calend[$M][8]."'";
						$ResReq_calend_cli = mysqli_query($db, $Requete_calend_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>'); 
						$Donnees_calend_cli = mysqli_fetch_array($ResReq_calend_cli);
						$nom_calend_cli=$Donnees_calend_cli["nom"];
						echo '</br></br>Client : <strong>'.$nom_calend_cli.'</strong>';
						}
					if ($Tabdo_calend[$M][9]!="")
						{
						$Requete_calend_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='".$Tabdo_calend[$M][9]."'";
						$ResReq_calend_four = mysqli_query($db, $Requete_calend_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>'); 
						$Donnees_calend_four = mysqli_fetch_array($ResReq_calend_four);
						$nom_calend_four=$Donnees_calend_four["nom"];
						echo '</br></br>Fournisseur : <strong>'.$nom_calend_four.'</strong>';
						}
					if ($Tabdo_calend[$M][10]!="")
						{
						$Requete_calend_prod = "SELECT designation FROM $db_prod_prest WHERE ref_produits='".$Tabdo_calend[$M][10]."'";
						$ResReq_calend_prod = mysqli_query($db, $Requete_calend_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>'); 
						$Donnees_calend_prod = mysqli_fetch_array($ResReq_calend_prod);
						$nom_calend_prod=$Donnees_calend_prod["designation"];
						echo '</br></br>Produit : <strong>'.$nom_calend_prod.'</strong>';
						}
					if ($Tabdo_calend[$M][11]!="")
						{
						$Requete_calend_prest = "SELECT designation FROM $db_prod_prest WHERE ref_produits='".$Tabdo_calend[$M][11]."'";
						$ResReq_calend_prest = mysqli_query($db, $Requete_calend_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>'); 
						$Donnees_calend_prest = mysqli_fetch_array($ResReq_calend_prest);
						$nom_calend_prest=$Donnees_calend_prest["designation"];
						echo '</br></br>Prestation : <strong>'.$nom_calend_prest.'</strong>';
						}
					echo '</br><em>'.$Tabdo_calend[$M][12].'</em>
					</td>';
					$a_trce=$a;
					}
				}
			if ($a_trce!=$a)
				{
				echo '<input type="hidden" name="annee_agend" value="'.$a_c.'"/>
				<input type="hidden" name="mois_agend" value="'.$m_c.'"/>
				<td class="cald2"><input type="submit" name="date_agend" value="'.$a.'"/></td>';
				}
			}
		else
			{
			echo '<td style="width:70px; height: 100px; background: #ECECEC;"></td>';
			}
		echo '<input type="hidden" name="cli_agend" value="'.$ref_clients.'"/>';
		echo '</form>';
		}
	echo '</tr>
		</table>
</div>';

?>
			
			<p class="cen"><strong>S&eacute;lectionnez une date pour ajouter une information &agrave; votre agenda</strong></p>
			
		</fieldset>
		
		<form action="imprime_cli.php" target="_blank" method="post">
		
		<fieldset>
			
			<legend> Impression fiche client : </legend>
				
			<p class="cen">
				<input type="hidden" name="ref_clients" id="ref_clients" value="<?php echo $ref_clients;?>" />
				<input type="submit" value="Imprimer"/>
			</p>
		
		</fieldset>
		
		</form>
		
		<p class="cen"><a href="liste_modif_cli.php">Revenir &agrave; la liste de modification des clients</a></p>
		
		<p class="cen"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
		
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>