<?php 
 
session_start();
date_default_timezone_set('Europe/Paris');

	if (isset($_SESSION['connect']))
		{
		$connect=$_SESSION['connect'];
		}
	else
		{
		$connect=0;
		}
		
	if (isset($_SESSION['log']))
		{
		$nom_membre=$_SESSION['log'];
		}
	else
		{
		$nom_membre=0;
		}	

include 'ccg_coquelipos_fact.php';

	if ($connect != "1" && $connect != "2")
		{
		header('Location: http://'.$link_domain.'/Accueil.php');
		exit;
		}
	else
		{
		require_once 'Main_hd.php';
?>

	<div id="feuille">
		
		<div id="feuille_bloc">
		
			<div id="feuille_para">
			
			<h2>Modification/Consultation document</h2>
			
				<p>
				Ce formulaire vous permet de <strong>consulter et/ou modifier un document existant</strong>.
				</p>
				
			</div>

<?php

	$db = mysqli_connect($db_server,$db_user,$db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
	mysqli_select_db($db,$db_database)  or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

	if ($connect == "1")
		{
		$Requete = "SELECT * FROM $db_facture_entete LEFT JOIN $db_compte_client ON $db_facture_entete.nom_client=$db_compte_client.ref_clients 
		WHERE etat != 'annulation' AND etat != 'perte' ORDER by num_fact";
		}
	else
		{
		$Requete = "SELECT * FROM $db_facture_entete LEFT JOIN $db_compte_client ON $db_facture_entete.nom_client=$db_compte_client.ref_clients 
		WHERE nom_membre = '$nom_membre' AND etat != 'annulation' AND etat != 'perte' ORDER by num_fact";
		}
		
	$ResReq = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect(e) ou inexistant(e)</span>'); 
	
	
?>
				<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
			    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/css/select2.min.css" rel="stylesheet" />
				<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.min.js"></script>
			    <script src="js/fact.js"></script>
			    <style type="text/css">
			    .select2-container{
			    	margin-left: 15px !important;
			    }
			    </style>

			<form action="modif_fact.php" method="post" id="list_modif_fact">
			
			<fieldset>
				
				<legend> Liste des documents pr&eacute;sent(e)s : </legend>

				<?php
		
				$ld = "<label class='gauche' for='nom_client'>Document type :</label>
				<select class='droit' id='nom_client' name='ListFact'>";
					while ($LigneDo = mysqli_fetch_array($ResReq)) 
						{
						$Nmnum = $LigneDo["num_fact"];
						$Nmtype = $LigneDo["type"];
						$Nmetat = $LigneDo["etat"];
						$nom = $LigneDo["nom"];
						
						$ld .= '<option value="'.$Nmnum.'">'.$Nmtype.' : '.$nom.' - '.$Nmnum.', statut : '.$Nmetat.'</option>';
						// $ld .= '<option value="'.$Nmnum.'">'.$Nmtype.' : '.$Nmnum.', statut : '.$Nmetat.'</option>';
						}
					$ld .= "</select>";
					
					print $ld;
							
				?>
				
				<p class="cen"><strong>Ne sont affich&eacute;s que les documents qui ne sont pas annul&eacute; ni perdus 
				(cas d'un devis dont l'&eacute;ch&eacute;ance est d&eacute;pass&eacute;e)</strong></p>
						
				<p class="cen"><input type="submit" value="Valider"/></p>


			
			</fieldset>
			
			</form>

			<?php
$domain = $_SERVER["SERVER_NAME"];
// echo $domain;
// exit();
?>
<?php

date_default_timezone_set('Europe/Paris');

if (isset($_SESSION['connect'])) {
    $connect = $_SESSION['connect'];
} else {
    $connect = 0;
}

if (isset($_SESSION['log'])) {
    $nom_membre = $_SESSION['log'];
} else {
    $nom_membre = 0;
}

include 'ccg_coquelipos_fact.php';
require_once 'Main_hd.php';

if ($connect != "1" && $connect != "2") {
} else {

    if (isset($_POST['date_deb'])) {
        $date_deb = $_POST['date_deb'];
    } else {
        $date_deb = "";
    }

    if (isset($_GET['date_deb'])) {
        $date_deb = $_GET['date_deb'];
    } else {
        $date_deb = $date_deb;
    }

    if (isset($_POST['date_fin'])) {
        $date_fin = $_POST['date_fin'];
    } else {
        $date_fin = "";
    }

    if (isset($_GET['date_fin'])) {
        $date_fin = $_GET['date_fin'];
    } else {
        $date_fin = $date_fin;
    }

    if (isset($_POST['affdatdeb'])) {
        $affdatdeb = $_POST['affdatdeb'];
    } else {
        $affdatdeb = "";
    }

    if (isset($_POST['affdatfin'])) {
        $affdatfin = $_POST['affdatfin'];
    } else {
        $affdatfin = "";
    }

    if (isset($_POST['num_nouv_cli'])) {
        $num_nouv_cli = $_POST['num_nouv_cli'];
    } else {
        $num_nouv_cli = "3";
    }

    unset($_SESSION['cli_agend']);

    $date_memo  = date('d/m/Y');
    $verif_memo = "";

    if (preg_match('`^[0-9]{2}/[0-9]{2}/[0-9]{4}$`', $date_deb)) {
        $tmp       = explode('/', $date_deb);
        $date_deb  = $tmp[2] . '/' . $tmp[1] . '/' . $tmp[0];
        $affdatdeb = $tmp[0] . '/' . $tmp[1] . '/' . $tmp[2];
    }

    if (preg_match('`^[0-9]{2}/[0-9]{2}/[0-9]{4}$`', $date_fin)) {
        $tmp       = explode('/', $date_fin);
        $date_fin  = $tmp[2] . '/' . $tmp[1] . '/' . $tmp[0];
        $affdatfin = $tmp[0] . '/' . $tmp[1] . '/' . $tmp[2];
    }

    $Requete          = "SELECT * FROM $db_membres WHERE nom ='$nom_membre'";
    $Requete_stock    = "SELECT * FROM $db_prod_prest WHERE qte_limite!=0 AND quantite<=qte_limite";
    $Requete_agend    = "SELECT * FROM $db_agenda WHERE nom_membre='$nom_membre' AND STR_TO_DATE(date_complete, '%d/%m/%Y')>= '$date_deb' AND STR_TO_DATE(date_complete, '%d/%m/%Y')<= '$date_fin' ORDER BY STR_TO_DATE(date_complete, '%d/%m/%Y') DESC";
    $Requete_nouv_cli = "SELECT * FROM $db_compte_client ORDER BY ref_clients ASC LIMIT $num_nouv_cli";
    $Requete_memo     = "SELECT * FROM $db_agenda WHERE nom_membre ='$nom_membre' AND date_complete='$date_memo' ORDER BY STR_TO_DATE(date_complete, '%d/%m/%Y') DESC";
    $Requete_fact     = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='facture' AND etat='en cours' ORDER by num_fact";
    $Requete_dev      = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='devis' AND etat='en cours' ORDER by num_fact";
    $Requete_comm     = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND type='commande' AND etat='en cours' ORDER by num_fact";
    $Requete_doc_val  = "SELECT * FROM $db_facture_entete WHERE nom_membre = '$nom_membre' AND STR_TO_DATE(date_valid_fin, '%d/%m/%Y')<= STR_TO_DATE('$date_memo', '%d/%m/%Y') ORDER by num_fact";
    if (isset($_GET['search']) && $_GET['search'] != '') {
        $Requete_doc_val = "SELECT * FROM $db_facture_entete RIGHT JOIN $db_compte_client ON {$db_facture_entete}.nom_client = {$db_compte_client}.ref_clients WHERE {$db_compte_client}.nom LIKE '%{$_GET['search']}%' AND {$db_facture_entete}.nom_membre = '$nom_membre' AND STR_TO_DATE(date_valid_fin, '%d/%m/%Y')<= STR_TO_DATE('$date_memo', '%d/%m/%Y') ORDER by {$db_facture_entete}.num_fact";
    }

    $db = mysqli_connect($db_server, $db_user, $db_password) or die('<span class="err_bdd">Erreur de connexion au serveur</span>');
    mysqli_select_db($db, $db_database) or die('<span class="err_bdd">Erreur de s&eacute;lection, base de donn&eacute;es incorrecte ou inexistante</span>');

    $ResReq       = mysqli_query($db, $Requete) or die('<span class="err_bdd">Erreur de s&eacute;lection, compte incorrect ou inexistant</span>');
    $ResReq_stock = mysqli_query($db, $Requete_stock) or die('<span class="err_bdd">Ex&eacute;cution requ&ecirc;te impossible, table incorrecte ou inexistante</span>');
    $ResReq_fact  = mysqli_query($db, $Requete_fact) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');
    $ResReq_dev   = mysqli_query($db, $Requete_dev) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');
    $ResReq_comm  = mysqli_query($db, $Requete_comm) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');

    $Donnees = mysqli_fetch_array($ResReq);

    $ref           = $Donnees["ref"];
    $id            = $Donnees["id"];
    $mdp           = $Donnees["mdp"];
    $nom           = $Donnees["nom"];
    $memo          = $Donnees["memo"];
    $info_admin    = $Donnees["info_admin"];
    $info_facture  = $Donnees["info_facture"];
    $info_devis    = $Donnees["info_devis"];
    $info_commande = $Donnees["info_commande"];

    $ResReq_agend                                 = mysqli_query($db, $Requete_agend) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
    $nbenreg_agend                                = mysqli_num_rows($ResReq_agend);
    $nbchamps_agend                               = mysqli_num_fields($ResReq_agend);
    $Tabdo_agend[$nbenreg_agend][$nbchamps_agend] = "";
    $Tabchamps_agend[$nbchamps_agend]             = "";

    for ($I = 0; $I < $nbchamps_agend; $I++) {
        $tabchamps_agend[$I] = mysqli_fetch_field_direct($ResReq_agend, $I);
    }

    $I = 0;

    while ($donnees_agend = mysqli_fetch_array($ResReq_agend)) {
        $Tabdo_agend[$I][1]  = $donnees_agend["ref"];
        $Tabdo_agend[$I][2]  = $donnees_agend["nom_membre"];
        $Tabdo_agend[$I][3]  = $donnees_agend["date_complete"];
        $Tabdo_agend[$I][4]  = $donnees_agend["intit_action"];
        $Tabdo_agend[$I][5]  = $donnees_agend["horaire_deb"];
        $Tabdo_agend[$I][6]  = $donnees_agend["horaire_fin"];
        $Tabdo_agend[$I][7]  = $donnees_agend["action"];
        $Tabdo_agend[$I][8]  = $donnees_agend["ListCli"];
        $Tabdo_agend[$I][9]  = $donnees_agend["ListFour"];
        $Tabdo_agend[$I][10] = $donnees_agend["ListProd"];
        $Tabdo_agend[$I][11] = $donnees_agend["ListPrest"];
        $Tabdo_agend[$I][12] = $donnees_agend["detail_agend"];
        $I++;
    }

    $ResReq_nouv_cli                                       = mysqli_query($db, $Requete_nouv_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
    $nbenreg_nouv_cli                                      = mysqli_num_rows($ResReq_nouv_cli);
    $nbchamps_nouv_cli                                     = mysqli_num_fields($ResReq_nouv_cli);
    $Tabdo_nouv_cli[$nbenreg_nouv_cli][$nbchamps_nouv_cli] = "";
    $Tabchamps_nouv_cli[$nbchamps_nouv_cli]                = "";

    for ($J = 0; $J < $nbchamps_nouv_cli; $J++) {
        $tabchamps_nouv_cli[$J] = mysqli_fetch_field_direct($ResReq_nouv_cli, $J);
    }

    $J = 0;

    while ($donnees_nouv_cli = mysqli_fetch_array($ResReq_nouv_cli)) {
        $Tabdo_nouv_cli[$J][1] = $donnees_nouv_cli["nom"];
        $Tabdo_nouv_cli[$J][2] = $donnees_nouv_cli["nature"];
        $Tabdo_nouv_cli[$J][3] = $donnees_nouv_cli["adresse"];
        $Tabdo_nouv_cli[$J][4] = $donnees_nouv_cli["ville"];
        $Tabdo_nouv_cli[$J][5] = $donnees_nouv_cli["code_postal"];
        $Tabdo_nouv_cli[$J][6] = $donnees_nouv_cli["tel"];
        $Tabdo_nouv_cli[$J][7] = $donnees_nouv_cli["tel_portable"];
        $Tabdo_nouv_cli[$J][8] = $donnees_nouv_cli["mail"];
        $Tabdo_nouv_cli[$J][9] = $donnees_nouv_cli["activite"];
        $J++;
    }

    $ResReq_memo                               = mysqli_query($db, $Requete_memo) or die('<span class="err_bdd">Erreur de s&eacute;lection, membre incorrect ou inexistant</span>');
    $nbenreg_memo                              = mysqli_num_rows($ResReq_memo);
    $nbchamps_memo                             = mysqli_num_fields($ResReq_memo);
    $Tabdo_memo[$nbenreg_memo][$nbchamps_memo] = "";
    $Tabchamps_memo[$nbchamps_memo]            = "";

    for ($K = 0; $K < $nbchamps_memo; $K++) {
        $tabchamps_memo[$K] = mysqli_fetch_field_direct($ResReq_memo, $K);
    }

    $K = 0;

    while ($donnees_memo = mysqli_fetch_array($ResReq_memo)) {
        $Tabdo_memo[$K][1]  = $donnees_memo["ref"];
        $verif_memo         = $Tabdo_memo[$K][1];
        $Tabdo_memo[$K][2]  = $donnees_memo["nom_membre"];
        $Tabdo_memo[$K][3]  = $donnees_memo["date_complete"];
        $Tabdo_memo[$K][4]  = $donnees_memo["intit_action"];
        $Tabdo_memo[$K][5]  = $donnees_memo["horaire_deb"];
        $Tabdo_memo[$K][6]  = $donnees_memo["horaire_fin"];
        $Tabdo_memo[$K][7]  = $donnees_memo["action"];
        $Tabdo_memo[$K][8]  = $donnees_memo["ListCli"];
        $Tabdo_memo[$K][9]  = $donnees_memo["ListFour"];
        $Tabdo_memo[$K][10] = $donnees_memo["ListProd"];
        $Tabdo_memo[$K][11] = $donnees_memo["ListPrest"];
        $Tabdo_memo[$K][12] = $donnees_memo["detail_agend"];
        $K++;
    }

    $ResReq_doc_val                                     = mysqli_query($db, $Requete_doc_val) or die('<span class="err_bdd">Erreur de s&eacute;lection, document incorrect ou inexistant</span>');
    $nbenreg_doc_val                                    = mysqli_num_rows($ResReq_doc_val);
    $nbchamps_doc_val                                   = mysqli_num_fields($ResReq_doc_val);
    $Tabdo_doc_val[$nbenreg_doc_val][$nbchamps_doc_val] = "";
    $Tabchamps_doc_val[$nbchamps_doc_val]               = "";

    for ($L = 0; $L < $nbchamps_doc_val; $L++) {
        $tabchamps_doc_val[$L] = mysqli_fetch_field_direct($ResReq_doc_val, $L);
    }

    $L = 0;

    while ($donnees_doc_val = mysqli_fetch_array($ResReq_doc_val)) {
        $Tabdo_doc_val[$L][1] = $donnees_doc_val["num_fact"];
        $Tabdo_doc_val[$L][2] = $donnees_doc_val["date_fact"];
        $Tabdo_doc_val[$L][3] = $donnees_doc_val["date_valid_deb"];
        $Tabdo_doc_val[$L][4] = $donnees_doc_val["date_valid_fin"];
        $Tabdo_doc_val[$L][5] = $donnees_doc_val["nom_client"];
        $Tabdo_doc_val[$L][6] = $donnees_doc_val["type"];
        $Tabdo_doc_val[$L][7] = $donnees_doc_val["etat"];
        $L++;
    }

    ?>
<?php
    if ($verif_memo != "") {
        ?>

			<?php
for ($K = 0; $K < $nbenreg_memo; $K++) {
            if ($Tabdo_memo[$K][5] != "") {
                if ($Tabdo_memo[$K][6] != "") {
                } else {
                }
            } else {
            }
            if ($Tabdo_memo[$K][8] != "") {
                $Requete_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='" . $Tabdo_memo[$K][8] . "'";
                $ResReq_cli  = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
                $Donnees_cli = mysqli_fetch_array($ResReq_cli);
                $nom_cli     = $Donnees_cli["nom"];
            }
            if ($Tabdo_memo[$K][9] != "") {
                $Requete_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='" . $Tabdo_memo[$K][9] . "'";
                $ResReq_four  = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>');
                $Donnees_four = mysqli_fetch_array($ResReq_four);
                $nom_four     = $Donnees_four["nom"];
            }
            if ($Tabdo_memo[$K][10] != "") {
                $Requete_prod = "SELECT designation FROM $db_prod_prest WHERE nature='produit' AND ref_produits='" . $Tabdo_memo[$K][10] . "'";
                $ResReq_prod  = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>');
                $Donnees_prod = mysqli_fetch_array($ResReq_prod);
                $nom_prod     = $Donnees_prod["designation"];
            }
            if ($Tabdo_memo[$K][11] != "") {
                $Requete_prest = "SELECT designation FROM $db_prod_prest WHERE nature='prestation' AND ref_produits='" . $Tabdo_memo[$K][11] . "'";
                $ResReq_prest  = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>');
                $Donnees_prest = mysqli_fetch_array($ResReq_prest);
                $nom_prest     = $Donnees_prest["designation"];
            }
        }
        ?>

		<?php
}
    ?>


<?php

    $ld = '<p><label class="gauche" for="liste_fact">Factures en cours : </label>
				<select class="droit" id="liste_fact" name="liste_fact">
				<option value=""></option>';
    while ($LigneDo_fact = mysqli_fetch_array($ResReq_fact)) {
        $Nmnum_fact = $LigneDo_fact["num_fact"];

        $ld .= '<option value="' . $Nmnum_fact . '">' . $Nmnum_fact . '</option>';
    }
    $ld .= '</select></p>';

    $ld2 = '<p><label class="gauche" for="liste_dev">Devis en cours : </label>
				<select class="droit" id="liste_dev" name="liste_dev">
				<option value=""></option>';
    while ($LigneDo_dev = mysqli_fetch_array($ResReq_dev)) {
        $Nmnum_dev = $LigneDo_dev["num_fact"];

        $ld2 .= '<option value="' . $Nmnum_dev . '">' . $Nmnum_dev . '</option>';
    }
    $ld2 .= '</select></p>';

    print $ld2;

    $ld3 = '<p><label class="gauche" for="liste_comm">Commandes en cours : </label>
				<select class="droit" id="liste_comm" name="liste_comm">
				<option value=""></option>';
    while ($LigneDo_comm = mysqli_fetch_array($ResReq_comm)) {
        $Nmnum_comm = $LigneDo_comm["num_fact"];

        $ld3 .= '<option value="' . $Nmnum_comm . '">' . $Nmnum_comm . '</option>';
    }
    $ld3 .= '</select></p>';

    ?>

<?php
if ($affdatdeb != "" || $affdatfin != "") {
        ?>

<?php
for ($I = 0; $I < $nbenreg_agend; $I++) {
            if ($Tabdo_agend[$I][5] != "") {
                if ($Tabdo_agend[$I][6] != "") {
                } else {
                }
            } else {
            }
            if ($Tabdo_agend[$I][8] != "") {
                $Requete_cli = "SELECT nom FROM $db_compte_client WHERE ref_clients='" . $Tabdo_agend[$I][8] . "'";
                $ResReq_cli  = mysqli_query($db, $Requete_cli) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
                $Donnees_cli = mysqli_fetch_array($ResReq_cli);
                $nom_cli     = $Donnees_cli["nom"];
            }
            if ($Tabdo_agend[$I][9] != "") {
                $Requete_four = "SELECT nom FROM $db_fournisseurs WHERE ref_fournisseur='" . $Tabdo_agend[$I][9] . "'";
                $ResReq_four  = mysqli_query($db, $Requete_four) or die('<span class="err_bdd">Erreur de s&eacute;lection, fournisseur incorrect ou inexistant</span>');
                $Donnees_four = mysqli_fetch_array($ResReq_four);
                $nom_four     = $Donnees_four["nom"];
            }
            if ($Tabdo_agend[$I][10] != "") {
                $Requete_prod = "SELECT designation FROM $db_prod_prest WHERE nature='produit' AND ref_produits='" . $Tabdo_agend[$I][10] . "'";
                $ResReq_prod  = mysqli_query($db, $Requete_prod) or die('<span class="err_bdd">Erreur de s&eacute;lection, produit incorrect ou inexistant</span>');
                $Donnees_prod = mysqli_fetch_array($ResReq_prod);
                $nom_prod     = $Donnees_prod["designation"];
            }
            if ($Tabdo_agend[$I][11] != "") {
                $Requete_prest = "SELECT designation FROM $db_prod_prest WHERE nature='prestation' AND ref_produits='" . $Tabdo_agend[$I][11] . "'";
                $ResReq_prest  = mysqli_query($db, $Requete_prest) or die('<span class="err_bdd">Erreur de s&eacute;lection, prestation incorrecte ou inexistante</span>');
                $Donnees_prest = mysqli_fetch_array($ResReq_prest);
                $nom_prest     = $Donnees_prest["designation"];
            }
        }
        ?>

<?php
}
    if ($nbenreg_doc_val != 0) {
        ?>
		<fieldset>

		<legend class="lg"> Ech&eacute;ance des documents : </legend>

		<p class="cen"><strong>Appara&icirc;ssent dans ce cadre les documents ayant d&eacute;pass&eacute; leur date d'&eacute;ch&eacute;ance</strong></p>
            <div class="all">
                <input type="radio" name="all" value="all"><p>All</p>
                <input type="radio" name="rien" value="hidden-all" checked="checked"><p>Rien</p>
                <form action="">
                    <label for="search">Search:</label>
                    <input type="text" name="search">
                </form>
            </div>
            <div class="filter-radio">
            <p>Type:</p>
                <input type="radio" name="type" value="commande"><p>Commande</p>
                <input type="radio" name="type" value="devis"><p>Devis</p>
                <input type="radio" name="type" value="facture"><p>Facture</p>
                <input type="radio" name="type" value="fact_av"><p>Avoir</p>
                <input type="radio" name="type" value="noselecttype"><p>All</p>
            </div>
            <div class="filter-radio">
            <p>Etat:</p>
                <input type="radio" name="etat" value="en cours"><p>En cours</p>
                <input type="radio" name="etat" value="annulation"><p>Annulation</p>
                <input type="radio" name="etat" value="erreur saisie"><p>Erreur saisie</p>
                <input type="radio" name="etat" value="noselectetat"><p>All</p>
            </div>
            <style>
            .filter-radio,.all{text-align: center;margin-bottom: 20px;}
                .filter-radio p, .all p{display: inline;margin: 0px 10px;}
            }
            </style>
			<table>

<?php
echo '<tr>
				<th>Num&eacute;ro</th>
				<th>Type</th>
				<th>Date cr&eacute;ation</th>
				<th>Validit&eacute;</th>
				<th>Client</th>
				<th>Etat</th>
				<th>Modifier</th>
			</tr>';

        for ($L = 0; $L < $nbenreg_doc_val; $L++) {
            echo '<form action="supp_fact.php" method="post">
				<input type="hidden" name="ListFact" value="' . $Tabdo_doc_val[$L][1] . '" />
				<tr>
				<td>' . $Tabdo_doc_val[$L][1] . '</td>
				<td class="type" name="' . $Tabdo_doc_val[$L][6] . '">' . $Tabdo_doc_val[$L][6] . '</td>
				<td>' . $Tabdo_doc_val[$L][2] . '</td>
				<td>';
            if ($Tabdo_doc_val[$L][3] != "" && $Tabdo_doc_val[$L][4] != "") {
                echo 'Du ' . $Tabdo_doc_val[$L][3] . ' au ' . $Tabdo_doc_val[$L][4];
            }
            echo '</td>';
            if ($Tabdo_doc_val[$L][5] != "") {
                $Requete_cli_ech = "SELECT nom FROM $db_compte_client WHERE ref_clients='" . $Tabdo_doc_val[$L][5] . "'";
                $ResReq_cli_ech  = mysqli_query($db, $Requete_cli_ech) or die('<span class="err_bdd">Erreur de s&eacute;lection, client incorrect ou inexistant</span>');
                $Donnees_cli_ech = mysqli_fetch_array($ResReq_cli_ech);
                $nom_cli_ech     = $Donnees_cli_ech["nom"];
                echo '<td>' . $nom_cli_ech . '</td>';
            } else {
                echo '<td></td>';
            }
            echo '<td class="etat" name="' . $Tabdo_doc_val[$L][7] . '">' . $Tabdo_doc_val[$L][7] . '</td>
				<td><a href="/modif_fact.php?num_fact=' . $Tabdo_doc_val[$L][1] . '">Modifier</a></td>
				<td><input type="submit" name="supp_fact" value="St."/></td>
				</tr>
				</form>';
        }
        ?>

			</table>

		</fieldset>

<?php
}
    ?>

<?php
if ($num_nouv_cli != "0") {
        ?>
<?php
for ($J = 0; $J < $nbenreg_nouv_cli; $J++) {
        }

    }
    ?>

				<?php
while ($LigneDo_stock = mysqli_fetch_array($ResReq_stock)) {
        $designation_stock = $LigneDo_stock["designation"];
        $reference_stock   = $LigneDo_stock["reference"];
        $quantite_stock    = $LigneDo_stock["quantite"];
        echo '<li><p>D&eacute;signation et r&eacute;f&eacute;rence : <strong>' . $designation_stock . ' - ' . $reference_stock . '</strong>, Quantit&eacute; restante : <strong>' . $quantite_stock . '</strong></p></li>';
    }
    ?>

<?php
}
?>


			
			<p class="cen" style="clear:both;"><a href="Accueil.php">Revenir &agrave; l'accueil</a></p>
				
		</div>
		
	</div>
	
<?php
		}
require_once 'Main_ft.php'; 
?>